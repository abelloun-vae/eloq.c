//######################
// Map Init
//######################
Value ValueMap_init(ValueMap this, Boo record, Map map) {
	this->id = ValueId_Map;
	this->record = record;
	this->count = 0;
	this->map.size = map.size;
	for(Nat i = 0; i < map.size; i++)
		this->map.pairs[i][0] = 0;
	for(Nat i = 0; i < map.size; i++)
		this->count += Map_add(this->map, map.pairs[i][0], map.pairs[i][1]);
	return (Value) this;
}

//######################
// Map Methods
//######################
Boo ValueMap_refl(ValueMap l, ValueMap r) {
	(void) l;
	(void) r;
	return 0;
}

Boo ValueMap_toBoo(ValueMap v) {
	(void) v;
	return 1;
}

Key32 ValueMap_toHash32(ValueMap v) {
	(void) v;
	return 0;
}

void ValueMap_write(ValueMap v, Writer w, Value_WriterMode mode) {
	Writer_writeChar(w, v->record ? '{' : '[');
	if (v->count) {
		Writer_indent(w, 1);
		for (Nat i = 0; i < v->map.size; i++)
			if (v->map.pairs[i][0]) {
				Writer_writeLine(w);
				Value_write(v->map.pairs[i][0], w, mode);
				Writer_writeStream0(w, (Stream0) ": ");
				Value_write(v->map.pairs[i][1], w, mode);
			}
		Writer_indent(w, -1);
		Writer_writeLine(w);
	}
	Writer_writeChar(w, v->record ? '}' : ']');
}

Values ValueMap_children(ValueMap this) {
	Value* values = Allocator_alloc(sizeof(Value[2 * this->count]));
	for(Nat i = 0, j = 0; i < this->map.size; i++)
		if (this->map.pairs[i][0]) {
			values[j++] = this->map.pairs[i][0];
			values[j++] = this->map.pairs[i][1];
		}
	return (Values) {2 * this->count, values};
}
