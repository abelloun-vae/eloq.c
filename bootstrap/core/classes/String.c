//######################
// String Methods
//######################
Boo ValueString_refl(ValueString l, ValueString r) {
	if (l->len != r->len)
		return 0;
	for(Nat i = 0; i < l->len; i++)
		if ( l->str[i] != r->str[i] )
			return 0;
	return 1;
}

Boo ValueString_toBoo(ValueString v) {
	return v->len ? 1 : 0;
}

Key32 ValueString_toHash32(ValueString v) {
	(void) v;
	return 0;
}

void ValueString_write(ValueString v, Writer w, Value_WriterMode mode) {
	if (mode == Value_WriterMode_string)
		Writer_writeString(w, v->str, v->len);
	else {
		Writer_writeChar(w, '"');
		for(Nat i = 0; i < v->len; i++) {
			if (v->str[i] == '\n') {
				Writer_writeChar(w, '\\');
				Writer_writeChar(w, 'n');
			} else if (v->str[i] == '\t') {
				Writer_writeChar(w, '\\');
				Writer_writeChar(w, 't');
			} else {
				if (v->str[i] == '"' || v->str[i] == '{')
					Writer_writeChar(w, '\\');
				Writer_writeChar(w, v->str[i]);
			}
		}
		Writer_writeChar(w, '"');
	}
}

Values ValueString_children(ValueString this) {
	(void) this;
	return (Values) {0, 0};
}
