//######################
// Name Methods
//######################
Boo ValueName_refl(ValueName l, ValueName r) {
	return l->name == r->name && l->arity == r->arity;
}

Boo ValueName_toBoo(ValueName v) {
	(void) v;
	return 1;
}

Key32 ValueName_toHash32(ValueName v) {
	(void) v;
	return 0;
}

void ValueName_write(ValueName v, Writer w, Value_WriterMode mode) {
	if (v->data.size) Writer_writeChar(w, '(');
	ValueName_writeName(v, w);
	for (Nat i = 0; i < v->data.size; i++) {
		Writer_writeChar(w, ' ');
		Value_write(v->data.values[i], w, mode);
	}
	if (v->data.size) Writer_writeChar(w, ')');
}

Values ValueName_children(ValueName this) {
	return this->data;
}
