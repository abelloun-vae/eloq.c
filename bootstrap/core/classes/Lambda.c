//######################
// Lambda Methods
//######################
Boo ValueLambda_refl(ValueLambda l, ValueLambda r) {
	return l->lambda == r->lambda && l->arity == r->arity;
}

Boo ValueLambda_toBoo(ValueLambda this) {
	(void) this;
	return 1;
}

Key32 ValueLambda_toHash32(ValueLambda this) {
	(void) this;
	return 0;
}

void ValueLambda_write(ValueLambda this, Writer w, Value_WriterMode mode) {
	(void) this;
	(void) mode;
	Writer_writeStream0(w, (Stream0) "<Lambda>");
}

Values ValueLambda_children(ValueLambda this) {
	return this->args;
}
