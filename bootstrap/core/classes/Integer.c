//######################
// Integer Methods
//######################
Boo ValueInteger_refl(ValueInteger l, ValueInteger r) {
	return l->i == r->i;
}

Boo ValueInteger_toBoo(ValueInteger v) {
	return v->i;
}

Key32 ValueInteger_toHash32(ValueInteger v) {
	(void) v;
	return 0;
}

void ValueInteger_write(ValueInteger v, Writer w, Value_WriterMode mode) {
	(void) mode;
	Writer_writeInteger(w, v->i);
}

Values ValueInteger_children(ValueInteger this) {
	(void) this;
	return (Values) {0, 0};
}
