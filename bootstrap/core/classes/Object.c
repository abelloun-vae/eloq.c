//######################
// Object Methods
//######################
Boo ValueObject_refl(ValueObject l, ValueObject r) {
	return l->objectId == r->objectId && l->object == r->object && l->arity == r->arity;
}

Boo ValueObject_toBoo(ValueObject this) {
	(void) this;
	return true;
}

Key32 ValueObject_toHash32(ValueObject this) {
	(void) this;
	return 0;
}

void ValueObject_write(ValueObject this, Writer w, Value_WriterMode mode) {
	(void) this;
	(void) mode;
	Writer_writeStream0(w, (Stream0) "(**<OBJECT>**)");
}

Values ValueObject_children(ValueObject this) {
	return this->args;
}
