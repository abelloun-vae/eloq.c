//######################
// Boolean Methods
//######################
Boo ValueBoolean_refl(ValueBoolean l, ValueBoolean r) {
	return l->b == r->b;
}

Boo ValueBoolean_toBoo(ValueBoolean this) {
	return this->b;
}

Key32 ValueBoolean_toHash32(ValueBoolean this) {
	(void) this;
	return 0;
}

void ValueBoolean_write(ValueBoolean this, Writer w, Value_WriterMode mode) {
	(void) mode;
	Writer_writeStream0(w, (Stream0) (this->b ? "true" : "false"));
}

Values ValueBoolean_children(ValueBoolean this) {
	(void) this;
	return (Values) {0, 0};
}
