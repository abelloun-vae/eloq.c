//######################
// Closure Methods
//######################
Boo ValueClosure_refl(ValueClosure l, ValueClosure r) {
	return l->closure == r->closure;
}

Boo ValueClosure_toBoo(ValueClosure v) {
	(void) v;
	return 1;
}

Key32 ValueClosure_toHash32(ValueClosure v) {
	(void) v;
	return 0;
}

void ValueClosure_write(ValueClosure v, Writer w, Value_WriterMode mode) {
	(void) v;
	(void) mode;
	Writer_writeStream0(w, (Stream0) "<CLOSURE>");
}

Values ValueClosure_children(ValueClosure this) {
	return this->scope;
}
