//######################
// Array Methods
//######################
Boo ValueArray_refl(ValueArray l, ValueArray r) {
	(void) l;
	(void) r;
	return 1;
}

Boo ValueArray_toBoo(ValueArray v) {
	return v->data.size ? 1 : 0;
}

Key32 ValueArray_toHash32(ValueArray v) {
	(void) v;
	return 0;
}

void ValueArray_write(ValueArray v, Writer w, Value_WriterMode mode) {
	Writer_writeChar(w, v->tuple ? '{' : '[');
	switch (v->data.size) {

		case 0: break;

		case 1: Value_write(v->data.values[0], w, mode); break;

		default:
			Writer_indent(w, 1);
			for (Nat i = 0; i < v->data.size; i++) {
				Writer_writeLine(w);
				Value_write(v->data.values[i], w, mode);
			}
			Writer_indent(w, -1);
			Writer_writeLine(w);
	}
	Writer_writeChar(w, v->tuple ? '}' : ']');
}

Values ValueArray_children(ValueArray this) {
	return this->data;
}
