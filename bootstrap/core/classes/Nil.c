//######################
// Nil Methods
//######################
Boo ValueNil_refl(ValueNil l, ValueNil r) {
	(void) l;
	(void) r;
	return 1;
}

Boo ValueNil_toBoo(ValueNil this) {
	(void) this;
	return 0;
}

Key32 ValueNil_toHash32(ValueNil this) {
	(void) this;
	return 0;
}

void ValueNil_write(ValueNil this, Writer w, Value_WriterMode mode) {
	(void) this;
	(void) mode;
	Writer_writeStream0(w, (Stream0) "()");
}

Values ValueNil_children(ValueNil this) {
	(void) this;
	return (Values) {0, 0};
}
