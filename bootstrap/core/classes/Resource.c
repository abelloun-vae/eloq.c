//######################
// Resource Methods
//######################
Boo ValueResource_refl(ValueResource l, ValueResource r) {
	return l->r == r->r;
}

Boo ValueResource_toBoo(ValueResource v) {
	(void) v;
	return 1;
}

Key32 ValueResource_toHash32(ValueResource v) {
	(void) v;
	return 0;
}

void ValueResource_write(ValueResource v, Writer w, Value_WriterMode mode) {
	(void) v;
	(void) mode;
	Writer_writeStream0(w, (Stream0) "<RESOURCE>");
}

Values ValueResource_children(ValueResource this) {
	(void) this;
	return (Values) {0, 0};
}
