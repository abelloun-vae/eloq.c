//######################
// Byte Methods
//######################
Boo ValueByte_refl(ValueByte l, ValueByte r) {
	return l->b == r->b;
}

Boo ValueByte_toBoo(ValueByte v) {
	return v->b ? 1 : 0;
}

Key32 ValueByte_toHash32(ValueByte v) {
	return v->b;
}

void ValueByte_write(ValueByte v, Writer w, Value_WriterMode mode) {
	(void) mode;
	Writer_writeByte(w, v->b);
}

Values ValueByte_children(ValueByte this) {
	(void) this;
	return (Values) {0, 0};
}
