//######################
// Cons Methods
//######################
Boo ValueCons_refl(ValueCons l, ValueCons r) {
	return Value_refl(l->head, r->head) && Value_refl(l->rest, r->rest);
}

Boo ValueCons_toBoo(ValueCons this) {
	(void) this;
	return 1;
}

Key32 ValueCons_toHash32(ValueCons this) {
	(void) this;
	return 0;
}

void ValueCons_write(ValueCons this, Writer w, Value_WriterMode mode) {
	(void) this;
	(void) mode;
	Writer_writeStream0(w, (Stream0) "(....)");
}

Values ValueCons_children(ValueCons this) {
	(void) this;
	return (Values) {0, 0};
}
