//######################
// Stream Methods
//######################
Boo ValueStream_refl(ValueStream l, ValueStream r) {
	if (l->len != r->len)
		return 0;
	for(Nat i = 0; i < l->len; i++)
		if ( l->stm[i] != r->stm[i] )
			return 0;
	return 1;
}

Boo ValueStream_toBoo(ValueStream v) {
	return v->len ? 1 : 0;
}

Key32 ValueStream_toHash32(ValueStream v) {
	(void) v;
	return 0;
}

void ValueStream_write(ValueStream v, Writer w, Value_WriterMode mode) {
	if (mode == Value_WriterMode_string)
		Writer_writeStream(w, v->stm, v->len);
	else {
		Writer_writeChar(w, '"');
		for(Nat i = 0; i < v->len; i++) {
			if (v->stm[i] == '\n') {
				Writer_writeChar(w, '\\');
				Writer_writeChar(w, 'n');
			} else if (v->stm[i] == '\t') {
				Writer_writeChar(w, '\\');
				Writer_writeChar(w, 't');
			} else {
				if (v->stm[i] == '"' || v->stm[i] == '{')
					Writer_writeChar(w, '\\');
				Writer_writeChar(w, v->stm[i]);
			}
		}
		Writer_writeChar(w, '"');
	}
}

Values ValueStream_children(ValueStream this) {
	(void) this;
	return (Values) {0, 0};
}
