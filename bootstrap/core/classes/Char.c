//######################
// Char Methods
//######################
Boo ValueChar_refl(ValueChar l, ValueChar r) {
	return l->c == r->c;
}

Boo ValueChar_toBoo(ValueChar v) {
	return v->c ? 1 : 0;
}

Key32 ValueChar_toHash32(ValueChar v) {
	(void) v;
	return 0;
}

void ValueChar_write(ValueChar v, Writer w, Value_WriterMode mode) {
	(void) mode;
	Writer_writeChar(w, v->c);
}

Values ValueChar_children(ValueChar this) {
	(void) this;
	return (Values) {0, 0};
}
