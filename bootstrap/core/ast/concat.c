//######################
// concat
//######################
Value Ast_concat_String_String(ValueString l, ValueString r) {
	String str = Allocator_alloc(sizeof(Char[l->len + r->len]));
	memcpy(str, l->str, sizeof(Char[l->len]));
	memcpy(str + l->len, r->str, sizeof(Char[r->len]));
	return ValueString_make(l->len + r->len, str);
}

Value Ast_concat_Stream_Stream(ValueStream l, ValueStream r) {
	Stream stm = Allocator_alloc(sizeof(Byte[l->len + r->len]));
	memcpy(stm, l->stm, l->len);
	memcpy(stm + l->len, r->stm, r->len);
	return ValueStream_make(l->len + r->len, stm);
}

Value Ast_concat_Array_Array(ValueArray l, ValueArray r) {
	ValueArray arr = ValueArray_alloc(l->data.size + r->data.size);
	arr->tuple = l->tuple || r->tuple;
	arr->data.size = l->data.size + r->data.size;
	memcpy(arr->data.values, l->data.values, sizeof(Value[l->data.size]));
	memcpy(arr->data.values + l->data.size, r->data.values, sizeof(Value[r->data.size]));
	return (Value) arr;
}

Value Ast_concat_Map_Array(ValueMap l, ValueArray r) {
	return Ast_concat_Array_Map(r, l);
}

Value Ast_concat_Array_Map(ValueArray l, ValueMap r) {
	if(!l->data.size)
		return (Value) r;
	if(!r->map.size)
		return (Value) l;
	printf("Cannot concat an Array and a Map !!!");
	exit(1);
}

Value Ast_concat_Map_Map(ValueMap l, ValueMap r) {
	ValueMap arr = ValueMap_alloc(l->count + r->count);
	arr->record = l->record || r->record;
	arr->map.size = l->count + r->count;
	for(Nat len = arr->map.size, i = 0; i < len; i++)
		arr->map.pairs[i][0] = 0;
	arr->count = Map_concat(arr->map, l->map, r->map);
	return (Value) arr;
}
