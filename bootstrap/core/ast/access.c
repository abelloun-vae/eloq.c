//######################
// Access
//######################
Value Ast_access_Array_Integer(ValueArray l, ValueInteger r) {
	return l->data.values[(r->i % l->data.size + l->data.size) % l->data.size];
}

Value Ast_access_String_Integer(ValueString l, ValueInteger r) {
	return ValueChar_make(l->str[(r->i % l->len + l->len) % l->len]);
}

Value Ast_access_Stream_Integer(ValueStream l, ValueInteger r) {
	return ValueByte_make(l->stm[(r->i % l->len + l->len) % l->len]);
}

Value Ast_access_Map_Any(ValueMap l, Value r) {
	return Map_access(l->map, r);
}