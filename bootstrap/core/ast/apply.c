//######################
// Apply
//######################
Value Ast_apply_Closure(ValueClosure f, Value a) {
	return f->closure(f->scope, a);
}

Value Ast_apply_Lambda(ValueLambda f, Value a) {
	Value data[f->args.size + 1];
	for(Nat size = f->args.size, i = 0; i < size; i++)
		data[i] = f->args.values[i];
	data[f->args.size] = a;
	if (f->arity > 1)
		return ValueLambda_make(f->lambda, f->arity - 1, (Values) {f->args.size + 1, data});
	return f->lambda((Values) {f->args.size + 1, data});
}

Value Ast_apply_Name(ValueName f, Value a) {
	if (f->arity == 0) {
		printf("Arity exceeded\n");
		exit(1);
	}
	Value data[f->data.size + 1];
	for(Nat size = f->data.size, i = 0; i < size; i++)
		data[i] = f->data.values[i];
	data[f->data.size] = a;
	return ValueName_make(f->name, f->arity - 1, (Values) {f->data.size + 1, data});
}

Value Ast_apply_Object(ValueObject f, Value a) {
	Value data[f->args.size + 1];
	LOOP(f->args.size, i)
		data[i] = f->args.values[i];
	data[f->args.size] = a;
	if (f->arity > 1)
		return ValueObject_make(f->objectId, f->object, f->arity - 1, (Values) {f->args.size + 1, data});

	if ( f->objectId != ObjectId_Parser ) {
		printf("Object must be parser instead it is :\n");
		Value_printInfo((Value)f);
		exit(1);
	}
	if (a->id != ValueId_String) {
		printf("Params to parser must be rule name provided as a string, instead it is :\n");
		Value_printInfo(a);
		exit(1);
	}
	Parser p = (Parser) f->object;
	ValueString rule = (ValueString) f->args.values[0];
	ValueString input = (ValueString) a;
	PResult result = Parser_parse(p, Parser_resolve(p, rule->len, rule->str), input->len, input->str);

	return ValueArray_make(1, (Values) {3, (Value[]){
		ValueInteger_make(result.error),
		ValueInteger_make(result.position),
		result.result ? result.result : ValueNil_make()
	}});

}
