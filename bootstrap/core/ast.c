#include "../main.h"

#include "ast/apply.c"
#include "ast/access.c"
#include "ast/concat.c"

Value Ast_car_Cons(ValueCons v) {
	return v->head;
}

Value Ast_cdr_Cons(ValueCons v) {
	return v->rest;
}

Value Ast_add_Integer_Integer(ValueInteger l, ValueInteger r) {
	return ValueInteger_make(l->i + r->i);
}

Value Ast_add_Char_Integer(ValueChar l, ValueInteger r) {
	return ValueChar_make(l->c + (r->i % 0x100000000 + 0x100000000));
}

Value Ast_add_Byte_Integer(ValueByte l, ValueInteger r) {
	return ValueChar_make((l->b + (r->i % 256 + 256)));
}

Value Ast_dif_Integer_Integer(ValueInteger l, ValueInteger r) {
	return ValueInteger_make(l->i - r->i);
}

//~ //######################
//~ // Operators
//~ //######################
//~ Value Value_compose(Value* scope, Value arg) {
	//~ return Value_apply(scope[0], Value_apply(scope[1], arg));
//~ }

//~ //######################
//~ // Operators
//~ //######################
//~ Value Value_div(Value l, Value r) {
	//~ return ValueInteger_make(((ValueInteger)l)->i / ((ValueInteger)r)->i);
//~ }

//~ Value Value_mul(Value l, Value r) {
	//~ return ValueInteger_make(((ValueInteger)l)->i * ((ValueInteger)r)->i);
//~ }

//~ Value Value_pow(Value l, Value r) {
	//~ return ValueInteger_make(pow(((ValueInteger)l)->i, ((ValueInteger)r)->i));
//~ }

//~ Value Value_divc(Value l, Value r) {
	//~ double x = (double) ((ValueInteger)l)->i;
	//~ double y = (double) ((ValueInteger)r)->i;
	//~ return ValueInteger_make((x + y - 1) / y);
//~ }

//~ Value Value_divr(Value l, Value r) {
	//~ return ValueInteger_make(((ValueInteger)l)->i / ((ValueInteger)r)->i);
//~ }

//~ Value Value_divf(Value l, Value r) {
	//~ return ValueInteger_make(((ValueInteger)l)->i / ((ValueInteger)r)->i);
//~ }

//~ Value Value_mod(Value l, Value r) {
	//~ return ValueInteger_make(((ValueInteger)l)->i % ((ValueInteger)r)->i);
//~ }

//~ Value Value_xwing(Value l, Value r) {
	//~ int x = ((ValueInteger) l)->i;
	//~ int y = ((ValueInteger) r)->i;
	//~ return ValueInteger_make(x > y ? 1 : (x < y ? -1 : 0));
//~ }

//~ Value Value_diffkeys(Value l, Value r) {
	//~ MATCH2(l, r)
		//~ CASE2(	HashMap,		HashMap	)	return ValueHashMap_diffkeys((ValueHashMap) l, (ValueHashMap) r);
	//~ return dispatch_error("Value_diffkeys", "no matching case", 2, (Value[]) {l, r});
//~ }
