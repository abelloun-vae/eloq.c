#include "../main.h"
//######################
// Implementation Classes
//######################
#include "classes/Boolean.c"
#include "classes/Integer.c"
#include "classes/Resource.c"
#include "classes/Byte.c"
#include "classes/Stream.c"
#include "classes/Char.c"
#include "classes/String.c"
#include "classes/Nil.c"
#include "classes/Cons.c"
#include "classes/Array.c"
#include "classes/Name.c"
#include "classes/Map.c"
#include "classes/Closure.c"
#include "classes/Lambda.c"
#include "classes/Object.c"
