#include "main.h"

//###########
// CLI
//###########
Byte* findNull(Stream0 c) {
	while(*c) c++;
	return c;
}

void copyArgs(Byte* cla, int argc, char** argv) {
	int j, k = 0;
	for(int i = 1; i < argc; i++) {
		if (k) {
			cla[k] = ' ';
			k++;
		}
		j = 0;
		while(argv[i][j]) {
			cla[k] = argv[i][j];
			k++;
			j++;
		}
	}
	cla[k] = 0;
}

Value Env_init(int argc, char** argv) {
	// ARGUMENT
	Nat size = argc > 1 ? findNull((Stream0)argv[argc - 1]) - (Byte*)argv[1] : 0;
	Byte cmd[size + 1];
	copyArgs(cmd, argc, argv);
	// ENV
	Byte cwd[PATH_MAX];
	getcwd((char*)cwd, sizeof(cwd));
	return ValueMap_make(0, (Map) {3, (Value[][2]) {
		{readStream0((Stream0) "command"), readStream0((Stream0) argv[0])},
		{readStream0((Stream0) "argument"), readStream0((Stream0) cmd)},
		{readStream0((Stream0) "path"), readStream0((Stream0) cwd)},
	}});
}

void dump(Byte* cmd, Nat len) {
	Nat csize;
	Nat value;
	for(Nat i = 0; i < len; i += csize) {
		printf("%-*u : ", 3, i);
		csize = Stream_getNextCharSize(cmd + i);
		value = 0;

		for(Nat j = 0; j < csize; j++)
			printf("%c", cmd[i + j]);

		printf("  \t : ");
		for(Nat j = 0; j < 4; j++) {
			if (!cmd[i + j] || j >= csize)
				printf(" 00");
			else {
				printf(" %-*X", 2, cmd[i + j]);
				value += cmd[i + j] << (j * 8);
			}
		}

		printf("\t\t%u\n", value);

	}
}

//###########
// MAIN
//###########
int main(int argc, char** argv) {
	//~ char *(ps[1000]);
	//~ for(Nat i = 0; i < 5; i++) {
		//~ ps[i] = Pool_alloc(pool, i);
		//~ for(Nat j = 0; j < i; j++)
			//~ ps[i][j] = 'a' + j;
	//~ }
	//~ for(Nat i = 0; i < 5; i++) {
		//~ Pool_free(pool, ps[i]);
	//~ }

	//~ printf("%lu \n", sizeof(S_Value));
	//~ Value_printSizes();


	Allocator_push(0);
	Value_printInfo(Ast_apply(
		lambda(implementation()),
		Env_init(argc, argv)
	));
	printf("\n");
	Allocator_pop(0, 0);
	Allocator_statistics();
	Allocator_flush();

	//~ Page live = pool->live;
	//~ Pool_print(pool, "pool");
	//~ Allocator_print(pool->allocator, "allocator");
	//~ Page_print(pool->allocator->frames, "frames");
	//~ Size tot = 0;
	//~ Nat n = 0;
	//~ if (live) while(live->next) {
		//~ Page_print(live, "live page");
		//~ tot += live->free + (live->current - ((Ptr)live));
		//~ n++;
		//~ Pool_stash(pool, live);
		//~ live = live->next;
		//~ Pool_flush(pool);
	//~ }
	//~ printf("Locked Pages : %u\nLocked bytes: %lu\n", n, tot);


	return 0;
}
