#ifndef MAIN_H
#define MAIN_H

	#include <limits.h>
	#include <unistd.h>
	#include <math.h>
	#include <stdio.h>
	#include <stdlib.h>

	#include "libs/Types.h"
	#include "libs/Allocator/Allocator.h"
	#include "libs/Files/Files.h"
	#include "libs/Strings/Strings.h"
	#include "libs/Parsers/Parsers.h"
	#include "autogen/app.names.h"
	#include "autogen/core.h"
	#include "autogen/app.h"
	#include "system/extensions/extensions.h"
	#include "system/helpers/helpers.h"
	#include "system/implementations/implementations.h"

	#define MATCH(v)						switch(v->id)
	#define CASE(typ)						case ValueId_##typ

	#define CASE2(left, right)				if (matcher == ValueId_##left * _ValueId_MAX_ + ValueId_##right)
	#define MATCH2(left, right)				ValueId ValueId_ANY_left = left->id;\
										ValueId ValueId_ANY_right = right->id;\
										Nat matcher = ValueId_ANY_left * _ValueId_MAX_ + ValueId_ANY_right;

	Value			lambda				(Value _variable_implementations);

#endif
