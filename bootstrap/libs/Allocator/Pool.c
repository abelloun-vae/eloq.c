//#################################################
//# Global Pool State
//#################################################
S_Page zpage = {
	free: 0,
	current: &zpage + 1,
	next: 0
};

S_Pool pool = {
	ready: 0,
	live: &zpage,
	minalloc: 0,
	maxalloc: 0
};

//#################################################
//# Supplement space for each page
//#################################################
Size Pool_supp() {
	return sizeof(Nat);
}

Ptr Pool_ptr(Page p) {
	return ((Ptr)p) - Pool_supp();
}

Page Pool_page(Ptr p) {
	return p + Pool_supp();
}

Page Pool_zero(Page p) {
	((Nat*)p)[-1] = 0;
	return p;
}

Nat Pool_count(Page p) {
	return ((Nat*)p)[-1];
}

Nat Pool_inc(Page p) {
	return ++(((Nat*)p)[-1]);
}

Nat Pool_dec(Page p) {
	return --(((Nat*)p)[-1]);
}

//#################################################
//# System Allocation strategy
//#################################################
Size Pool_heuristic(Size s) {
	return s;
}

Page Pool_request(Size s) {
	if ( pool.live->free >= s )
		return Pages_remove(&pool.live);
	if ( pool.ready && pool.ready->free >= s )
		return Pages_remove(&pool.ready);
	s = Pool_heuristic(s);
	Page p = Pool_page(malloc(Pool_supp() + sizeof(S_Page) + s));
	return Page_init(Pool_zero(p), s);
}

Unit Pool_release(Page p) {
	free(Pool_ptr(p));
}

//#################################################
//# Client API
//#################################################
Ptr Pool_alloc(Size s) {
	Page page = Pool_request(s);
	Ptr mem = page->current;
	Pool_inc(page);
	page->current += s;
	page->free -= s;
	Pages_insertMax(&pool.live, page);
	return mem;
}

Unit Pool_free(Ptr mem) {
	Page* loc = Pages_find(&pool.live, mem - 1);
	if (!loc) {
		printf("Pool_free: Cannot free unknown pointer %p !", mem);
		exit(1);
	}
	if (!Pool_dec(*loc))
		Pool_release(Pages_remove(loc));
}

Ptr Pool_realloc(Ptr mem, Size old, Size new) {
	Page* loc = Pages_find(&pool.live, mem - 1);
	if (!loc) {
		printf("Pool_realloc: Cannot realloc unknown pointer %p !", mem);
		exit(1);
	}
	Page p = *loc;
	if ( old == new )
		return mem;
	if ( old > new ) {
		if (p->current == mem + old) {
			p->free += old - new;
			p->current -= old - new;
		}
		return mem;
	}
	if ( p->current == mem + old && p->free + old >= new ) {
		p->free -= new - old;
		p->current += new - old;
		return mem;
	}
	Ptr nmem = Pool_alloc(new);
	memcpy(nmem, mem, old);
	Pool_free(mem);
	return nmem;
}
