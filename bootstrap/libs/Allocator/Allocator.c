#include "Allocator.h"
#include "Page.c"
//#include "Pool.c"
//#############################################################################
//
//#############################################################################

Nat Allocator_gen(Allocator this) {
	return this ? 1 + Allocator_gen(this->next) : 0;
}

Unit Allocator_print(Allocator this, char* format, ...) {
	va_list args;
	printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	printf("@ ALLOCATOR %p	: ",		this				);
	va_start(args, format);
	vfprintf(stdout, format, args);
	va_end(args);
	printf("\n");
	printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	if (!this) return;

	printf("@ S_ALLO	: %lu\n",	sizeof(S_Allocator)		);
	printf("@ S_PAGE	: %lu\n",	sizeof(S_Page)			);
	printf("@ page		: %p\n", 	Page_find(this->pages, this)	);
	printf("@ addr		: %p\n", 	this				);
	printf("@ next		: %p\n",	this->next			);
	printf("@ skip		: %u\n",	this->skip			);
	printf("@ generation	: %u\n",	Allocator_gen(this->next)	);
	printf("@ pages count	: %u\n",	Pages_count(this->pages)	);
	printf("@ pages full	: %lu\n",	Pages_full(this->pages)		);
	printf("@ pages size	: %lu\n",	Pages_size(this->pages)		);
	printf("@ pages used	: %lu\n",	Pages_used(this->pages)		);
	printf("@ pages free	: %lu\n",	Pages_free(this->pages)		);
	printf("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");
	Pages_print(this->pages, 0);
}



//#############################################################################
// State
//#############################################################################
Size minalloc = 1024;
Size maxalloc = 2 * 1024 * 1024;
S_Allocator zallocator = {skip: 0, pages: 0, next: 0};
Allocator allocator = &zallocator;
Allocator oldAllocator = 0;
Page ready = 0;
Page tmp;
Nat allocators = 0;
Nat generation = 0;
Nat maxgen = 0;
Ptr lastkeep = 0;
Ptr lastflush = 0;
Size sofar = 0;
Boo noFlush = 0;

Unit Allocator_flush() {
	while(ready)
		free(Pages_remove(&ready));
}

Unit Allocator_statistics() {
	printf("Total allocators : %u\n", allocators);
	printf("Max generation : %u\n", maxgen);
}


Size Allocator_heuristic(Size s) {
	//~ Allocator_print(allocator, "HEURISTIC");
	//~ if ( Allocator_gen(allocator) > 3 )
		//~ exit(1);
	if (s >= maxalloc)
		return s;
	s = s + 2 * sofar + minalloc;
	return s > maxalloc ? maxalloc : s;
}

Page Allocator_request(Page* page, Size s) {
	if (*page && (*page)->free >= s)
		return Pages_remove(page);
	if (ready && ready->free >= s)
		return Pages_remove(&ready);
	s = Allocator_heuristic(s);
	sofar += s;
	printf("++++ MAKE NEW PAGE size : %lu\n", s);
	return Page_init(malloc(sizeof(S_Page) + s), s);
}

Unit Allocator_release(Page page) {
	Page_reset(page);
	if (Pages_free(ready) + page->free <=  maxalloc) {
		Pages_insertMax(&ready, page);
	} else {
		sofar -= page->free;
		free(page);
	}
}

Unit Allocator_push(Size s) {
	if ( allocator->skip < 255 && allocator->pages && allocator->pages->free > Page_size(allocator->pages) / 4 ) {
		allocator->skip++;
		return;
	}
	allocators++;
	generation++;
	maxgen = maxgen > generation ? maxgen : generation;
	//~ Nat gen = Allocator_gen(allocator) + 1;
	//~ generation = generation > gen ? generation : gen;
	//~ Allocator_print(allocator, "PUSH");
	Page p = Allocator_request(&allocator->pages, sizeof(S_Allocator) + s);
	//~ Page_print(p, "Selected page for allocator");
	Allocator a = Page_forward(p, sizeof(S_Allocator));
	a->skip = 0;
	a->pages = p;
	a->next = allocator;
	allocator = a;
	//~ Allocator_print(allocator, "PUSHED");
	//~ Page_print(p, "Selected page for allocator");
}

Ptr Allocator_pop(Cloner cl, Ptr obj) {
	if (allocator->skip--) {
		return obj;
	}
	generation--;
	//~ Allocator_print(allocator, "POP");
	//~ Page_print(page, "PARENT PAGE");
	//~ Pages_insertMax(&allocator->pages, page);
	oldAllocator = allocator;
	allocator = oldAllocator->next;
	oldAllocator->next = 0;
	Page parent = Pages_remove(Pages_find(&oldAllocator->pages, oldAllocator));
	if (oldAllocator->pages)
		Pages_insert(&allocator->pages, Pages_remove(&oldAllocator->pages));
	Ptr current = parent->current;
	tmp = parent;
	Pages_insertMax(&allocator->pages, parent);
	noFlush = 0;
	obj = obj ? cl(obj) : obj;
	tmp = 0;
	while (oldAllocator->pages)
		Allocator_release(Pages_remove(&oldAllocator->pages));
	if ( !noFlush && parent->current == current )
		Page_backward(parent, parent->current - (Ptr) oldAllocator);
	oldAllocator = 0;
	//~ Pages_insertMax(&allocator->pages, parent);
	//~ Allocator_print(allocator, "POPED");
	//~ Page_print(page, "POPED");
	return obj;
}

//#################################################
//# Client API
//#################################################
Boo Allocator_keep(Ptr ptr) {
	//~ return !Page_find(oldAllocator->pages, ptr);
	if (Page_find(tmp, ptr))
		noFlush = 1;
	return !Page_find(oldAllocator->pages, ptr);
}

Ptr Allocator_alloc(Size s) {
	Page page = Allocator_request(&allocator->pages, s);
	Ptr mem = Page_forward(page, s);
	Pages_insertMax(&allocator->pages, page);
	return mem;
}

Ptr Allocator_realloc(Ptr mem, Size old, Size new) {
	Ptr nmem = Allocator_alloc(new);
	memcpy(nmem, mem, old);
	Allocator_free(mem);
	return nmem;
}

Unit Allocator_free(Ptr ptr) {
	(void) ptr;
}
