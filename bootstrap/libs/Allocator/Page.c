//#############################################################################
// Page
//#############################################################################
Unit Page_print(Page this, char* format, ...) {
	va_list args;
	printf("*************************************************************\n");
	printf("* PAGE %p	: ",		this				);
	va_start(args, format);
	vfprintf(stdout, format, args);
	va_end(args);
	printf("\n");
	printf("*************************************************************\n");
	printf("* S_ALLO	: %lu\n",	sizeof(S_Allocator)		);
	printf("* S_PAGE	: %lu\n",	sizeof(S_Page)			);
	printf("* addr		: %p\n", 	this				);
	printf("* start		: %p\n",	Page_start(this)		);
	printf("* current	: %p\n",	this->current			);
	printf("* end		: %p\n",	Page_end(this)			);
	printf("* full		: %lu\n",	Page_full(this)			);
	printf("* size		: %lu\n",	Page_size(this)			);
	printf("* used		: %lu\n",	Page_used(this)			);
	printf("* free 		: %lu\n",	this->free			);
	printf("* next count	: %u\n",	Pages_count(this->next)		);
	printf("* next full	: %lu\n",	Pages_full(this->next)		);
	printf("* next size	: %lu\n",	Pages_size(this->next)		);
	printf("* next used	: %lu\n",	Pages_used(this->next)		);
	printf("* next free	: %lu\n",	Pages_free(this->next)		);
	printf("*************************************************************\n");
}

Size Page_full(Page this) {
	return (this->current - ((Ptr) this)) + this->free;
}

Size Page_size(Page this) {
	return Page_full(this) - sizeof(S_Page);
}

Size Page_used(Page this) {
	return Page_size(this) - this->free;
}

Ptr Page_start(Page this) {
	return this + 1;
}

Ptr Page_end(Page this) {
	return ((Ptr)this) + Page_full(this);
}

Boo Page_empty(Page this) {
	return this->current == Page_start(this);
}

Boo Page_contains(Page this, Ptr mem) {
	return Page_start(this) <= mem && mem <= Page_end(this);
}

//###############################################

Page Page_init(Page this, Size s) {
	this->free = s;
	this->current = this + 1;
	this->next = 0;
	return this;
}

Page Page_reset(Page this) {
	this->free = Page_size(this);
	this->current = this + 1;
	return this;
}

Ptr Page_forward(Page this, Size s) {
	this->free -= s;
	this->current += s;
	return this->current - s;
}

Ptr Page_backward(Page this, Size s) {
	this->free += s;
	this->current -= s;
	return this->current;
}

//#############################################################################
// Pages
//#############################################################################
Unit Pages_print(Page p, char* format, ...) {
	va_list args;

	printf("#################################################################################\n");
	if (format) {
		printf("# ");
		va_start(args, format);
		vfprintf(stdout, format, args);
		va_end(args);
		printf("\n");
	printf("#################################################################################\n");
	}
	printf("# pos | %12s | %12s | %8s | %8s | %8s | %8s #\n", 	"addr", "end", "full", "size", "used", "free"	);
	printf("# 000 | %12p | %12p | %8lu | %8lu | %8lu | %8lu #\n",	(Ptr)0, (Ptr)0, Pages_full(p), Pages_size(p), Pages_used(p), Pages_free(p));
	printf("# ----------------------------------------------------------------------------- #\n");
	Nat c = Pages_count(p);
	for(Nat i = 0; i < c; i++) {
	printf("# %3u | %12p | %12p | %8lu | %8lu | %8lu | %8lu #\n",	i, p, Page_end(p), Page_full(p), Page_size(p), Page_used(p), p->free);
	p = p->next;
	}
	printf("#################################################################################\n");
}

Nat Pages_count(Page p) {
	return p ? 1 + Pages_count(p->next) : 0;
}

Size Pages_full(Page p) {
	return p ? Page_full(p) + Pages_full(p->next) : 0;
}

Size Pages_size(Page p) {
	return p ? Page_size(p) + Pages_size(p->next) : 0;
}

Size Pages_used(Page p) {
	return p ? Page_used(p) + Pages_used(p->next) : 0;
}

Size Pages_free(Page p) {
	return p ? p->free + Pages_free(p->next) : 0;
}

//###############################################
Page* Pages_find(Page* loc, Ptr mem) {
	Page p = *loc;
	if (!p)
		return 0;
	if (Page_contains(p, mem))
		return loc;
	return Pages_find(&(p->next), mem);
}

Page Page_find(Page this, Ptr mem) {
	if (!this)
		return 0;
	if (Page_contains(this, mem))
		return this;
	return Page_find(this->next, mem);
}

Page* Pages_insertMin(Page* loc, Page p) {
	if (!*loc || p->free <= (*loc)->free) {
		Pages_insert(loc, p);
		return loc;
	}
	return Pages_insertMin(&((*loc)->next), p);
}

Page* Pages_insertMax(Page* loc, Page p) {
	if (!*loc || p->free >= (*loc)->free) {
		Pages_insert(loc, p);
		return loc;
	}
	return Pages_insertMax(&((*loc)->next), p);
}

Page Pages_insert(Page* loc, Page p) {
	p->next = *loc;
	*loc = p;
	return p;
}

Page Pages_remove(Page* loc) {
	Page p = *loc;
	*loc = p->next;
	p->next = 0;
	return p;
}
