#ifndef ALLOCATOR_H
#define ALLOCATOR_H

	#include <stdio.h>
	#include <stdlib.h>
	#include <stdarg.h>
	#include <string.h>
	#include "../Types.h"


	typedef			Ptr			(*Cloner)(Ptr value);
	typedef struct		Page*			Page;
	typedef struct		Pool*			Pool;
	typedef struct		Allocator*		Allocator;

	typedef struct		Page {
				Size			free;
				Ptr			current;
				Page			next;
	}						S_Page;

	typedef struct		Pool {
				Page			live;
				Page			ready;
				Size			minalloc;
				Size			maxalloc;
	}						S_Pool;

	typedef struct		Allocator {
				Nat8			skip;
				Page			pages;
				Allocator		next;
	}						S_Allocator;

	Unit			Page_print		(Page this, char* txt, ...);
	Size			Page_full		(Page this);
	Size			Page_size		(Page this);
	Size			Page_used		(Page this);
	Ptr			Page_start		(Page this);
	Ptr			Page_end		(Page this);
	Boo			Page_empty		(Page this);
	Boo			Page_contains		(Page this, Ptr mem);
	Page			Page_init		(Page this, Size size);
	Page			Page_reset		(Page this);


	Unit			Pages_printInfo		(Page p, char* format, ...);
	Unit			Pages_print		(Page p, char* format, ...);
	Nat			Pages_count		(Page p);
	Size			Pages_full		(Page p);
	Size			Pages_size		(Page p);
	Size			Pages_used		(Page p);
	Size			Pages_free		(Page p);
	Page*			Pages_find		(Page* loc, Ptr mem);
	Page*			Pages_insertMin		(Page* loc, Page p);
	Page*			Pages_insertMax		(Page* loc, Page p);
	Page			Pages_insert		(Page* loc, Page p);
	Page			Pages_remove		(Page* loc);

	Unit			Allocator_flush		();
	Unit			Allocator_statistics	();
	Unit			Allocator_push		(Size s);
	Ptr			Allocator_pop		(Cloner cl, Ptr obj);
	Ptr			Allocator_alloc		(Size s);
	Ptr			Allocator_realloc	(Ptr ptr, Size old, Size new);
	Unit			Allocator_free		(Ptr ptr);
	Boo			Allocator_keep		(Ptr ptr);

#endif
