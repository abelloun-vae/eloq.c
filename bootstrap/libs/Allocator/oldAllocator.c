Unit Allocator_print(Allocator a, char* txt) {
	printf("\n##########################\n");
	printf("# txt		: ALLOCATOR : %s\n", txt);
	printf("# addr		: %p\n", a);
	printf("# S_ALLO	: %lu\n", sizeof(S_Allocator));
	printf("# S_PAGE	: %lu\n", sizeof(S_Page));
	printf("# S_REGI	: %lu\n", sizeof(Register));
	printf("# skip		: %u\n", a->skip);
	printf("# busy		: %lu\n", a->busy);
	printf("# end		: %p\n", ((Ptr) a) + sizeof(*a));
	printf("# end		: %p\n", a + 1);
	printf("# frames	: %p\n", a->frames);
	printf("# frames	: %u\n", Page_count(a->frames));
	printf("# total	free	: %lu\n", Page_sumfree(a->frames));
	printf("# total		: %lu\n", Page_sum(a->frames));
	printf("############################\n\n");
}

Allocator Allocator_init(Allocator a, Size size, Nat32 generation) {
	a->skip = 0;
	a->busy = 0;
	a->generation = generation;
	a->frames = Page_init((Ptr) (a + 1), size);
	a->next = 0;
	return a;
}

//#############################################################################
// Allocator
//#############################################################################
Size Allocator_heuristic(Size s) {
	return s;
	return s > pool->minalloc - 200 ? s : pool->minalloc - 200;
	return s > pool->allocator->busy ? s : pool->allocator->busy;
}

Allocator Allocator_make(Size s) {
	return Allocator_init(
		Pool_alloc(pool, sizeof(S_Allocator) + sizeof(S_Page) + sizeof(Register) + s),
		sizeof(Register) + s,
		pool->allocator->generation + 1
	);
}

Unit Allocator_push(Size s) {
	if (pool->allocator->busy < 1024 && pool->allocator->skip < 16) {
		pool->allocator->skip++;
		return;
	}
	//~ printf("\nPush size : %lu", s);
	//~ Allocator_print(pool->allocator, "Allocator_push");
	Allocator nAllocator = Allocator_make(Allocator_heuristic(s));
	nAllocator->next = pool->allocator;
	pool->allocator = nAllocator;
	//~ Allocator_print(pool->allocator, "Allocator_pushed");
}

Ptr Allocator_pop(Cloner cloner, Ptr object) {
	if (pool->allocator->skip) {
		pool->allocator->skip--;
		return object;
	}
	//~ Allocator_print(pool->allocator, "Allocator_pop");
	Allocator oldAllocator = pool->allocator;
	pool->allocator = pool->allocator->next;
	Ptr clone = object ? cloner(object) : 0;
	Page pp;
	Page p = oldAllocator->frames;
	while((pp = p->next)) {
		Pool_free(pool, p);
		p = pp;
	}
	Pool_free(pool, oldAllocator);
	return clone;
}

Ptr Allocator_alleoc(Size s) {
	//~ printf("Allocator_alloc : %lu\n", s);
	//~ Page_print(pool->allocator->frames, "alloc");

	Page page = pool->allocator->frames;
	if ((s + sizeof(Register)) <= page->free)
		page = Page_remove(&(pool->allocator->frames));
	else {
		//~ page = *Page_insertMax(
			//~ Page_init(
				//~ Pool_alloc(pool, sizeof(S_Page) + sizeof(Register) + Allocator_heuristic(s)),
				//~ sizeof(Register) + Allocator_heuristic(s)
			//~ ),
			//~ &(pool->allocator->frames)
		//~ );
		page = Page_init(
			Pool_alloc(pool, sizeof(S_Page) + 2 * sizeof(Register) + Allocator_heuristic(s)),
			2 * sizeof(Register) + Allocator_heuristic(s)
		);
		//~ printf("Size : %lu", Allocator_heuristic(s));
		//~ Allocator_print(pool->allocator, "KO");
		//~ Page_print(page, "KO");
		//~ exit(0);
	}
	void* alloc = page->current;
	Register* reg = alloc;
	reg->generation = pool->allocator->generation;
	reg->size = s;
	page->current += sizeof(Register) + s;

	//~ Page_print(page, "ok");
	Page_insert(page, &(pool->allocator->frames));
	page->free -= sizeof(Register) + s;
	//~ Page_print(page, "ok");
	pool->allocator->busy += s;
	return alloc + sizeof(Register);
	//~ return Pool_alloc(pool, s);
}


Ptr Allocator_alloc(Size s) {

	Ptr		p;
	Register*	r;
	Page		page	= pool->allocator->frames;

	if (page->free >= sizeof(Register) + s)
		page = Page_remove(&(pool->allocator->frames));
	else
		page = Page_init(
			Pool_alloc(pool, sizeof(S_Page) + 2 * sizeof(Register) + Allocator_heuristic(s)),
			2 * sizeof(Register) + Allocator_heuristic(s)
		);

	/* start position of memory segments */
	r = page->current;			// register
	p = page->current + sizeof(Register);	// user memory
	/* set register state */
	r->generation = pool->allocator->generation;
	r->size = s;
	/* update allocator state */
	pool->allocator->busy += s;
	/* update page state */
	page->count++;
	page->free -= sizeof(Register) + s;
	page->current += sizeof(Register) + s;
	Page_insertMax(page, &(pool->allocator->frames));

	/* set next register state */
	r = page->current;
	r->generation = 0;
	r->size = page->free;

	/* return pointer to area of size s */
	return p;
}

Ptr Allocator_realloc(Ptr ptr, Size s, Size copy) {
	Ptr _ptr = Allocator_alloc(s);
	memcpy(_ptr, ptr, copy);
	Allocator_free(ptr);
	return _ptr;
}

//~ Ptr Allocator_realloc(Ptr ptr, size_t newSize, size_t copyLength) {
	//~ Ptr p = Allocator_alloc(newSize);
	//~ memcpy(p, ptr, copyLength);
	//~ return p;
//~ }

Unit Allocator_free(Ptr p) {
	(void) p;
	//~ Pool_free(pool, p);
}

Boo Allocator_keep(Register* r) {
	return r[-1].generation <= pool->allocator->generation;
}

//~ struct {Register reg; S_Value value;} Zero = {
	//~ reg: {generation: 0, size: 0},
	//~ value: {Integer: {id: ValueId_Integer, i: 42}}
//~ };








#include "Allocator.h"

struct {Register reg; S_Value value;} Zero = {
	reg: {generation: 0, size: 0},
	value: {Integer: {id: ValueId_Integer, i: 42}}
};



//#############################################################################
// Allocator
//#############################################################################
void Allocator_push(size_t size) {
	//~ allocations = (allocations + 1) % 4;
	//~ if (allocations)
		//~ return;
	if ( (sizeof(S_Allocator) + sizeof(S_Page) + size) * 2 <= allocator->pages->free )
		Allocator_extends(size);
	else Allocator_make(size);
}


void Allocator_extends(size_t size) {
	void* p = allocator->pages->current + size + sizeof(S_Allocator) + sizeof(S_Page);
	Allocator newAllocator = p;
	newAllocator->generation = 1 + allocator->generation;
	newAllocator->allocations = 0;
	newAllocator->real = 0;
	newAllocator->total = allocator->pages->free - 2 * (size + sizeof(S_Allocator) + sizeof(S_Page));
	newAllocator->pages = p + sizeof(S_Allocator);
	newAllocator->allocators = allocator;
	newAllocator->pages->free = allocator->pages->free - 2 * (size + sizeof(S_Allocator) + sizeof(S_Page));
	newAllocator->pages->current = p + sizeof(S_Allocator) + sizeof(S_Page);
	newAllocator->pages->pages = 0;
	allocator = newAllocator;
}

void Allocator_make(size_t size) {
	//~ size = size > allocator->total ? size : allocator->total;
	//~ size = allocator && allocator->total > size ? allocator->total : size;
	size = size < 1024 ? 1024 : size;
	void* p = malloc(sizeof(S_Allocator) + sizeof(S_Page) + size);
	if (!p) {
		printf("No memory error !");
		exit(1);
	}
	Allocator newAllocator = p;
	newAllocator->generation = 1 + (allocator ? allocator->generation : 0);
	newAllocator->allocations = 0;
	newAllocator->real = 1;
	newAllocator->total = size;
	newAllocator->pages = p + sizeof(S_Allocator);
	newAllocator->allocators = allocator;
	newAllocator->pages->free = size;
	newAllocator->pages->current = p + sizeof(S_Allocator) + sizeof(S_Page);
	newAllocator->pages->pages = 0;
	allocator = newAllocator;
}

Page Allocator_pushPage(size_t size) {
	if (!size)
		return allocator->pages;
	void* p = malloc(sizeof(S_Page) + size);
	if (!p) {
		printf("No memory error !");
		exit(1);
	}
	Page newPage = p;
	newPage->free = size;
	newPage->current = p + sizeof(S_Page);
	newPage->pages = allocator->pages;
	allocator->pages = newPage;
	allocator->total += size;
	return newPage;
}

void* Allocator_alloc(size_t size) {
	if (!size) return 0;
	Page page = allocator->pages;
	while (page && (size + sizeof(Register)) > page->free)
		page = page->pages;
	if(!page)
		page = Allocator_pushPage((sizeof(Register) + size) * 2 + allocator->total + sizeof(Register) * allocator->allocations);
	void* alloc = page->current;
	Register* reg = alloc;
	reg->generation = allocator->generation;
	reg->size = size;
	page->current += sizeof(Register) + size;
	page->free -= sizeof(Register) + size;
	allocator->allocations++;
	return alloc + sizeof(Register);
}

void* Allocator_realloc(void* ptr, size_t newSize, size_t copyLength) {
	void* p = Allocator_alloc(newSize);
	memcpy(p, ptr, copyLength);
	return p;
}
