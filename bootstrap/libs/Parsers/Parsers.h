#ifndef PARSER_H
#define PARSER_H

	#include <stdio.h>
	#include <stdlib.h>
	#include "../Types.h"
	#include "../Strings/Strings.h"

	//############################################################
	typedef struct		PResult			PResult;
	typedef struct		PRule			PRule;
	typedef struct		Parser*			Parser;
	typedef struct		PState*			PState;
	typedef struct		PContext*		PContext;
	typedef struct		PTable*			PTable;
	typedef struct		PCell			PCell;

	typedef struct		Parser			S_Parser;
	typedef struct		PState			S_PState;
	typedef struct		PContext		S_PContext;
	typedef struct		PTable			S_PTable;

	typedef			Ptr			(*PExpr)(PContext context, PState state);
	//############################################################
	Parser			Parser_alloc		(Nat ssize, Nat asize);
	Parser			Parser_init		(Parser this, Nat ssize, Ptr* scope, Nat asize, Ptr* args, Nat gsize, Nat rsize, PRule* rules);
	Parser			Parser_make		(Nat ssize, Ptr* scope, Nat asize, Ptr* args, Nat gsize, Nat rsize, PRule* rules);
	PExpr			Parser_resolve		(Parser p, Nat len, String str);
	Ptr			Parser_invoke		(PContext context, PState state, String str, Nat len);
	PResult			Parser_parse		(Parser parser, PExpr pexpr, Nat length, String input);
	Boo			Parser_parseStream0	(PContext context, PState state, Stream0 stm);
	Boo			Parser_parseString	(PContext context, PState state, Nat len, String str);
	//############################################################
	PTable			PTable_alloc		(Nat size);
	Key32			PTable_hash		(PState state);
	Nat			PTable_add		(PTable table, PState key, PState state, Ptr value);
	PTable			PTable_expand		(PTable table, Nat size);
	Ptr			PTable_store		(PTable* table, PState key, PState state, Ptr value);
	Ptr			PTable_restore		(PTable* table, PState key, PState state);
	Boo			PTable_exists		(PTable* table, PState key);
	//############################################################
	struct PResult {
		Nat		error;
		Nat		position;
		Ptr		result;
	};

	struct PRule {
		Nat		len;
		String		id;
		PExpr		pexpr;
	};

	struct Parser {
		Nat		ssize;
		Ptr*		scope;
		Nat		asize;
		Ptr*		args;
		Nat		gsize;
		Nat		rsize;
		PRule*		rules;
	};

	struct PState {
		Nat		position;
		Nat8		prec;
		Int8		asso;
	};

	struct PContext {
		Parser		parser;
		Nat		length;
		String		input;
		PTable*		memo;
	};

	struct PCell {
		Boo		set;
		S_PState	key;
		S_PState	state;
		Ptr		value;
	};

	struct PTable {
		Nat		size;
		PCell		cells[];
	};
	//############################################################
#endif
