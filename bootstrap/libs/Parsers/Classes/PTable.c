PTable PTable_alloc(Nat size) {
	PTable t = malloc(sizeof(S_PTable) + sizeof(PCell[size]));
	t->size = size;
	for(Nat i = 0; i < size; i++)
		t->cells[i].set = 0;
	return t;
}

PTable PTable_expand(PTable table, Nat size) {
	//printf("######################################\nExpand\n");
	//printf("######################################\n");
	PTable t = PTable_alloc(size);
	for(Nat i = 0; i < table->size; i++)
		if (table->cells[i].set)
			PTable_add(t, &table->cells[i].key, &table->cells[i].state, table->cells[i].value);
	free(table);
	return t;
}

Key32 PTable_hash(PState state) {
	return (state->position << 8) + (state->prec << 2) + (state->asso + 2);
}


Nat PTable_add(PTable table, PState key, PState state, Ptr value) {
	Nat pos;
	Nat size = table->size;
	PCell* cells = table->cells;
	Key32 hash = PTable_hash(key) % size;
	for(Nat i = 0; i < size; i++) {
		pos = (hash + i) % size;
		if (!cells[pos].set) {
			cells[pos] = (PCell) {set: 1, key: *key, state: *state, value: value};
			//printf("ADD Probe : %u\n", i);
			return i;
		} else if ((*key).position == cells[pos].key.position && (*key).prec == cells[pos].key.prec && (*key).asso == cells[pos].key.asso) {
			cells[pos] = (PCell) {set: 1, key: *key, state: *state, value: value};
			//printf("ADD Probe : %u\n", i);
			return i;
		}
	}
	printf("ParserMemo_add panic, can't add new key !");
	exit(1);
	return 0;
}

Ptr PTable_store(PTable* table, PState key, PState state, Ptr value) {
	if (!*table)
		*table = PTable_alloc(32);
	Nat probe = PTable_add(*table, key, state, value);
	if (probe > 5)
		*table = PTable_expand(*table, (*table)->size * 2 + 7);
	return value;
}

Ptr PTable_restore(PTable* table, PState key, PState state) {
	if (!*table)
		*table = PTable_alloc(32);
	PCell c;
	Nat size = (*table)->size;
	PCell* cells = (*table)->cells;
	Key32 hash = PTable_hash(key) % size;
	for(Nat i = 0; i < size; i++) {
		c = cells[(hash + i) % size];
		if (!c.set)
			break;
		if ((*key).position == c.key.position && (*key).prec == c.key.prec && (*key).asso == c.key.asso) {
			*state = c.state;
			//printf("RESTORE Probe : %u\n", i);
			return c.value;
		}
	}
	printf("ParserMemo_restore panic, can't find key !");
	exit(1);
	return 0;
}

Boo PTable_exists(PTable* table, PState key) {
	if (!*table)
		*table = PTable_alloc(32);
	PCell c;
	Nat size = (*table)->size;
	PCell* cells = (*table)->cells;
	Key32 hash = PTable_hash(key) % size;
	for(Nat i = 0; i < size; i++) {
		c = cells[(hash + i) % size];
		if (!c.set) {
			//printf("EXIS Probe : %u\n", i);
			return 0;
		}
		if ((*key).position == c.key.position && (*key).prec == c.key.prec && (*key).asso == c.key.asso) {
			//printf("EXIS Probe : %u\n", i);
			return 1;
		}
	}
	return 0;
}
