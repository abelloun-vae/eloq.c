size_t Parser_sizeof(Nat ssize, Nat asize) {
	return sizeof(S_Parser) + ssize * sizeof(Ptr) + asize * sizeof(Ptr);
}

Parser Parser_alloc(Nat ssize, Nat asize) {
	Ptr pp = malloc(sizeof(S_Parser) + ssize * sizeof(Ptr) + asize * sizeof(Ptr));
	Parser p = pp;
	p->scope = pp + sizeof(S_Parser);
	p->args = pp + sizeof(S_Parser) + ssize * sizeof(Ptr);
	return p;
}

Parser Parser_init(Parser this, Nat ssize, Ptr* scope, Nat asize, Ptr* args, Nat gsize, Nat rsize, PRule* rules) {
	this->ssize = ssize;
	this->asize = asize;
	this->gsize = gsize;
	this->rsize = rsize;
	this->rules = rules;
	for(Nat i = 0; i < ssize; i++)
		this->scope[i] = scope[i];
	for(Nat i = 0; i < asize; i++)
		this->args[i] = args[i];
	return this;
}

Parser Parser_make(Nat ssize, Ptr* scope, Nat asize, Ptr* args, Nat gsize, Nat rsize, PRule* rules) {
	return Parser_init(Parser_alloc(ssize, asize), ssize, scope, asize, args, gsize, rsize, rules);
}

//###########
// Parser utils
//###########
PExpr Parser_resolve(Parser p, Nat len, String str) {
	for(Nat i = 0; i < p->rsize; i++)
		if ( p->rules[i].len == len && String_refl(len, str, p->rules[i].id) )
			return p->rules[i].pexpr;
	printf("Rule not found in parser : \n");
	String_print(str, len);
	exit(1);
	return 0;
}

Ptr Parser_invoke(PContext context, PState state, String str, Nat len) {
	PExpr pe = Parser_resolve(context->parser, len, str);
	return pe(context, state);
}

PResult Parser_parse(Parser parser, PExpr pexpr, Nat length, String input) {

	PTable memo[parser->rsize];
	for(Nat i = 0; i < parser->rsize; i++)
		memo[i] = 0;

	S_PContext context = {
		parser: parser,
		length: length,
		input: input,
		memo: memo,
	};

	S_PState state = {
		position: 0,
		prec: 0,
		asso: 0,
	};

	Ptr result = pexpr(&context, &state);

	if (!result)
		return (PResult) {error: 1, position: state.position, result: 0};
	if (state.position < length)
		return (PResult) {error: 2, position: state.position, result: 0};
	return (PResult) {error: 0, position: state.position, result: result};

}

//###########
// Parsers
//###########
Boo Parser_parseStream0(PContext context, PState state, Stream0 stm) {
	Nat csize;
	while (*stm) {
		if (state->position >= context->length)
			return 0;
		csize = Stream_getNextCharSize(stm);
		if (context->input[state->position] != Stream_getNextChar(stm, csize))
			return 0;
		stm += csize;
		state->position++;
	}
	return 1;
}

Boo Parser_parseString(PContext context, PState state, Nat len, String str) {
	for(Nat i = 0; i < len; i++)
		if (state->position >= context->length || context->input[state->position] != str[i])
			return 0;
		else state->position++;
	return 1;
}

