//################
//# Stream
//################
Writer WriterStream_open(Writer w) {
	w->length = 0;
	w->indent = 0;
	w->writeString = &WriterStream_writeString;
	w->writeStream = &WriterStream_writeStream;
	w->state.stream.max = 0;
	w->state.stream.stm = 0;
	return w;
}

Nat WriterStream_close(Writer w) {
	w->state.stream.stm[w->length] = 0;
	return w->length;
}

void WriterStream_writeString(Writer w, String str, Nat len) {
	Nat csize;
	for(Nat i = 0; i < len; i++) {
		csize = Char_getSize(str[i]);
		for(Nat j = 0; j < csize; j++)
			w->state.stream.stm[w->length++] = 0xFF & ( str[i] >> (j * 8) );
	}
}

void WriterStream_writeStream(Writer w, Stream stm, Nat len) {
	if (w->length + len > w->state.stream.max)
		WriterStream_expand(w, len);
	memcpy(w->state.stream.stm + w->length, stm, len);
	w->length += len;
}

Writer WriterStream_expand(Writer w, Nat len) {
	w->state.stream.stm = Allocator_realloc(w->state.stream.stm, sizeof(Byte[w->state.stream.max + 1]), sizeof(Byte[Writer_heuristic(w, len) + 1]));
	w->state.stream.max = Writer_heuristic(w, len);
	return w;
}

void WriterStream_free(Writer w) {
	free(w->state.stream.stm);
}
