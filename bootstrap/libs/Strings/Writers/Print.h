//################
//# Print
//################
typedef struct	WriterState_Print			WriterState_Print;
struct WriterState_Print {};

Writer WriterPrint_open(Writer writer);
void WriterPrint_writeString(Writer writer, String str, Nat len);
void WriterPrint_writeStream(Writer writer, Stream stm, Nat len);
Nat WriterPrint_close(Writer writer);