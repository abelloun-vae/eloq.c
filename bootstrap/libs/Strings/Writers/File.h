//################
//# File
//################

typedef struct	WriterState_File				WriterState_File;
struct WriterState_File {
				FILE*				fd;
};

Writer WriterFile_open(Writer writer, FILE* file);
void WriterFile_writeString(Writer writer, String s, Nat len);
void WriterFile_writeStream(Writer writer, Stream s, Nat size);
Nat WriterFile_close(Writer writer);
