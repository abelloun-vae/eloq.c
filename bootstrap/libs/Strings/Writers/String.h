//################
//# WriterString
//################
typedef struct		WriterState_String			WriterState_String;
struct			WriterState_String {
			Nat					max;
			String					str;
};

Writer			WriterString_open			(Writer w);
Nat			WriterString_close			(Writer w);
void			WriterString_writeString		(Writer w, String str, Nat len);
void			WriterString_writeStream		(Writer w, Stream stm, Nat len);

Writer			WriterString_expand			(Writer w, Nat len);

