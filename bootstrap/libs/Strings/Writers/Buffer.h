//###############################
typedef struct	WriterState_Buffer			WriterState_Buffer;
struct			WriterState_Buffer {
				Nat						size;
				Nat						cur;
				String					buffer;
				Writer					writer;
};
