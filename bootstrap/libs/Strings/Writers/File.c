//################
//# File
//################
Writer WriterFile_open(Writer writer, FILE* file) {
	writer->length = 0;
	writer->indent = 0;
	writer->writeString = &WriterFile_writeString;
	writer->writeStream = &WriterFile_writeStream;
	writer->state.file.fd = file;
	return writer;
}

void WriterFile_writeString(Writer writer, String s, Nat len) {
	for(Nat i = 0; i < len; i++)
		fwrite(s + i, 1, Char_getSize(s[i]), writer->state.file.fd);
	writer->length += len;
}

void WriterFile_writeStream(Writer writer, Stream stm, Nat size) {
	Nat buf = 4096;
	Nat loop = size / buf;
	Nat rest = size % buf;
	//~ printf("Loops : %u\n", loop);
	//~ printf("Rest : %u\n", rest);
	for(Nat i = 0; i < loop; i++) {
		fwrite(stm, 1, buf, writer->state.file.fd);
		stm += buf;
	}
	if ( rest ) fwrite(stm, 1, rest, writer->state.file.fd);
	//~ fwrite(stm, 1, size, writer->state.file.fd);
	writer->length += size;
}

Nat WriterFile_close(Writer writer) {
	writer->state.file.fd = 0;
	return 0;
}
