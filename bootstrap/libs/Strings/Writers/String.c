//################
//# String
//################
Writer WriterString_open(Writer w) {
	w->length = 0;
	w->indent = 0;
	w->writeString = &WriterString_writeString;
	w->writeStream = &WriterString_writeStream;
	w->state.string.max = 0;
	w->state.string.str = 0;
	return w;
}

Nat WriterString_close(Writer w) {
	return w->length;
}

void WriterString_writeString(Writer w, String str, Nat len) {
	if (w->length + len > w->state.string.max)
		WriterString_expand(w, len);
	memcpy(w->state.string.str + w->length, str, len * 4);
	w->length += len;
}

void WriterString_writeStream(Writer w, Stream stm, Nat len) {
	Nat csize;
	if (w->length + len > w->state.string.max)
		WriterString_expand(w, len);
	for(Nat i = 0; i < len; i += csize) {
		csize = Stream_getNextCharSize(stm);
		w->state.string.str[w->length++] = Stream_getNextChar(stm, csize);
		stm += csize;
	}
}

Writer WriterString_expand(Writer w, Nat len) {
	w->state.string.str = Allocator_realloc(w->state.string.str, sizeof(Char[w->state.string.max]), sizeof(Char[Writer_heuristic(w, len)]));
	w->state.string.max = Writer_heuristic(w, len);
	return w;
}
