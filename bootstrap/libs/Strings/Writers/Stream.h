//################
//# WriterString
//################
typedef struct		WriterState_Stream			WriterState_Stream;
struct			WriterState_Stream {
			Nat					max;
			Stream					stm;
};

Writer			WriterStream_open			(Writer w);
Nat			WriterStream_close			(Writer w);
void			WriterStream_free			(Writer w);
void			WriterStream_writeString		(Writer w, String str, Nat len);
void			WriterStream_writeStream		(Writer w, Stream stm, Nat len);

Writer			WriterStream_expand			(Writer w, Nat len);
