//################
//# Print
//################
Writer WriterPrint_open(Writer writer) {
	writer->length = 0;
	writer->indent = 0;
	writer->writeString = &WriterPrint_writeString;
	writer->writeStream = &WriterPrint_writeStream;
	return writer;
}

void WriterPrint_writeString(Writer writer, String str, Nat len) {
	String_print(str, len);
	writer->length += len;
}

void WriterPrint_writeStream(Writer writer, Stream stm, Nat len) {
	Stream_print(stm, len);
	writer->length += len;
}

Nat WriterPrint_close(Writer writer) {
	return writer->length;
}
