//################
//# Char
//################
Nat Char_getSize(Char c) {
	return c < 128 ? 1 : 2;
}

Byte Char_getByte(Char c, Nat i) {
	return 0xFF & (c >> (i * 8));
}

Nat Char_print(Char c) {
	Byte b;
	for(Nat i = 0; i < 4; i++) {
		b = 0xFF & (c >> (i * 8));
		if (b) printf("%c", b);
		else break;
	}
	return 1;
}
