//################
//# Stream
//################
Nat Stream_print(Stream s, Nat size) {
	printf("%.*s", size, (char*) s);
	return size;
}

Nat Stream_getNextCharSize(Stream s) {
	return *s < 127 ? 1 : 2;
}

Char Stream_getNextChar(Stream s, Nat size) {
	Char c = 0;
	for(Nat i = 0; i < size; i++)
		c+= s[i] << 8 * i;
	return c;
}

Boo Stream_refl(Nat size, Stream l, Stream r) {
	for(Nat i = 0; i < size; i++, l++, r++)
		if (*l != *r) return 0;
	return 1;
}
