//################
//# Stream0
//################
Nat Stream0_print(Stream0 s) {
	printf("%s", (char*) s);
	return Stream0_size(s);
}

Nat Stream0_size(Stream0 s) {
	Stream0 t = s;
	while(*t) t++;
	return t - s;
}

Boo Stream0_refl(Stream0 l, Stream0 r) {
	do {
		if (*l != *r)
			return 0;
		if (!*l)
			return 1;
		l++;
		r++;
	} while(1);
}
