//################
//# String
//################
Nat String_print(String s, Nat len) {
	for(Nat i = 0; i < len; i++)
		Char_print(s[i]);
	return len;
}

Boo String_refl(Nat length, String l, String r) {
	for(Nat i = 0; i < length; i++, l++, r++)
		if (*l != *r) return 0;
	return 1;
}
