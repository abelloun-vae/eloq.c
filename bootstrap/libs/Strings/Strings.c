//################
//# Strings
//################
#include "Strings.h"
#include "Classes/Char.c"
#include "Classes/String.c"
#include "Classes/Stream.c"
#include "Classes/Stream0.c"
#include "Writers/Stream.c"
#include "Writers/String.c"
#include "Writers/Buffer.c"
#include "Writers/Print.c"
#include "Writers/File.c"

Nat Writer_heuristic(Writer w, Nat len) {
	return w->length + len;
}

//################
void Writer_indent(Writer w, Int indent) {
	w->indent += indent;
}
//################
//# Write
//################
void Writer_writeStream0(Writer w, Stream0 stm) {
	w->writeStream(w, stm, Stream0_size(stm));
}

void Writer_writeStream(Writer w, Stream stm, Nat size) {
	w->writeStream(w, stm, size);
}

void Writer_writeChar(Writer w, Char c) {
	w->writeString(w, &c, 1);
}

void Writer_writeString(Writer w, String str, Nat len) {
	w->writeString(w, str, len);
}

void Writer_writeLine(Writer w) {
	Writer_writeChar(w, '\n');
	for(Nat i = 0; i < w->indent; i++)
		Writer_writeChar(w, '\t');
}

void Writer_writeInteger(Writer w, Int n) {
	Nat len = 0;
	Char buff[32];
	if (n < 0) {
		Writer_writeChar(w, '-');
		n = -n;
	}
	do {
		buff[len++] = '0' + n % 10;
		n /= 10;
	} while(n);
	while(len--)
		Writer_writeChar(w, buff[len]);
}

void Writer_writeByte(Writer w, Byte b) {
	Writer_writeChar(w, b);
}

void Writer_writeFile(Writer w, FILE* fd, Nat blen) {
	Byte buff[blen];
	Nat wlen;
	do {
		wlen = fread(buff, 1, blen, fd);
		w->writeStream(w, buff, wlen);
	} while(wlen);
}
