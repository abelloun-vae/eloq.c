#ifndef STRINGS_H
#define STRINGS_H

	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "../Types.h"
	#include "../Allocator/Allocator.h"
	//###############################
	typedef struct		Writer*					Writer;
	typedef struct		Writer					S_Writer;
	typedef union		WriterState				WriterState;
	typedef			void					(*Writer_writerString)		(Writer w, String str, Nat len);
	typedef			void					(*Writer_writerStream)		(Writer w, Stream stm, Nat len);
	typedef			Nat					(*Writer_closer)		(Writer w);
	//###############################
	#include "Writers/Stream.h"
	#include "Writers/String.h"
	#include "Writers/Buffer.h"
	#include "Writers/Print.h"
	#include "Writers/File.h"
	//###############################
	union WriterState {
				WriterState_Print			print;
				WriterState_String			string;
				WriterState_Stream			stream;
				WriterState_Buffer			buffer;
				WriterState_File			file;
	};
	//###############################
	struct Writer {
				Nat					indent;
				Nat					length;
				Writer_writerString			writeString;
				Writer_writerStream			writeStream;
				WriterState				state;
	};
	//###############################

	Nat			Writer_heuristic			(Writer w, Nat len);
	void			Writer_indent				(Writer w, Int indent);
	void			Writer_writeStream0			(Writer w, Stream0 stm);
	void			Writer_writeStream			(Writer w, Stream stm, Nat size);
	void			Writer_writeChar			(Writer w, Char c);
	void			Writer_writeString			(Writer w, String str, Nat len);
	void			Writer_writeLine			(Writer w);
	void			Writer_writeInteger			(Writer w, Int n);
	void			Writer_writeByte			(Writer w, Byte b);
	void			Writer_writeFile			(Writer w, FILE* fd, Nat blen);
	//###############################
	//# String
	//###############################
	Nat			Char_print				(Char c);
	Nat			Char_size				(Char c);
	Byte			Char_getByte				(Char c, Nat i);
	//###############################
	//# String
	//###############################
	Boo			String_refl				(Nat len, String l, String r);
	Nat			String_print				(String s, Nat len);
	//###############################
	//#
	//###############################
	Boo			Stream_refl				(Nat size, Stream l, Stream r);
	Nat			Stream_print				(Stream s, Nat size);
	Nat			Stream_getNextCharSize			(Stream s);
	Char			Stream_getNextChar			(Stream s, Nat size);
	//###############################
	//#
	//###############################
	Boo			Stream0_refl				(Stream0 l, Stream0 r);
	Nat			Stream0_print				(Stream0 s);
	Nat			Stream0_size				(Stream0 s);

#endif
