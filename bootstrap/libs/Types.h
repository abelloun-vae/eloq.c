#ifndef TYPES_H
#define TYPES_H

	#include <stdbool.h>
	#include <stdint.h>

	//###########
	// Types :)
	//###########
	typedef void*					Ptr;
	//###########
	typedef void					Unit;
	typedef bool					Boo;
	//##########################################################
	typedef unsigned char				Byte;
	typedef Byte*					Stream;
	typedef Byte*					Stream0;
	//###########################
	typedef uint32_t				Char;
	typedef Char*					String;
	//##########################################################
	typedef uint8_t					Bits8;
	typedef uint16_t				Bits16;
	typedef uint32_t				Bits32;
	typedef uint64_t				Bits64;
	//##########################################################
	typedef unsigned int				Nat;
	typedef uint8_t					Nat8;
	typedef uint16_t				Nat16;
	typedef uint32_t				Nat32;
	typedef uint64_t				Nat64;
	//###########################
	typedef int64_t					Int;
	typedef int8_t					Int8;
	typedef int16_t					Int16;
	typedef int32_t					Int32;
	typedef int64_t					Int64;
	//##########################################################
	typedef double					Flt;
	//##########################################################
	typedef uint8_t					Key8;
	typedef uint16_t				Key16;
	typedef uint32_t				Key32;
	typedef uint64_t				Key64;
	//##########################################################
	typedef size_t					Size;

#endif
