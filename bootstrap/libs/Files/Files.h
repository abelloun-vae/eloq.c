#ifndef FILES_H
#define FILES_H

	#include <stdio.h>
	#include <stdlib.h>
	#include "../Types.h"
	#include "../Strings/Strings.h"

	FILE* File_open(String file, Nat len, char* mode);

#endif
