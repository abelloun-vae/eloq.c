//################
//#
//################
#include "Files.h"

FILE* File_open(String file, Nat len, char* mode) {
	S_Writer wpath;
	WriterStream_open(&wpath);
	WriterStream_expand(&wpath, 128);
	Writer_writeString(&wpath, file, len);
	WriterStream_close(&wpath);
	FILE* fd = fopen((char*)wpath.state.stream.stm, mode);
	WriterStream_free(&wpath);
	return fd;
}
