#ifndef BOOTSTRAP_H
#define BOOTSTRAP_H

	//######################
	// Body
	//######################
	void		ValueName_writeName			(ValueName v, Writer w);

	//######################
	// lifted lambdas
	//######################
	Value _lambda__aro__dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0(Values scope, Value _variable_i);
	Value _lambda__aro__dot_0_dot_0_dot_0_dot_0_dot_0_dot_0(Values scope, Value _variable_n);
	Value _lambda__aro__dot_0_dot_0_dot_0_dot_0_dot_1_dot_0(Values scope, Value _variable_n);
	Value all__sco__aro__dot_0_dot_0_dot_0_dot_0(Value _variable_implementations);
	void out__sco__aro__dot_0_dot_0_dot_0_dot_0(Value* scope );
	Value e__sco__aro__dot_0_dot_0_dot_0_dot_0(Value _variable_implementations, Value _variable_arr);
	Value _lambda__aro__dot_0_dot_0(Values scope, Value _variable_x);

	//######################
	// Lambda
	//######################
	Value lambda(Value _variable_implementations);

#endif
