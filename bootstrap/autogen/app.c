#include "../main.h"
//###########################################################
// Body
//###########################################################
void ValueName_writeName(ValueName v, Writer w) {
	switch (v->name) {
		
		
		
		default: Writer_writeStream0(w, (Stream0) "UNKNOWN");
	}
}

//###########
// lifted lambdas
//###########
Value _lambda__aro__dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0(Values scope, Value _variable_i) {
	Value _variable_n = scope.values[0];
	Value _variable_sum = scope.values[1];
	Allocator_push(0);
	Value result = (Value_toBoo(_variable_n) ? Ast_apply(
		Ast_apply(
			_variable_sum,
			Ast_dif(_variable_n, ValueInteger_make(1))
		),
		Ast_add(_variable_i, _variable_n)
	) : _variable_i);
	result = Allocator_pop(&Value_escape, result);
	return result;
}

Value _lambda__aro__dot_0_dot_0_dot_0_dot_0_dot_0_dot_0(Values scope, Value _variable_n) {
	Value _variable_sum = scope.values[0];
	Allocator_push(0);
	Value result = ValueClosure_make(&_lambda__aro__dot_0_dot_0_dot_0_dot_0_dot_0_dot_0_dot_0, (Values) {2, (Value[]) {_variable_n, _variable_sum}});
	result = Allocator_pop(&Value_escape, result);
	return result;
}

Value _lambda__aro__dot_0_dot_0_dot_0_dot_0_dot_1_dot_0(Values scope, Value _variable_n) {
	Value _variable_arr = scope.values[0];
	Allocator_push(0);
	Value result = (Value_toBoo(_variable_n) ? Ast_concat(ValueArray_make(0, (Values) {1, (Value[]) {ValueInteger_make(1)}}), Ast_apply(
		_variable_arr,
		Ast_dif(_variable_n, ValueInteger_make(1))
	)) : ValueArray_make(0, (Values) {0, 0}));
	result = Allocator_pop(&Value_escape, result);
	return result;
}

Value all__sco__aro__dot_0_dot_0_dot_0_dot_0(Value _variable_implementations) {
	Value scope[2];
	out__sco__aro__dot_0_dot_0_dot_0_dot_0(scope );
	return e__sco__aro__dot_0_dot_0_dot_0_dot_0(_variable_implementations, scope[1]);
}

void out__sco__aro__dot_0_dot_0_dot_0_dot_0(Value* scope ) {
	Value _variable_sum = (Value) ValueClosure_alloc(1);
	Value _variable_arr = (Value) ValueClosure_alloc(1);
	ValueClosure_init((ValueClosure) _variable_sum, &_lambda__aro__dot_0_dot_0_dot_0_dot_0_dot_0_dot_0, (Values) {1, (Value[]) {_variable_sum}});
	ValueClosure_init((ValueClosure) _variable_arr, &_lambda__aro__dot_0_dot_0_dot_0_dot_0_dot_1_dot_0, (Values) {1, (Value[]) {_variable_arr}});
	scope[0] = _variable_sum;
	scope[1] = _variable_arr;
}

Value e__sco__aro__dot_0_dot_0_dot_0_dot_0(Value _variable_implementations, Value _variable_arr) {
	return Ast_apply(
		Ast_access(_variable_implementations, readStream0((Stream0) "array_length")),
		Ast_apply(
			_variable_arr,
			ValueInteger_make(20000)
		)
	);
}

Value _lambda__aro__dot_0_dot_0(Values scope, Value _variable_x) {
	Value _variable_implementations = scope.values[0];
	Allocator_push(0);
	Value result = (all__sco__aro__dot_0_dot_0_dot_0_dot_0(_variable_implementations));
	result = Allocator_pop(&Value_escape, result);
	return result;
}

//######################
// Lambda
//######################
Value lambda(Value _variable_implementations) {
	return ValueClosure_make(&_lambda__aro__dot_0_dot_0, (Values) {1, (Value[]) {_variable_implementations}});
}
