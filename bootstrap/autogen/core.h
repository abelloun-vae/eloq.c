#ifndef CORE_H
#define CORE_H

	//###########################################################
	//
	//###########################################################
	typedef enum		Value_WriterMode {
				Value_WriterMode_serial,
				Value_WriterMode_string,
				Value_WriterMode_info,
	}							Value_WriterMode;

	typedef enum		ObjectId {
				ObjectId_Parser,
				ObjectId_Grammar,
				ObjectId_Template,
	}							ObjectId;

	//###########
	// Ids
	//###########
	typedef enum		ValueId {
				ValueId_MIN_,
				ValueId_Boolean,
				ValueId_Integer,
				ValueId_Resource,
				ValueId_Byte,
				ValueId_Stream,
				ValueId_Char,
				ValueId_String,
				ValueId_Nil,
				ValueId_Cons,
				ValueId_Array,
				ValueId_Map,
				ValueId_Name,
				ValueId_Closure,
				ValueId_Lambda,
				ValueId_Object,
				ValueId_MAX_,
	}							ValueId;

	//###########
	// Abstract Value
	//###########
	typedef union		Value*				Value;
	typedef struct		Values				Values;
	typedef struct		Map				Map;
	typedef			Value				(*Closure)(Values scope, Value arg);
	typedef			Value				(*Lambda)(Values args);

	#define			LOOP(n, i)			for(Nat __tot = n, i = 0; i < __tot; i++)
	//###########################################################
	#define			Values_COPTR(x)			((Ptr) x.values)
	#define			Values_COALLOC(x, p, s)		x.size = s; x.values = (Value*) (p)
	#define			Values_COPY(x, y)		y.size = x.size; LOOP(x.size, __i) y.values[__i] = x.values[__i]
	typedef			Value*				Values_ALLOCTYPE;
	typedef			Nat				Values_FIELDS_size;
	typedef			Value*				Values_FIELDS_values;
	struct			Values {
				Nat				size;
				Value*				values;
	};

	//###########################################################
	#define			Map_COPTR(x)			((Ptr) x.pairs)
	#define			Map_COALLOC(x, p, s)		x.size = s; x.pairs = (Value(*)[2]) (p)
	#define			Map_COPY(x, y)			y.size = x.size; LOOP(x.size, __i) {\
									y.pairs[__i][0] = x.pairs[__i][0];\
									if (y.pairs[__i][0]) y.pairs[__i][1] = x.pairs[__i][1];\
								}
	typedef			Value				Map_ALLOCTYPE[2];
	typedef			Nat				Map_FIELDS_size;
	typedef			Value				(*Map_FIELDS_pairs)[2];
	struct			Map {
				Nat				size;
				Value				(*pairs)[2];
	};

	//###########
	// Types
	//###########
	typedef struct		ValueBoolean*			ValueBoolean;
	typedef struct		ValueInteger*			ValueInteger;
	typedef struct		ValueResource*			ValueResource;
	typedef struct		ValueByte*			ValueByte;
	typedef struct		ValueStream*			ValueStream;
	typedef struct		ValueChar*			ValueChar;
	typedef struct		ValueString*			ValueString;
	typedef struct		ValueNil*			ValueNil;
	typedef struct		ValueCons*			ValueCons;
	typedef struct		ValueArray*			ValueArray;
	typedef struct		ValueMap*			ValueMap;
	typedef struct		ValueName*			ValueName;
	typedef struct		ValueClosure*			ValueClosure;
	typedef struct		ValueLambda*			ValueLambda;
	typedef struct		ValueObject*			ValueObject;

	//######################
	// Value
	//######################
	void			Value_writeName			(Value v, Writer w);
	void			Value_printSizes			();

	//###########################################################
	// Classes Definitions
	//###########################################################
	//######################
	// Class Boolean
	//######################
	typedef struct		ValueBoolean {
				ValueId				id;
				Boo				b;
	}							S_ValueBoolean;
	typedef			Boo				ValueBoolean_FIELDS_b;
	//########### METHODS ###########
	Value			ValueBoolean_make  		(Boo b);
	size_t			ValueBoolean_sizeof		();
	ValueBoolean		ValueBoolean_alloc 		();
	Value			ValueBoolean_init  		(ValueBoolean this, Boo b);
	ValueBoolean		ValueBoolean_realloc		(ValueBoolean this);
	ValueBoolean		ValueBoolean_coalloc		(ValueBoolean this, Byte* _ptr);
	//###########  ###########
	void			ValueBoolean_check 		(Value v);
	void			ValueBoolean_free  		(ValueBoolean this);
	Value			ValueBoolean_clone 		(ValueBoolean this);
	Value			ValueBoolean_copy  		(ValueBoolean src, ValueBoolean tgt);
	Value			ValueBoolean_fill  		(ValueBoolean src, ValueBoolean tgt);
	
	
	//######################
	// Class Integer
	//######################
	typedef struct		ValueInteger {
				ValueId				id;
				Int				i;
	}							S_ValueInteger;
	typedef			Int				ValueInteger_FIELDS_i;
	//########### METHODS ###########
	Value			ValueInteger_make  		(Int i);
	size_t			ValueInteger_sizeof		();
	ValueInteger		ValueInteger_alloc 		();
	Value			ValueInteger_init  		(ValueInteger this, Int i);
	ValueInteger		ValueInteger_realloc		(ValueInteger this);
	ValueInteger		ValueInteger_coalloc		(ValueInteger this, Byte* _ptr);
	//###########  ###########
	void			ValueInteger_check 		(Value v);
	void			ValueInteger_free  		(ValueInteger this);
	Value			ValueInteger_clone 		(ValueInteger this);
	Value			ValueInteger_copy  		(ValueInteger src, ValueInteger tgt);
	Value			ValueInteger_fill  		(ValueInteger src, ValueInteger tgt);
	
	
	//######################
	// Class Resource
	//######################
	typedef struct		ValueResource {
				ValueId				id;
				Ptr				r;
	}							S_ValueResource;
	typedef			Ptr				ValueResource_FIELDS_r;
	//########### METHODS ###########
	Value			ValueResource_make  		(Ptr r);
	size_t			ValueResource_sizeof		();
	ValueResource		ValueResource_alloc 		();
	Value			ValueResource_init  		(ValueResource this, Ptr r);
	ValueResource		ValueResource_realloc		(ValueResource this);
	ValueResource		ValueResource_coalloc		(ValueResource this, Byte* _ptr);
	//###########  ###########
	void			ValueResource_check 		(Value v);
	void			ValueResource_free  		(ValueResource this);
	Value			ValueResource_clone 		(ValueResource this);
	Value			ValueResource_copy  		(ValueResource src, ValueResource tgt);
	Value			ValueResource_fill  		(ValueResource src, ValueResource tgt);
	
	
	//######################
	// Class Byte
	//######################
	typedef struct		ValueByte {
				ValueId				id;
				Byte				b;
	}							S_ValueByte;
	typedef			Byte				ValueByte_FIELDS_b;
	//########### METHODS ###########
	Value			ValueByte_make  		(Byte b);
	size_t			ValueByte_sizeof		();
	ValueByte		ValueByte_alloc 		();
	Value			ValueByte_init  		(ValueByte this, Byte b);
	ValueByte		ValueByte_realloc		(ValueByte this);
	ValueByte		ValueByte_coalloc		(ValueByte this, Byte* _ptr);
	//###########  ###########
	void			ValueByte_check 		(Value v);
	void			ValueByte_free  		(ValueByte this);
	Value			ValueByte_clone 		(ValueByte this);
	Value			ValueByte_copy  		(ValueByte src, ValueByte tgt);
	Value			ValueByte_fill  		(ValueByte src, ValueByte tgt);
	
	
	//######################
	// Class Stream
	//######################
	typedef struct		ValueStream {
				ValueId				id;
				Nat				len;
				Stream				stm;
	}							S_ValueStream;
	typedef			Nat				ValueStream_FIELDS_len;
	typedef			Stream				ValueStream_FIELDS_stm;
	//########### METHODS ###########
	Value			ValueStream_make  		(Nat len, Stream stm);
	size_t			ValueStream_sizeof		();
	ValueStream		ValueStream_alloc 		();
	Value			ValueStream_init  		(ValueStream this, Nat len, Stream stm);
	ValueStream		ValueStream_realloc		(ValueStream this);
	ValueStream		ValueStream_coalloc		(ValueStream this, Byte* _ptr);
	//###########  ###########
	void			ValueStream_check 		(Value v);
	void			ValueStream_free  		(ValueStream this);
	Value			ValueStream_clone 		(ValueStream this);
	Value			ValueStream_copy  		(ValueStream src, ValueStream tgt);
	Value			ValueStream_fill  		(ValueStream src, ValueStream tgt);
	
	
	//######################
	// Class Char
	//######################
	typedef struct		ValueChar {
				ValueId				id;
				Char				c;
	}							S_ValueChar;
	typedef			Char				ValueChar_FIELDS_c;
	//########### METHODS ###########
	Value			ValueChar_make  		(Char c);
	size_t			ValueChar_sizeof		();
	ValueChar		ValueChar_alloc 		();
	Value			ValueChar_init  		(ValueChar this, Char c);
	ValueChar		ValueChar_realloc		(ValueChar this);
	ValueChar		ValueChar_coalloc		(ValueChar this, Byte* _ptr);
	//###########  ###########
	void			ValueChar_check 		(Value v);
	void			ValueChar_free  		(ValueChar this);
	Value			ValueChar_clone 		(ValueChar this);
	Value			ValueChar_copy  		(ValueChar src, ValueChar tgt);
	Value			ValueChar_fill  		(ValueChar src, ValueChar tgt);
	
	
	//######################
	// Class String
	//######################
	typedef struct		ValueString {
				ValueId				id;
				Nat				len;
				String				str;
	}							S_ValueString;
	typedef			Nat				ValueString_FIELDS_len;
	typedef			String				ValueString_FIELDS_str;
	//########### METHODS ###########
	Value			ValueString_make  		(Nat len, String str);
	size_t			ValueString_sizeof		();
	ValueString		ValueString_alloc 		();
	Value			ValueString_init  		(ValueString this, Nat len, String str);
	ValueString		ValueString_realloc		(ValueString this);
	ValueString		ValueString_coalloc		(ValueString this, Byte* _ptr);
	//###########  ###########
	void			ValueString_check 		(Value v);
	void			ValueString_free  		(ValueString this);
	Value			ValueString_clone 		(ValueString this);
	Value			ValueString_copy  		(ValueString src, ValueString tgt);
	Value			ValueString_fill  		(ValueString src, ValueString tgt);
	
	
	//######################
	// Class Nil
	//######################
	typedef struct		ValueNil {
				ValueId				id;
				
	}							S_ValueNil;
	
	//########### METHODS ###########
	Value			ValueNil_make  		();
	size_t			ValueNil_sizeof		();
	ValueNil		ValueNil_alloc 		();
	Value			ValueNil_init  		(ValueNil this);
	ValueNil		ValueNil_realloc		(ValueNil this);
	ValueNil		ValueNil_coalloc		(ValueNil this, Byte* _ptr);
	//###########  ###########
	void			ValueNil_check 		(Value v);
	void			ValueNil_free  		(ValueNil this);
	Value			ValueNil_clone 		(ValueNil this);
	Value			ValueNil_copy  		(ValueNil src, ValueNil tgt);
	Value			ValueNil_fill  		(ValueNil src, ValueNil tgt);
	
	
	//######################
	// Class Cons
	//######################
	typedef struct		ValueCons {
				ValueId				id;
				Value				head;
				Value				rest;
	}							S_ValueCons;
	typedef			Value				ValueCons_FIELDS_head;
	typedef			Value				ValueCons_FIELDS_rest;
	//########### METHODS ###########
	Value			ValueCons_make  		(Value head, Value rest);
	size_t			ValueCons_sizeof		();
	ValueCons		ValueCons_alloc 		();
	Value			ValueCons_init  		(ValueCons this, Value head, Value rest);
	ValueCons		ValueCons_realloc		(ValueCons this);
	ValueCons		ValueCons_coalloc		(ValueCons this, Byte* _ptr);
	//###########  ###########
	void			ValueCons_check 		(Value v);
	void			ValueCons_free  		(ValueCons this);
	Value			ValueCons_clone 		(ValueCons this);
	Value			ValueCons_copy  		(ValueCons src, ValueCons tgt);
	Value			ValueCons_fill  		(ValueCons src, ValueCons tgt);
	
	
	//######################
	// Class Array
	//######################
	typedef struct		ValueArray {
				ValueId				id;
				Boo				tuple;
				Values				data;
	}							S_ValueArray;
	typedef			Boo				ValueArray_FIELDS_tuple;
	typedef			Values				ValueArray_FIELDS_data;
	//########### METHODS ###########
	Value			ValueArray_make  		(Boo tuple, Values data);
	size_t			ValueArray_sizeof		(Values_FIELDS_size data_size);
	ValueArray		ValueArray_alloc 		(Values_FIELDS_size data_size);
	Value			ValueArray_init  		(ValueArray this, Boo tuple, Values data);
	ValueArray		ValueArray_realloc		(ValueArray this, Values_FIELDS_size data_size);
	ValueArray		ValueArray_coalloc		(ValueArray this, Byte* _ptr, Values_FIELDS_size data_size);
	//###########  ###########
	void			ValueArray_check 		(Value v);
	void			ValueArray_free  		(ValueArray this);
	Value			ValueArray_clone 		(ValueArray this);
	Value			ValueArray_copy  		(ValueArray src, ValueArray tgt);
	Value			ValueArray_fill  		(ValueArray src, ValueArray tgt);
	
	
	//######################
	// Class Map
	//######################
	typedef struct		ValueMap {
				ValueId				id;
				Boo				record;
				Map				map;
				Nat				count;
	}							S_ValueMap;
	typedef			Boo				ValueMap_FIELDS_record;
	typedef			Map				ValueMap_FIELDS_map;
	typedef			Nat				ValueMap_FIELDS_count;
	//########### METHODS ###########
	Value			ValueMap_make  		(Boo record, Map map);
	size_t			ValueMap_sizeof		(Map_FIELDS_size map_size);
	ValueMap		ValueMap_alloc 		(Map_FIELDS_size map_size);
	Value			ValueMap_init  		(ValueMap this, Boo record, Map map);
	ValueMap		ValueMap_realloc		(ValueMap this, Map_FIELDS_size map_size);
	ValueMap		ValueMap_coalloc		(ValueMap this, Byte* _ptr, Map_FIELDS_size map_size);
	//###########  ###########
	void			ValueMap_check 		(Value v);
	void			ValueMap_free  		(ValueMap this);
	Value			ValueMap_clone 		(ValueMap this);
	Value			ValueMap_copy  		(ValueMap src, ValueMap tgt);
	Value			ValueMap_fill  		(ValueMap src, ValueMap tgt);
	
	
	//######################
	// Class Name
	//######################
	typedef struct		ValueName {
				ValueId				id;
				NameId				name;
				Nat				arity;
				Values				data;
	}							S_ValueName;
	typedef			NameId				ValueName_FIELDS_name;
	typedef			Nat				ValueName_FIELDS_arity;
	typedef			Values				ValueName_FIELDS_data;
	//########### METHODS ###########
	Value			ValueName_make  		(NameId name, Nat arity, Values data);
	size_t			ValueName_sizeof		(Values_FIELDS_size data_size);
	ValueName		ValueName_alloc 		(Values_FIELDS_size data_size);
	Value			ValueName_init  		(ValueName this, NameId name, Nat arity, Values data);
	ValueName		ValueName_realloc		(ValueName this, Values_FIELDS_size data_size);
	ValueName		ValueName_coalloc		(ValueName this, Byte* _ptr, Values_FIELDS_size data_size);
	//###########  ###########
	void			ValueName_check 		(Value v);
	void			ValueName_free  		(ValueName this);
	Value			ValueName_clone 		(ValueName this);
	Value			ValueName_copy  		(ValueName src, ValueName tgt);
	Value			ValueName_fill  		(ValueName src, ValueName tgt);
	
	
	//######################
	// Class Closure
	//######################
	typedef struct		ValueClosure {
				ValueId				id;
				Closure				closure;
				Values				scope;
	}							S_ValueClosure;
	typedef			Closure				ValueClosure_FIELDS_closure;
	typedef			Values				ValueClosure_FIELDS_scope;
	//########### METHODS ###########
	Value			ValueClosure_make  		(Closure closure, Values scope);
	size_t			ValueClosure_sizeof		(Values_FIELDS_size scope_size);
	ValueClosure		ValueClosure_alloc 		(Values_FIELDS_size scope_size);
	Value			ValueClosure_init  		(ValueClosure this, Closure closure, Values scope);
	ValueClosure		ValueClosure_realloc		(ValueClosure this, Values_FIELDS_size scope_size);
	ValueClosure		ValueClosure_coalloc		(ValueClosure this, Byte* _ptr, Values_FIELDS_size scope_size);
	//###########  ###########
	void			ValueClosure_check 		(Value v);
	void			ValueClosure_free  		(ValueClosure this);
	Value			ValueClosure_clone 		(ValueClosure this);
	Value			ValueClosure_copy  		(ValueClosure src, ValueClosure tgt);
	Value			ValueClosure_fill  		(ValueClosure src, ValueClosure tgt);
	
	
	//######################
	// Class Lambda
	//######################
	typedef struct		ValueLambda {
				ValueId				id;
				Lambda				lambda;
				Nat				arity;
				Values				args;
	}							S_ValueLambda;
	typedef			Lambda				ValueLambda_FIELDS_lambda;
	typedef			Nat				ValueLambda_FIELDS_arity;
	typedef			Values				ValueLambda_FIELDS_args;
	//########### METHODS ###########
	Value			ValueLambda_make  		(Lambda lambda, Nat arity, Values args);
	size_t			ValueLambda_sizeof		(Values_FIELDS_size args_size);
	ValueLambda		ValueLambda_alloc 		(Values_FIELDS_size args_size);
	Value			ValueLambda_init  		(ValueLambda this, Lambda lambda, Nat arity, Values args);
	ValueLambda		ValueLambda_realloc		(ValueLambda this, Values_FIELDS_size args_size);
	ValueLambda		ValueLambda_coalloc		(ValueLambda this, Byte* _ptr, Values_FIELDS_size args_size);
	//###########  ###########
	void			ValueLambda_check 		(Value v);
	void			ValueLambda_free  		(ValueLambda this);
	Value			ValueLambda_clone 		(ValueLambda this);
	Value			ValueLambda_copy  		(ValueLambda src, ValueLambda tgt);
	Value			ValueLambda_fill  		(ValueLambda src, ValueLambda tgt);
	
	
	//######################
	// Class Object
	//######################
	typedef struct		ValueObject {
				ValueId				id;
				ObjectId				objectId;
				Ptr				object;
				Nat				arity;
				Values				args;
	}							S_ValueObject;
	typedef			ObjectId				ValueObject_FIELDS_objectId;
	typedef			Ptr				ValueObject_FIELDS_object;
	typedef			Nat				ValueObject_FIELDS_arity;
	typedef			Values				ValueObject_FIELDS_args;
	//########### METHODS ###########
	Value			ValueObject_make  		(ObjectId objectId, Ptr object, Nat arity, Values args);
	size_t			ValueObject_sizeof		(Values_FIELDS_size args_size);
	ValueObject		ValueObject_alloc 		(Values_FIELDS_size args_size);
	Value			ValueObject_init  		(ValueObject this, ObjectId objectId, Ptr object, Nat arity, Values args);
	ValueObject		ValueObject_realloc		(ValueObject this, Values_FIELDS_size args_size);
	ValueObject		ValueObject_coalloc		(ValueObject this, Byte* _ptr, Values_FIELDS_size args_size);
	//###########  ###########
	void			ValueObject_check 		(Value v);
	void			ValueObject_free  		(ValueObject this);
	Value			ValueObject_clone 		(ValueObject this);
	Value			ValueObject_copy  		(ValueObject src, ValueObject tgt);
	Value			ValueObject_fill  		(ValueObject src, ValueObject tgt);
	
	

	//###########################################################
	// Union
	//###########################################################
	typedef union		Value {
				ValueId				id;
				S_ValueBoolean			Boolean;
				S_ValueInteger			Integer;
				S_ValueResource			Resource;
				S_ValueByte			Byte;
				S_ValueStream			Stream;
				S_ValueChar			Char;
				S_ValueString			String;
				S_ValueNil			Nil;
				S_ValueCons			Cons;
				S_ValueArray			Array;
				S_ValueMap			Map;
				S_ValueName			Name;
				S_ValueClosure			Closure;
				S_ValueLambda			Lambda;
				S_ValueObject			Object;
	}							S_Value;

	//###########################################################
	//
	//###########################################################
	Value Ast_apply(Value f, Value a);
	Value Ast_apply_Name(ValueName f, Value a);
	Value Ast_apply_Closure(ValueClosure f, Value a);
	Value Ast_apply_Lambda(ValueLambda f, Value a);
	Value Ast_apply_Object(ValueObject f, Value a);

	Value Ast_access(Value l, Value r);
	Value Ast_access_Array_Integer(ValueArray l, ValueInteger r);
	Value Ast_access_String_Integer(ValueString l, ValueInteger r);
	Value Ast_access_Stream_Integer(ValueStream l, ValueInteger r);
	Value Ast_access_Map_Any(ValueMap l, Value r);

	Value Ast_concat(Value l, Value r);
	Value Ast_concat_Map_Map(ValueMap l, ValueMap r);
	Value Ast_concat_Map_Array(ValueMap l, ValueArray r);
	Value Ast_concat_Array_Map(ValueArray l, ValueMap r);
	Value Ast_concat_Array_Array(ValueArray l, ValueArray r);
	Value Ast_concat_String_String(ValueString l, ValueString r);
	Value Ast_concat_Stream_Stream(ValueStream l, ValueStream r);

	Value Ast_add(Value l, Value r);
	Value Ast_add_Integer_Integer(ValueInteger l, ValueInteger r);
	Value Ast_add_Char_Integer(ValueChar l, ValueInteger r);
	Value Ast_add_Byte_Integer(ValueByte l, ValueInteger r);

	Value Ast_dif(Value l, Value r);
	Value Ast_dif_Integer_Integer(ValueInteger l, ValueInteger r);

	Value Ast_car(Value v);
	Value Ast_car_Cons(ValueCons v);

	Value Ast_cdr(Value v);
	Value Ast_cdr_Cons(ValueCons v);


	void			Value_free			(Value this);


	Value			Value_clone			(Value this);


	Value			Value_copy			(Value src, Value tgt);


	Value			Value_fill			(Value src, Value tgt);


	Boo			Value_refl			(Value l, Value r);
	Boo			ValueBoolean_refl          	(ValueBoolean l, ValueBoolean r);
	Boo			ValueInteger_refl          	(ValueInteger l, ValueInteger r);
	Boo			ValueResource_refl          	(ValueResource l, ValueResource r);
	Boo			ValueByte_refl          	(ValueByte l, ValueByte r);
	Boo			ValueStream_refl          	(ValueStream l, ValueStream r);
	Boo			ValueChar_refl          	(ValueChar l, ValueChar r);
	Boo			ValueString_refl          	(ValueString l, ValueString r);
	Boo			ValueNil_refl          	(ValueNil l, ValueNil r);
	Boo			ValueCons_refl          	(ValueCons l, ValueCons r);
	Boo			ValueArray_refl          	(ValueArray l, ValueArray r);
	Boo			ValueMap_refl          	(ValueMap l, ValueMap r);
	Boo			ValueName_refl          	(ValueName l, ValueName r);
	Boo			ValueClosure_refl          	(ValueClosure l, ValueClosure r);
	Boo			ValueLambda_refl          	(ValueLambda l, ValueLambda r);
	Boo			ValueObject_refl          	(ValueObject l, ValueObject r);

	void			Value_write			(Value this, Writer w, Value_WriterMode mode);
	void			ValueBoolean_write          	(ValueBoolean this, Writer w, Value_WriterMode mode);
	void			ValueInteger_write          	(ValueInteger this, Writer w, Value_WriterMode mode);
	void			ValueResource_write          	(ValueResource this, Writer w, Value_WriterMode mode);
	void			ValueByte_write          	(ValueByte this, Writer w, Value_WriterMode mode);
	void			ValueStream_write          	(ValueStream this, Writer w, Value_WriterMode mode);
	void			ValueChar_write          	(ValueChar this, Writer w, Value_WriterMode mode);
	void			ValueString_write          	(ValueString this, Writer w, Value_WriterMode mode);
	void			ValueNil_write          	(ValueNil this, Writer w, Value_WriterMode mode);
	void			ValueCons_write          	(ValueCons this, Writer w, Value_WriterMode mode);
	void			ValueArray_write          	(ValueArray this, Writer w, Value_WriterMode mode);
	void			ValueMap_write          	(ValueMap this, Writer w, Value_WriterMode mode);
	void			ValueName_write          	(ValueName this, Writer w, Value_WriterMode mode);
	void			ValueClosure_write          	(ValueClosure this, Writer w, Value_WriterMode mode);
	void			ValueLambda_write          	(ValueLambda this, Writer w, Value_WriterMode mode);
	void			ValueObject_write          	(ValueObject this, Writer w, Value_WriterMode mode);

	Boo			Value_toBoo			(Value this);
	Boo			ValueBoolean_toBoo          	(ValueBoolean this);
	Boo			ValueInteger_toBoo          	(ValueInteger this);
	Boo			ValueResource_toBoo          	(ValueResource this);
	Boo			ValueByte_toBoo          	(ValueByte this);
	Boo			ValueStream_toBoo          	(ValueStream this);
	Boo			ValueChar_toBoo          	(ValueChar this);
	Boo			ValueString_toBoo          	(ValueString this);
	Boo			ValueNil_toBoo          	(ValueNil this);
	Boo			ValueCons_toBoo          	(ValueCons this);
	Boo			ValueArray_toBoo          	(ValueArray this);
	Boo			ValueMap_toBoo          	(ValueMap this);
	Boo			ValueName_toBoo          	(ValueName this);
	Boo			ValueClosure_toBoo          	(ValueClosure this);
	Boo			ValueLambda_toBoo          	(ValueLambda this);
	Boo			ValueObject_toBoo          	(ValueObject this);

	Key32			Value_toHash32			(Value this);
	Key32			ValueBoolean_toHash32          	(ValueBoolean this);
	Key32			ValueInteger_toHash32          	(ValueInteger this);
	Key32			ValueResource_toHash32          	(ValueResource this);
	Key32			ValueByte_toHash32          	(ValueByte this);
	Key32			ValueStream_toHash32          	(ValueStream this);
	Key32			ValueChar_toHash32          	(ValueChar this);
	Key32			ValueString_toHash32          	(ValueString this);
	Key32			ValueNil_toHash32          	(ValueNil this);
	Key32			ValueCons_toHash32          	(ValueCons this);
	Key32			ValueArray_toHash32          	(ValueArray this);
	Key32			ValueMap_toHash32          	(ValueMap this);
	Key32			ValueName_toHash32          	(ValueName this);
	Key32			ValueClosure_toHash32          	(ValueClosure this);
	Key32			ValueLambda_toHash32          	(ValueLambda this);
	Key32			ValueObject_toHash32          	(ValueObject this);

	Values			Value_children			(Value this);
	Values			ValueBoolean_children          	(ValueBoolean this);
	Values			ValueInteger_children          	(ValueInteger this);
	Values			ValueResource_children          	(ValueResource this);
	Values			ValueByte_children          	(ValueByte this);
	Values			ValueStream_children          	(ValueStream this);
	Values			ValueChar_children          	(ValueChar this);
	Values			ValueString_children          	(ValueString this);
	Values			ValueNil_children          	(ValueNil this);
	Values			ValueCons_children          	(ValueCons this);
	Values			ValueArray_children          	(ValueArray this);
	Values			ValueMap_children          	(ValueMap this);
	Values			ValueName_children          	(ValueName this);
	Values			ValueClosure_children          	(ValueClosure this);
	Values			ValueLambda_children          	(ValueLambda this);
	Values			ValueObject_children          	(ValueObject this);


#endif
