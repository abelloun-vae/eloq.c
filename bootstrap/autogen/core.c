#include "../main.h"
//###########################################################
// Body
//###########################################################

//######################
// Value
//######################
void Value_writeName(Value v, Writer w) {
	switch (v->id) {
		case ValueId_Boolean: Writer_writeStream0(w, (Stream0) "Boolean"); break;
		case ValueId_Integer: Writer_writeStream0(w, (Stream0) "Integer"); break;
		case ValueId_Resource: Writer_writeStream0(w, (Stream0) "Resource"); break;
		case ValueId_Byte: Writer_writeStream0(w, (Stream0) "Byte"); break;
		case ValueId_Stream: Writer_writeStream0(w, (Stream0) "Stream"); break;
		case ValueId_Char: Writer_writeStream0(w, (Stream0) "Char"); break;
		case ValueId_String: Writer_writeStream0(w, (Stream0) "String"); break;
		case ValueId_Nil: Writer_writeStream0(w, (Stream0) "Nil"); break;
		case ValueId_Cons: Writer_writeStream0(w, (Stream0) "Cons"); break;
		case ValueId_Array: Writer_writeStream0(w, (Stream0) "Array"); break;
		case ValueId_Map: Writer_writeStream0(w, (Stream0) "Map"); break;
		case ValueId_Name: Writer_writeStream0(w, (Stream0) "Name"); break;
		case ValueId_Closure: Writer_writeStream0(w, (Stream0) "Closure"); break;
		case ValueId_Lambda: Writer_writeStream0(w, (Stream0) "Lambda"); break;
		case ValueId_Object: Writer_writeStream0(w, (Stream0) "Object"); break;
		default: Writer_writeStream0(w, (Stream0) "<UNKNOWN Value Type : HUGE ERROR !>");
	}
}

void Value_printSizes() {
	printf("Value\t\t: %lu\n", sizeof(Value));
	printf("S_Value\t\t: %lu\n", sizeof(S_Value));
	printf("Boolean		: %lu\n", sizeof(S_ValueBoolean));
	printf("Integer		: %lu\n", sizeof(S_ValueInteger));
	printf("Resource		: %lu\n", sizeof(S_ValueResource));
	printf("Byte		: %lu\n", sizeof(S_ValueByte));
	printf("Stream		: %lu\n", sizeof(S_ValueStream));
	printf("Char		: %lu\n", sizeof(S_ValueChar));
	printf("String		: %lu\n", sizeof(S_ValueString));
	printf("Nil		: %lu\n", sizeof(S_ValueNil));
	printf("Cons		: %lu\n", sizeof(S_ValueCons));
	printf("Array		: %lu\n", sizeof(S_ValueArray));
	printf("Map		: %lu\n", sizeof(S_ValueMap));
	printf("Name		: %lu\n", sizeof(S_ValueName));
	printf("Closure		: %lu\n", sizeof(S_ValueClosure));
	printf("Lambda		: %lu\n", sizeof(S_ValueLambda));
	printf("Object		: %lu\n", sizeof(S_ValueObject));
	printf("##############################\n");
}

//###########################################################
// DISPATCHERS
//###########################################################
#define			DISPATCHER_refl_BODY(left, right)			if (left == right) return 1; if (left->id != right->id) return 0
#define			DISPATCHER_refl_EXPR(type, left, right)		(Value##type##_refl((Value##type) left, (Value##type) right) && \
													Values_refl(Value##type##_children((Value##type) left), Value##type##_children((Value##type) right)))
#define			DISPATCHER_write_BODY(this, w, mode)		if (mode == Value_WriterMode_info) {\
														Writer_writeChar(w, '(');\
														Value_writeName(this, w);\
														Writer_writeChar(w, ')');\
														Writer_writeChar(w, ' ');\
													}

Value Ast_apply(Value f, Value a) {
	switch((0 * ValueId_MAX_ + f->id)) {

		case (0 * ValueId_MAX_ + ValueId_Name): return Ast_apply_Name((ValueName) f, a);
		case (0 * ValueId_MAX_ + ValueId_Closure): return Ast_apply_Closure((ValueClosure) f, a);
		case (0 * ValueId_MAX_ + ValueId_Lambda): return Ast_apply_Lambda((ValueLambda) f, a);
		case (0 * ValueId_MAX_ + ValueId_Object): return Ast_apply_Object((ValueObject) f, a);
		default: 			dispatch_error("Ast_apply", "no matching case", 1, (Value[]) {f}); return 0;
	}
}


Value Ast_access(Value l, Value r) {
	switch(((0 * ValueId_MAX_ + l->id) * ValueId_MAX_ + r->id)) {

		case ((0 * ValueId_MAX_ + ValueId_Array) * ValueId_MAX_ + ValueId_Integer): return Ast_access_Array_Integer((ValueArray) l, (ValueInteger) r);
		case ((0 * ValueId_MAX_ + ValueId_String) * ValueId_MAX_ + ValueId_Integer): return Ast_access_String_Integer((ValueString) l, (ValueInteger) r);
		case ((0 * ValueId_MAX_ + ValueId_Stream) * ValueId_MAX_ + ValueId_Integer): return Ast_access_Stream_Integer((ValueStream) l, (ValueInteger) r);
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Boolean):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Integer):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Resource):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Byte):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Stream):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Char):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_String):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Nil):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Cons):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Array):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Map):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Name):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Closure):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Lambda):
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Object): return Ast_access_Map_Any((ValueMap) l, r);
		default: 			dispatch_error("Ast_access", "no matching case", 2, (Value[]) {l, r}); return 0;
	}
}


Value Ast_concat(Value l, Value r) {
	switch(((0 * ValueId_MAX_ + l->id) * ValueId_MAX_ + r->id)) {

		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Map): return Ast_concat_Map_Map((ValueMap) l, (ValueMap) r);
		case ((0 * ValueId_MAX_ + ValueId_Map) * ValueId_MAX_ + ValueId_Array): return Ast_concat_Map_Array((ValueMap) l, (ValueArray) r);
		case ((0 * ValueId_MAX_ + ValueId_Array) * ValueId_MAX_ + ValueId_Map): return Ast_concat_Array_Map((ValueArray) l, (ValueMap) r);
		case ((0 * ValueId_MAX_ + ValueId_Array) * ValueId_MAX_ + ValueId_Array): return Ast_concat_Array_Array((ValueArray) l, (ValueArray) r);
		case ((0 * ValueId_MAX_ + ValueId_String) * ValueId_MAX_ + ValueId_String): return Ast_concat_String_String((ValueString) l, (ValueString) r);
		case ((0 * ValueId_MAX_ + ValueId_Stream) * ValueId_MAX_ + ValueId_Stream): return Ast_concat_Stream_Stream((ValueStream) l, (ValueStream) r);
		default: 			dispatch_error("Ast_concat", "no matching case", 2, (Value[]) {l, r}); return 0;
	}
}


Value Ast_add(Value l, Value r) {
	switch(((0 * ValueId_MAX_ + l->id) * ValueId_MAX_ + r->id)) {

		case ((0 * ValueId_MAX_ + ValueId_Integer) * ValueId_MAX_ + ValueId_Integer): return Ast_add_Integer_Integer((ValueInteger) l, (ValueInteger) r);
		case ((0 * ValueId_MAX_ + ValueId_Char) * ValueId_MAX_ + ValueId_Integer): return Ast_add_Char_Integer((ValueChar) l, (ValueInteger) r);
		case ((0 * ValueId_MAX_ + ValueId_Byte) * ValueId_MAX_ + ValueId_Integer): return Ast_add_Byte_Integer((ValueByte) l, (ValueInteger) r);
		default: 			dispatch_error("Ast_add", "no matching case", 2, (Value[]) {l, r}); return 0;
	}
}


Value Ast_dif(Value l, Value r) {
	switch(((0 * ValueId_MAX_ + l->id) * ValueId_MAX_ + r->id)) {

		case ((0 * ValueId_MAX_ + ValueId_Integer) * ValueId_MAX_ + ValueId_Integer): return Ast_dif_Integer_Integer((ValueInteger) l, (ValueInteger) r);
		default: 			dispatch_error("Ast_dif", "no matching case", 2, (Value[]) {l, r}); return 0;
	}
}


Value Ast_car(Value v) {
	switch((0 * ValueId_MAX_ + v->id)) {

		case (0 * ValueId_MAX_ + ValueId_Cons): return Ast_car_Cons((ValueCons) v);
		default: 			dispatch_error("Ast_car", "no matching case", 1, (Value[]) {v}); return 0;
	}
}


Value Ast_cdr(Value v) {
	switch((0 * ValueId_MAX_ + v->id)) {

		case (0 * ValueId_MAX_ + ValueId_Cons): return Ast_cdr_Cons((ValueCons) v);
		default: 			dispatch_error("Ast_cdr", "no matching case", 1, (Value[]) {v}); return 0;
	}
}



//###########################################################
// free
//###########################################################
void Value_free(Value this) {
	
	switch(this->id) {
		case ValueId_Boolean: ValueBoolean_free((ValueBoolean) this); break;
		case ValueId_Integer: ValueInteger_free((ValueInteger) this); break;
		case ValueId_Resource: ValueResource_free((ValueResource) this); break;
		case ValueId_Byte: ValueByte_free((ValueByte) this); break;
		case ValueId_Stream: ValueStream_free((ValueStream) this); break;
		case ValueId_Char: ValueChar_free((ValueChar) this); break;
		case ValueId_String: ValueString_free((ValueString) this); break;
		case ValueId_Nil: ValueNil_free((ValueNil) this); break;
		case ValueId_Cons: ValueCons_free((ValueCons) this); break;
		case ValueId_Array: ValueArray_free((ValueArray) this); break;
		case ValueId_Map: ValueMap_free((ValueMap) this); break;
		case ValueId_Name: ValueName_free((ValueName) this); break;
		case ValueId_Closure: ValueClosure_free((ValueClosure) this); break;
		case ValueId_Lambda: ValueLambda_free((ValueLambda) this); break;
		case ValueId_Object: ValueObject_free((ValueObject) this); break;
		default:
			printf("\nfree not dispatched for object !!!");
			exit(1);
	}
}

//###########################################################
// clone
//###########################################################
Value Value_clone(Value this) {
	
	switch(this->id) {
		case ValueId_Boolean: return (Value) ValueBoolean_clone((ValueBoolean) this); 
		case ValueId_Integer: return (Value) ValueInteger_clone((ValueInteger) this); 
		case ValueId_Resource: return (Value) ValueResource_clone((ValueResource) this); 
		case ValueId_Byte: return (Value) ValueByte_clone((ValueByte) this); 
		case ValueId_Stream: return (Value) ValueStream_clone((ValueStream) this); 
		case ValueId_Char: return (Value) ValueChar_clone((ValueChar) this); 
		case ValueId_String: return (Value) ValueString_clone((ValueString) this); 
		case ValueId_Nil: return (Value) ValueNil_clone((ValueNil) this); 
		case ValueId_Cons: return (Value) ValueCons_clone((ValueCons) this); 
		case ValueId_Array: return (Value) ValueArray_clone((ValueArray) this); 
		case ValueId_Map: return (Value) ValueMap_clone((ValueMap) this); 
		case ValueId_Name: return (Value) ValueName_clone((ValueName) this); 
		case ValueId_Closure: return (Value) ValueClosure_clone((ValueClosure) this); 
		case ValueId_Lambda: return (Value) ValueLambda_clone((ValueLambda) this); 
		case ValueId_Object: return (Value) ValueObject_clone((ValueObject) this); 
		default:
			printf("\nclone not dispatched for object !!!");
			exit(1);
	}
}

//###########################################################
// copy
//###########################################################
Value Value_copy(Value src, Value tgt) {
	
	switch(src->id) {
		case ValueId_Boolean: return (Value) ValueBoolean_copy((ValueBoolean) src, (ValueBoolean) tgt); 
		case ValueId_Integer: return (Value) ValueInteger_copy((ValueInteger) src, (ValueInteger) tgt); 
		case ValueId_Resource: return (Value) ValueResource_copy((ValueResource) src, (ValueResource) tgt); 
		case ValueId_Byte: return (Value) ValueByte_copy((ValueByte) src, (ValueByte) tgt); 
		case ValueId_Stream: return (Value) ValueStream_copy((ValueStream) src, (ValueStream) tgt); 
		case ValueId_Char: return (Value) ValueChar_copy((ValueChar) src, (ValueChar) tgt); 
		case ValueId_String: return (Value) ValueString_copy((ValueString) src, (ValueString) tgt); 
		case ValueId_Nil: return (Value) ValueNil_copy((ValueNil) src, (ValueNil) tgt); 
		case ValueId_Cons: return (Value) ValueCons_copy((ValueCons) src, (ValueCons) tgt); 
		case ValueId_Array: return (Value) ValueArray_copy((ValueArray) src, (ValueArray) tgt); 
		case ValueId_Map: return (Value) ValueMap_copy((ValueMap) src, (ValueMap) tgt); 
		case ValueId_Name: return (Value) ValueName_copy((ValueName) src, (ValueName) tgt); 
		case ValueId_Closure: return (Value) ValueClosure_copy((ValueClosure) src, (ValueClosure) tgt); 
		case ValueId_Lambda: return (Value) ValueLambda_copy((ValueLambda) src, (ValueLambda) tgt); 
		case ValueId_Object: return (Value) ValueObject_copy((ValueObject) src, (ValueObject) tgt); 
		default:
			printf("\ncopy not dispatched for object !!!");
			exit(1);
	}
}

//###########################################################
// fill
//###########################################################
Value Value_fill(Value src, Value tgt) {
	
	switch(src->id) {
		case ValueId_Boolean: return (Value) ValueBoolean_fill((ValueBoolean) src, (ValueBoolean) tgt); 
		case ValueId_Integer: return (Value) ValueInteger_fill((ValueInteger) src, (ValueInteger) tgt); 
		case ValueId_Resource: return (Value) ValueResource_fill((ValueResource) src, (ValueResource) tgt); 
		case ValueId_Byte: return (Value) ValueByte_fill((ValueByte) src, (ValueByte) tgt); 
		case ValueId_Stream: return (Value) ValueStream_fill((ValueStream) src, (ValueStream) tgt); 
		case ValueId_Char: return (Value) ValueChar_fill((ValueChar) src, (ValueChar) tgt); 
		case ValueId_String: return (Value) ValueString_fill((ValueString) src, (ValueString) tgt); 
		case ValueId_Nil: return (Value) ValueNil_fill((ValueNil) src, (ValueNil) tgt); 
		case ValueId_Cons: return (Value) ValueCons_fill((ValueCons) src, (ValueCons) tgt); 
		case ValueId_Array: return (Value) ValueArray_fill((ValueArray) src, (ValueArray) tgt); 
		case ValueId_Map: return (Value) ValueMap_fill((ValueMap) src, (ValueMap) tgt); 
		case ValueId_Name: return (Value) ValueName_fill((ValueName) src, (ValueName) tgt); 
		case ValueId_Closure: return (Value) ValueClosure_fill((ValueClosure) src, (ValueClosure) tgt); 
		case ValueId_Lambda: return (Value) ValueLambda_fill((ValueLambda) src, (ValueLambda) tgt); 
		case ValueId_Object: return (Value) ValueObject_fill((ValueObject) src, (ValueObject) tgt); 
		default:
			printf("\nfill not dispatched for object !!!");
			exit(1);
	}
}

//###########################################################
// refl
//###########################################################
Boo Value_refl(Value l, Value r) {
	DISPATCHER_refl_BODY(l, r);
	switch(l->id) {
		case ValueId_Boolean: return (Boo) DISPATCHER_refl_EXPR(Boolean, l, r);
		case ValueId_Integer: return (Boo) DISPATCHER_refl_EXPR(Integer, l, r);
		case ValueId_Resource: return (Boo) DISPATCHER_refl_EXPR(Resource, l, r);
		case ValueId_Byte: return (Boo) DISPATCHER_refl_EXPR(Byte, l, r);
		case ValueId_Stream: return (Boo) DISPATCHER_refl_EXPR(Stream, l, r);
		case ValueId_Char: return (Boo) DISPATCHER_refl_EXPR(Char, l, r);
		case ValueId_String: return (Boo) DISPATCHER_refl_EXPR(String, l, r);
		case ValueId_Nil: return (Boo) DISPATCHER_refl_EXPR(Nil, l, r);
		case ValueId_Cons: return (Boo) DISPATCHER_refl_EXPR(Cons, l, r);
		case ValueId_Array: return (Boo) DISPATCHER_refl_EXPR(Array, l, r);
		case ValueId_Map: return (Boo) DISPATCHER_refl_EXPR(Map, l, r);
		case ValueId_Name: return (Boo) DISPATCHER_refl_EXPR(Name, l, r);
		case ValueId_Closure: return (Boo) DISPATCHER_refl_EXPR(Closure, l, r);
		case ValueId_Lambda: return (Boo) DISPATCHER_refl_EXPR(Lambda, l, r);
		case ValueId_Object: return (Boo) DISPATCHER_refl_EXPR(Object, l, r);
		default:
			printf("\nrefl not dispatched for object !!!");
			exit(1);
	}
}

//###########################################################
// write
//###########################################################
void Value_write(Value this, Writer w, Value_WriterMode mode) {
	DISPATCHER_write_BODY(this, w, mode);
	switch(this->id) {
		case ValueId_Boolean: ValueBoolean_write((ValueBoolean) this, w, mode); break;
		case ValueId_Integer: ValueInteger_write((ValueInteger) this, w, mode); break;
		case ValueId_Resource: ValueResource_write((ValueResource) this, w, mode); break;
		case ValueId_Byte: ValueByte_write((ValueByte) this, w, mode); break;
		case ValueId_Stream: ValueStream_write((ValueStream) this, w, mode); break;
		case ValueId_Char: ValueChar_write((ValueChar) this, w, mode); break;
		case ValueId_String: ValueString_write((ValueString) this, w, mode); break;
		case ValueId_Nil: ValueNil_write((ValueNil) this, w, mode); break;
		case ValueId_Cons: ValueCons_write((ValueCons) this, w, mode); break;
		case ValueId_Array: ValueArray_write((ValueArray) this, w, mode); break;
		case ValueId_Map: ValueMap_write((ValueMap) this, w, mode); break;
		case ValueId_Name: ValueName_write((ValueName) this, w, mode); break;
		case ValueId_Closure: ValueClosure_write((ValueClosure) this, w, mode); break;
		case ValueId_Lambda: ValueLambda_write((ValueLambda) this, w, mode); break;
		case ValueId_Object: ValueObject_write((ValueObject) this, w, mode); break;
		default:
			printf("\nwrite not dispatched for object !!!");
			exit(1);
	}
}

//###########################################################
// toBoo
//###########################################################
Boo Value_toBoo(Value this) {
	
	switch(this->id) {
		case ValueId_Boolean: return (Boo) ValueBoolean_toBoo((ValueBoolean) this); 
		case ValueId_Integer: return (Boo) ValueInteger_toBoo((ValueInteger) this); 
		case ValueId_Resource: return (Boo) ValueResource_toBoo((ValueResource) this); 
		case ValueId_Byte: return (Boo) ValueByte_toBoo((ValueByte) this); 
		case ValueId_Stream: return (Boo) ValueStream_toBoo((ValueStream) this); 
		case ValueId_Char: return (Boo) ValueChar_toBoo((ValueChar) this); 
		case ValueId_String: return (Boo) ValueString_toBoo((ValueString) this); 
		case ValueId_Nil: return (Boo) ValueNil_toBoo((ValueNil) this); 
		case ValueId_Cons: return (Boo) ValueCons_toBoo((ValueCons) this); 
		case ValueId_Array: return (Boo) ValueArray_toBoo((ValueArray) this); 
		case ValueId_Map: return (Boo) ValueMap_toBoo((ValueMap) this); 
		case ValueId_Name: return (Boo) ValueName_toBoo((ValueName) this); 
		case ValueId_Closure: return (Boo) ValueClosure_toBoo((ValueClosure) this); 
		case ValueId_Lambda: return (Boo) ValueLambda_toBoo((ValueLambda) this); 
		case ValueId_Object: return (Boo) ValueObject_toBoo((ValueObject) this); 
		default:
			printf("\ntoBoo not dispatched for object !!!");
			exit(1);
	}
}

//###########################################################
// toHash32
//###########################################################
Key32 Value_toHash32(Value this) {
	
	switch(this->id) {
		case ValueId_Boolean: return (Key32) ValueBoolean_toHash32((ValueBoolean) this); 
		case ValueId_Integer: return (Key32) ValueInteger_toHash32((ValueInteger) this); 
		case ValueId_Resource: return (Key32) ValueResource_toHash32((ValueResource) this); 
		case ValueId_Byte: return (Key32) ValueByte_toHash32((ValueByte) this); 
		case ValueId_Stream: return (Key32) ValueStream_toHash32((ValueStream) this); 
		case ValueId_Char: return (Key32) ValueChar_toHash32((ValueChar) this); 
		case ValueId_String: return (Key32) ValueString_toHash32((ValueString) this); 
		case ValueId_Nil: return (Key32) ValueNil_toHash32((ValueNil) this); 
		case ValueId_Cons: return (Key32) ValueCons_toHash32((ValueCons) this); 
		case ValueId_Array: return (Key32) ValueArray_toHash32((ValueArray) this); 
		case ValueId_Map: return (Key32) ValueMap_toHash32((ValueMap) this); 
		case ValueId_Name: return (Key32) ValueName_toHash32((ValueName) this); 
		case ValueId_Closure: return (Key32) ValueClosure_toHash32((ValueClosure) this); 
		case ValueId_Lambda: return (Key32) ValueLambda_toHash32((ValueLambda) this); 
		case ValueId_Object: return (Key32) ValueObject_toHash32((ValueObject) this); 
		default:
			printf("\ntoHash32 not dispatched for object !!!");
			exit(1);
	}
}

//###########################################################
// children
//###########################################################
Values Value_children(Value this) {
	
	switch(this->id) {
		case ValueId_Boolean: return (Values) ValueBoolean_children((ValueBoolean) this); 
		case ValueId_Integer: return (Values) ValueInteger_children((ValueInteger) this); 
		case ValueId_Resource: return (Values) ValueResource_children((ValueResource) this); 
		case ValueId_Byte: return (Values) ValueByte_children((ValueByte) this); 
		case ValueId_Stream: return (Values) ValueStream_children((ValueStream) this); 
		case ValueId_Char: return (Values) ValueChar_children((ValueChar) this); 
		case ValueId_String: return (Values) ValueString_children((ValueString) this); 
		case ValueId_Nil: return (Values) ValueNil_children((ValueNil) this); 
		case ValueId_Cons: return (Values) ValueCons_children((ValueCons) this); 
		case ValueId_Array: return (Values) ValueArray_children((ValueArray) this); 
		case ValueId_Map: return (Values) ValueMap_children((ValueMap) this); 
		case ValueId_Name: return (Values) ValueName_children((ValueName) this); 
		case ValueId_Closure: return (Values) ValueClosure_children((ValueClosure) this); 
		case ValueId_Lambda: return (Values) ValueLambda_children((ValueLambda) this); 
		case ValueId_Object: return (Values) ValueObject_children((ValueObject) this); 
		default:
			printf("\nchildren not dispatched for object !!!");
			exit(1);
	}
}


//######################
// Abstract Class
//######################
//######################
// CLASS Boolean
//######################
Value ValueBoolean_make(Boo b) {
	return ValueBoolean_init(ValueBoolean_alloc(), b);
}

size_t ValueBoolean_sizeof() {
	return sizeof(S_ValueBoolean) + 0;
}

ValueBoolean ValueBoolean_alloc() {
	Byte* _ptr = Allocator_alloc(ValueBoolean_sizeof());
	ValueBoolean this = (ValueBoolean) _ptr;
	this->id = ValueId_Boolean;
	ValueBoolean_coalloc(this, _ptr + sizeof(S_ValueBoolean));
	return this;
}

Value ValueBoolean_init(ValueBoolean this, Boo b) {
	this->id = ValueId_Boolean;
	this->b = b;
	return (Value) this;
}

ValueBoolean ValueBoolean_realloc(ValueBoolean this) {
	Byte* _ptr = Allocator_alloc(0);
	ValueBoolean_coalloc(this, _ptr);
	return this;
}

ValueBoolean ValueBoolean_coalloc(ValueBoolean this, Byte* _ptr) {
	
	return this;
}

//############################################
//
//############################################
void ValueBoolean_check(Value v) {
	if (v->id != ValueId_Boolean) {
		printf("Type error, expected : \"Boolean\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueBoolean_free(ValueBoolean this) {
	Ptr first = this + 1;
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueBoolean_clone(ValueBoolean v) {
	return ValueBoolean_copy(v, ValueBoolean_alloc());
}

Value ValueBoolean_copy(ValueBoolean src, ValueBoolean tgt) {
	tgt->id = ValueId_Boolean;
	tgt->b = src->b;
	return (Value) tgt;
}

Value ValueBoolean_fill(ValueBoolean src, ValueBoolean tgt) {
	ValueBoolean_realloc(tgt);
	return ValueBoolean_copy(src, tgt);
}

//######################
// CLASS Integer
//######################
Value ValueInteger_make(Int i) {
	return ValueInteger_init(ValueInteger_alloc(), i);
}

size_t ValueInteger_sizeof() {
	return sizeof(S_ValueInteger) + 0;
}

ValueInteger ValueInteger_alloc() {
	Byte* _ptr = Allocator_alloc(ValueInteger_sizeof());
	ValueInteger this = (ValueInteger) _ptr;
	this->id = ValueId_Integer;
	ValueInteger_coalloc(this, _ptr + sizeof(S_ValueInteger));
	return this;
}

Value ValueInteger_init(ValueInteger this, Int i) {
	this->id = ValueId_Integer;
	this->i = i;
	return (Value) this;
}

ValueInteger ValueInteger_realloc(ValueInteger this) {
	Byte* _ptr = Allocator_alloc(0);
	ValueInteger_coalloc(this, _ptr);
	return this;
}

ValueInteger ValueInteger_coalloc(ValueInteger this, Byte* _ptr) {
	
	return this;
}

//############################################
//
//############################################
void ValueInteger_check(Value v) {
	if (v->id != ValueId_Integer) {
		printf("Type error, expected : \"Integer\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueInteger_free(ValueInteger this) {
	Ptr first = this + 1;
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueInteger_clone(ValueInteger v) {
	return ValueInteger_copy(v, ValueInteger_alloc());
}

Value ValueInteger_copy(ValueInteger src, ValueInteger tgt) {
	tgt->id = ValueId_Integer;
	tgt->i = src->i;
	return (Value) tgt;
}

Value ValueInteger_fill(ValueInteger src, ValueInteger tgt) {
	ValueInteger_realloc(tgt);
	return ValueInteger_copy(src, tgt);
}

//######################
// CLASS Resource
//######################
Value ValueResource_make(Ptr r) {
	return ValueResource_init(ValueResource_alloc(), r);
}

size_t ValueResource_sizeof() {
	return sizeof(S_ValueResource) + 0;
}

ValueResource ValueResource_alloc() {
	Byte* _ptr = Allocator_alloc(ValueResource_sizeof());
	ValueResource this = (ValueResource) _ptr;
	this->id = ValueId_Resource;
	ValueResource_coalloc(this, _ptr + sizeof(S_ValueResource));
	return this;
}

Value ValueResource_init(ValueResource this, Ptr r) {
	this->id = ValueId_Resource;
	this->r = r;
	return (Value) this;
}

ValueResource ValueResource_realloc(ValueResource this) {
	Byte* _ptr = Allocator_alloc(0);
	ValueResource_coalloc(this, _ptr);
	return this;
}

ValueResource ValueResource_coalloc(ValueResource this, Byte* _ptr) {
	
	return this;
}

//############################################
//
//############################################
void ValueResource_check(Value v) {
	if (v->id != ValueId_Resource) {
		printf("Type error, expected : \"Resource\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueResource_free(ValueResource this) {
	Ptr first = this + 1;
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueResource_clone(ValueResource v) {
	return ValueResource_copy(v, ValueResource_alloc());
}

Value ValueResource_copy(ValueResource src, ValueResource tgt) {
	tgt->id = ValueId_Resource;
	tgt->r = src->r;
	return (Value) tgt;
}

Value ValueResource_fill(ValueResource src, ValueResource tgt) {
	ValueResource_realloc(tgt);
	return ValueResource_copy(src, tgt);
}

//######################
// CLASS Byte
//######################
Value ValueByte_make(Byte b) {
	return ValueByte_init(ValueByte_alloc(), b);
}

size_t ValueByte_sizeof() {
	return sizeof(S_ValueByte) + 0;
}

ValueByte ValueByte_alloc() {
	Byte* _ptr = Allocator_alloc(ValueByte_sizeof());
	ValueByte this = (ValueByte) _ptr;
	this->id = ValueId_Byte;
	ValueByte_coalloc(this, _ptr + sizeof(S_ValueByte));
	return this;
}

Value ValueByte_init(ValueByte this, Byte b) {
	this->id = ValueId_Byte;
	this->b = b;
	return (Value) this;
}

ValueByte ValueByte_realloc(ValueByte this) {
	Byte* _ptr = Allocator_alloc(0);
	ValueByte_coalloc(this, _ptr);
	return this;
}

ValueByte ValueByte_coalloc(ValueByte this, Byte* _ptr) {
	
	return this;
}

//############################################
//
//############################################
void ValueByte_check(Value v) {
	if (v->id != ValueId_Byte) {
		printf("Type error, expected : \"Byte\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueByte_free(ValueByte this) {
	Ptr first = this + 1;
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueByte_clone(ValueByte v) {
	return ValueByte_copy(v, ValueByte_alloc());
}

Value ValueByte_copy(ValueByte src, ValueByte tgt) {
	tgt->id = ValueId_Byte;
	tgt->b = src->b;
	return (Value) tgt;
}

Value ValueByte_fill(ValueByte src, ValueByte tgt) {
	ValueByte_realloc(tgt);
	return ValueByte_copy(src, tgt);
}

//######################
// CLASS Stream
//######################
Value ValueStream_make(Nat len, Stream stm) {
	return ValueStream_init(ValueStream_alloc(), len, stm);
}

size_t ValueStream_sizeof() {
	return sizeof(S_ValueStream) + 0;
}

ValueStream ValueStream_alloc() {
	Byte* _ptr = Allocator_alloc(ValueStream_sizeof());
	ValueStream this = (ValueStream) _ptr;
	this->id = ValueId_Stream;
	ValueStream_coalloc(this, _ptr + sizeof(S_ValueStream));
	return this;
}

Value ValueStream_init(ValueStream this, Nat len, Stream stm) {
	this->id = ValueId_Stream;
	this->len = len;
	this->stm = stm;
	return (Value) this;
}

ValueStream ValueStream_realloc(ValueStream this) {
	Byte* _ptr = Allocator_alloc(0);
	ValueStream_coalloc(this, _ptr);
	return this;
}

ValueStream ValueStream_coalloc(ValueStream this, Byte* _ptr) {
	
	return this;
}

//############################################
//
//############################################
void ValueStream_check(Value v) {
	if (v->id != ValueId_Stream) {
		printf("Type error, expected : \"Stream\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueStream_free(ValueStream this) {
	Ptr first = this + 1;
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueStream_clone(ValueStream v) {
	return ValueStream_copy(v, ValueStream_alloc());
}

Value ValueStream_copy(ValueStream src, ValueStream tgt) {
	tgt->id = ValueId_Stream;
	tgt->len = src->len;
	tgt->stm = src->stm;
	return (Value) tgt;
}

Value ValueStream_fill(ValueStream src, ValueStream tgt) {
	ValueStream_realloc(tgt);
	return ValueStream_copy(src, tgt);
}

//######################
// CLASS Char
//######################
Value ValueChar_make(Char c) {
	return ValueChar_init(ValueChar_alloc(), c);
}

size_t ValueChar_sizeof() {
	return sizeof(S_ValueChar) + 0;
}

ValueChar ValueChar_alloc() {
	Byte* _ptr = Allocator_alloc(ValueChar_sizeof());
	ValueChar this = (ValueChar) _ptr;
	this->id = ValueId_Char;
	ValueChar_coalloc(this, _ptr + sizeof(S_ValueChar));
	return this;
}

Value ValueChar_init(ValueChar this, Char c) {
	this->id = ValueId_Char;
	this->c = c;
	return (Value) this;
}

ValueChar ValueChar_realloc(ValueChar this) {
	Byte* _ptr = Allocator_alloc(0);
	ValueChar_coalloc(this, _ptr);
	return this;
}

ValueChar ValueChar_coalloc(ValueChar this, Byte* _ptr) {
	
	return this;
}

//############################################
//
//############################################
void ValueChar_check(Value v) {
	if (v->id != ValueId_Char) {
		printf("Type error, expected : \"Char\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueChar_free(ValueChar this) {
	Ptr first = this + 1;
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueChar_clone(ValueChar v) {
	return ValueChar_copy(v, ValueChar_alloc());
}

Value ValueChar_copy(ValueChar src, ValueChar tgt) {
	tgt->id = ValueId_Char;
	tgt->c = src->c;
	return (Value) tgt;
}

Value ValueChar_fill(ValueChar src, ValueChar tgt) {
	ValueChar_realloc(tgt);
	return ValueChar_copy(src, tgt);
}

//######################
// CLASS String
//######################
Value ValueString_make(Nat len, String str) {
	return ValueString_init(ValueString_alloc(), len, str);
}

size_t ValueString_sizeof() {
	return sizeof(S_ValueString) + 0;
}

ValueString ValueString_alloc() {
	Byte* _ptr = Allocator_alloc(ValueString_sizeof());
	ValueString this = (ValueString) _ptr;
	this->id = ValueId_String;
	ValueString_coalloc(this, _ptr + sizeof(S_ValueString));
	return this;
}

Value ValueString_init(ValueString this, Nat len, String str) {
	this->id = ValueId_String;
	this->len = len;
	this->str = str;
	return (Value) this;
}

ValueString ValueString_realloc(ValueString this) {
	Byte* _ptr = Allocator_alloc(0);
	ValueString_coalloc(this, _ptr);
	return this;
}

ValueString ValueString_coalloc(ValueString this, Byte* _ptr) {
	
	return this;
}

//############################################
//
//############################################
void ValueString_check(Value v) {
	if (v->id != ValueId_String) {
		printf("Type error, expected : \"String\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueString_free(ValueString this) {
	Ptr first = this + 1;
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueString_clone(ValueString v) {
	return ValueString_copy(v, ValueString_alloc());
}

Value ValueString_copy(ValueString src, ValueString tgt) {
	tgt->id = ValueId_String;
	tgt->len = src->len;
	tgt->str = src->str;
	return (Value) tgt;
}

Value ValueString_fill(ValueString src, ValueString tgt) {
	ValueString_realloc(tgt);
	return ValueString_copy(src, tgt);
}

//######################
// CLASS Nil
//######################
Value ValueNil_make() {
	return ValueNil_init(ValueNil_alloc());
}

size_t ValueNil_sizeof() {
	return sizeof(S_ValueNil) + 0;
}

ValueNil ValueNil_alloc() {
	Byte* _ptr = Allocator_alloc(ValueNil_sizeof());
	ValueNil this = (ValueNil) _ptr;
	this->id = ValueId_Nil;
	ValueNil_coalloc(this, _ptr + sizeof(S_ValueNil));
	return this;
}

Value ValueNil_init(ValueNil this) {
	this->id = ValueId_Nil;
	
	return (Value) this;
}

ValueNil ValueNil_realloc(ValueNil this) {
	Byte* _ptr = Allocator_alloc(0);
	ValueNil_coalloc(this, _ptr);
	return this;
}

ValueNil ValueNil_coalloc(ValueNil this, Byte* _ptr) {
	
	return this;
}

//############################################
//
//############################################
void ValueNil_check(Value v) {
	if (v->id != ValueId_Nil) {
		printf("Type error, expected : \"Nil\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueNil_free(ValueNil this) {
	Ptr first = this + 1;
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueNil_clone(ValueNil v) {
	return ValueNil_copy(v, ValueNil_alloc());
}

Value ValueNil_copy(ValueNil src, ValueNil tgt) {
	tgt->id = ValueId_Nil;
	
	return (Value) tgt;
}

Value ValueNil_fill(ValueNil src, ValueNil tgt) {
	ValueNil_realloc(tgt);
	return ValueNil_copy(src, tgt);
}

//######################
// CLASS Cons
//######################
Value ValueCons_make(Value head, Value rest) {
	return ValueCons_init(ValueCons_alloc(), head, rest);
}

size_t ValueCons_sizeof() {
	return sizeof(S_ValueCons) + 0;
}

ValueCons ValueCons_alloc() {
	Byte* _ptr = Allocator_alloc(ValueCons_sizeof());
	ValueCons this = (ValueCons) _ptr;
	this->id = ValueId_Cons;
	ValueCons_coalloc(this, _ptr + sizeof(S_ValueCons));
	return this;
}

Value ValueCons_init(ValueCons this, Value head, Value rest) {
	this->id = ValueId_Cons;
	this->head = head;
	this->rest = rest;
	return (Value) this;
}

ValueCons ValueCons_realloc(ValueCons this) {
	Byte* _ptr = Allocator_alloc(0);
	ValueCons_coalloc(this, _ptr);
	return this;
}

ValueCons ValueCons_coalloc(ValueCons this, Byte* _ptr) {
	
	return this;
}

//############################################
//
//############################################
void ValueCons_check(Value v) {
	if (v->id != ValueId_Cons) {
		printf("Type error, expected : \"Cons\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueCons_free(ValueCons this) {
	Ptr first = this + 1;
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueCons_clone(ValueCons v) {
	return ValueCons_copy(v, ValueCons_alloc());
}

Value ValueCons_copy(ValueCons src, ValueCons tgt) {
	tgt->id = ValueId_Cons;
	tgt->head = src->head;
	tgt->rest = src->rest;
	return (Value) tgt;
}

Value ValueCons_fill(ValueCons src, ValueCons tgt) {
	ValueCons_realloc(tgt);
	return ValueCons_copy(src, tgt);
}

//######################
// CLASS Array
//######################
Value ValueArray_make(Boo tuple, Values data) {
	return ValueArray_init(ValueArray_alloc(data.size), tuple, data);
}

size_t ValueArray_sizeof(Values_FIELDS_size data_size) {
	return sizeof(S_ValueArray) + sizeof(Values_ALLOCTYPE[data_size]) + 0;
}

ValueArray ValueArray_alloc(Values_FIELDS_size data_size) {
	Byte* _ptr = Allocator_alloc(ValueArray_sizeof(data_size));
	ValueArray this = (ValueArray) _ptr;
	this->id = ValueId_Array;
	ValueArray_coalloc(this, _ptr + sizeof(S_ValueArray), data_size);
	return this;
}

Value ValueArray_init(ValueArray this, Boo tuple, Values data) {
	this->id = ValueId_Array;
	this->tuple = tuple;
	Values_COPY(data, this->data);
	return (Value) this;
}

ValueArray ValueArray_realloc(ValueArray this, Values_FIELDS_size data_size) {
	Byte* _ptr = Allocator_alloc(sizeof(Values_ALLOCTYPE[data_size]) + 0);
	ValueArray_coalloc(this, _ptr, data_size);
	return this;
}

ValueArray ValueArray_coalloc(ValueArray this, Byte* _ptr, Values_FIELDS_size data_size) {
	Values_COALLOC(this->data, _ptr, data_size);
	
	return this;
}

//############################################
//
//############################################
void ValueArray_check(Value v) {
	if (v->id != ValueId_Array) {
		printf("Type error, expected : \"Array\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueArray_free(ValueArray this) {
	Ptr first = Values_COPTR(this->data);
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueArray_clone(ValueArray v) {
	return ValueArray_copy(v, ValueArray_alloc(v->data.size));
}

Value ValueArray_copy(ValueArray src, ValueArray tgt) {
	tgt->id = ValueId_Array;
	tgt->tuple = src->tuple;
	Values_COPY(src->data, tgt->data);
	return (Value) tgt;
}

Value ValueArray_fill(ValueArray src, ValueArray tgt) {
	ValueArray_realloc(tgt, src->data.size);
	return ValueArray_copy(src, tgt);
}

//######################
// CLASS Map
//######################
Value ValueMap_make(Boo record, Map map) {
	return ValueMap_init(ValueMap_alloc(map.size), record, map);
}

size_t ValueMap_sizeof(Map_FIELDS_size map_size) {
	return sizeof(S_ValueMap) + sizeof(Map_ALLOCTYPE[map_size]) + 0;
}

ValueMap ValueMap_alloc(Map_FIELDS_size map_size) {
	Byte* _ptr = Allocator_alloc(ValueMap_sizeof(map_size));
	ValueMap this = (ValueMap) _ptr;
	this->id = ValueId_Map;
	ValueMap_coalloc(this, _ptr + sizeof(S_ValueMap), map_size);
	return this;
}

Value ValueMap_init_noInit(ValueMap this, Boo record, Map map) {
	this->id = ValueId_Map;
	this->record = record;
	Map_COPY(map, this->map);
	return (Value) this;
}

ValueMap ValueMap_realloc(ValueMap this, Map_FIELDS_size map_size) {
	Byte* _ptr = Allocator_alloc(sizeof(Map_ALLOCTYPE[map_size]) + 0);
	ValueMap_coalloc(this, _ptr, map_size);
	return this;
}

ValueMap ValueMap_coalloc(ValueMap this, Byte* _ptr, Map_FIELDS_size map_size) {
	Map_COALLOC(this->map, _ptr, map_size);
	
	return this;
}

//############################################
//
//############################################
void ValueMap_check(Value v) {
	if (v->id != ValueId_Map) {
		printf("Type error, expected : \"Map\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueMap_free(ValueMap this) {
	Ptr first = Map_COPTR(this->map);
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueMap_clone(ValueMap v) {
	return ValueMap_copy(v, ValueMap_alloc(v->map.size));
}

Value ValueMap_copy(ValueMap src, ValueMap tgt) {
	tgt->id = ValueId_Map;
	tgt->record = src->record;
	Map_COPY(src->map, tgt->map);
	tgt->count = src->count;
	return (Value) tgt;
}

Value ValueMap_fill(ValueMap src, ValueMap tgt) {
	ValueMap_realloc(tgt, src->map.size);
	return ValueMap_copy(src, tgt);
}

//######################
// CLASS Name
//######################
Value ValueName_make(NameId name, Nat arity, Values data) {
	return ValueName_init(ValueName_alloc(data.size), name, arity, data);
}

size_t ValueName_sizeof(Values_FIELDS_size data_size) {
	return sizeof(S_ValueName) + sizeof(Values_ALLOCTYPE[data_size]) + 0;
}

ValueName ValueName_alloc(Values_FIELDS_size data_size) {
	Byte* _ptr = Allocator_alloc(ValueName_sizeof(data_size));
	ValueName this = (ValueName) _ptr;
	this->id = ValueId_Name;
	ValueName_coalloc(this, _ptr + sizeof(S_ValueName), data_size);
	return this;
}

Value ValueName_init(ValueName this, NameId name, Nat arity, Values data) {
	this->id = ValueId_Name;
	this->name = name;
	this->arity = arity;
	Values_COPY(data, this->data);
	return (Value) this;
}

ValueName ValueName_realloc(ValueName this, Values_FIELDS_size data_size) {
	Byte* _ptr = Allocator_alloc(sizeof(Values_ALLOCTYPE[data_size]) + 0);
	ValueName_coalloc(this, _ptr, data_size);
	return this;
}

ValueName ValueName_coalloc(ValueName this, Byte* _ptr, Values_FIELDS_size data_size) {
	Values_COALLOC(this->data, _ptr, data_size);
	
	return this;
}

//############################################
//
//############################################
void ValueName_check(Value v) {
	if (v->id != ValueId_Name) {
		printf("Type error, expected : \"Name\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueName_free(ValueName this) {
	Ptr first = Values_COPTR(this->data);
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueName_clone(ValueName v) {
	return ValueName_copy(v, ValueName_alloc(v->data.size));
}

Value ValueName_copy(ValueName src, ValueName tgt) {
	tgt->id = ValueId_Name;
	tgt->name = src->name;
	tgt->arity = src->arity;
	Values_COPY(src->data, tgt->data);
	return (Value) tgt;
}

Value ValueName_fill(ValueName src, ValueName tgt) {
	ValueName_realloc(tgt, src->data.size);
	return ValueName_copy(src, tgt);
}

//######################
// CLASS Closure
//######################
Value ValueClosure_make(Closure closure, Values scope) {
	return ValueClosure_init(ValueClosure_alloc(scope.size), closure, scope);
}

size_t ValueClosure_sizeof(Values_FIELDS_size scope_size) {
	return sizeof(S_ValueClosure) + sizeof(Values_ALLOCTYPE[scope_size]) + 0;
}

ValueClosure ValueClosure_alloc(Values_FIELDS_size scope_size) {
	Byte* _ptr = Allocator_alloc(ValueClosure_sizeof(scope_size));
	ValueClosure this = (ValueClosure) _ptr;
	this->id = ValueId_Closure;
	ValueClosure_coalloc(this, _ptr + sizeof(S_ValueClosure), scope_size);
	return this;
}

Value ValueClosure_init(ValueClosure this, Closure closure, Values scope) {
	this->id = ValueId_Closure;
	this->closure = closure;
	Values_COPY(scope, this->scope);
	return (Value) this;
}

ValueClosure ValueClosure_realloc(ValueClosure this, Values_FIELDS_size scope_size) {
	Byte* _ptr = Allocator_alloc(sizeof(Values_ALLOCTYPE[scope_size]) + 0);
	ValueClosure_coalloc(this, _ptr, scope_size);
	return this;
}

ValueClosure ValueClosure_coalloc(ValueClosure this, Byte* _ptr, Values_FIELDS_size scope_size) {
	Values_COALLOC(this->scope, _ptr, scope_size);
	
	return this;
}

//############################################
//
//############################################
void ValueClosure_check(Value v) {
	if (v->id != ValueId_Closure) {
		printf("Type error, expected : \"Closure\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueClosure_free(ValueClosure this) {
	Ptr first = Values_COPTR(this->scope);
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueClosure_clone(ValueClosure v) {
	return ValueClosure_copy(v, ValueClosure_alloc(v->scope.size));
}

Value ValueClosure_copy(ValueClosure src, ValueClosure tgt) {
	tgt->id = ValueId_Closure;
	tgt->closure = src->closure;
	Values_COPY(src->scope, tgt->scope);
	return (Value) tgt;
}

Value ValueClosure_fill(ValueClosure src, ValueClosure tgt) {
	ValueClosure_realloc(tgt, src->scope.size);
	return ValueClosure_copy(src, tgt);
}

//######################
// CLASS Lambda
//######################
Value ValueLambda_make(Lambda lambda, Nat arity, Values args) {
	return ValueLambda_init(ValueLambda_alloc(args.size), lambda, arity, args);
}

size_t ValueLambda_sizeof(Values_FIELDS_size args_size) {
	return sizeof(S_ValueLambda) + sizeof(Values_ALLOCTYPE[args_size]) + 0;
}

ValueLambda ValueLambda_alloc(Values_FIELDS_size args_size) {
	Byte* _ptr = Allocator_alloc(ValueLambda_sizeof(args_size));
	ValueLambda this = (ValueLambda) _ptr;
	this->id = ValueId_Lambda;
	ValueLambda_coalloc(this, _ptr + sizeof(S_ValueLambda), args_size);
	return this;
}

Value ValueLambda_init(ValueLambda this, Lambda lambda, Nat arity, Values args) {
	this->id = ValueId_Lambda;
	this->lambda = lambda;
	this->arity = arity;
	Values_COPY(args, this->args);
	return (Value) this;
}

ValueLambda ValueLambda_realloc(ValueLambda this, Values_FIELDS_size args_size) {
	Byte* _ptr = Allocator_alloc(sizeof(Values_ALLOCTYPE[args_size]) + 0);
	ValueLambda_coalloc(this, _ptr, args_size);
	return this;
}

ValueLambda ValueLambda_coalloc(ValueLambda this, Byte* _ptr, Values_FIELDS_size args_size) {
	Values_COALLOC(this->args, _ptr, args_size);
	
	return this;
}

//############################################
//
//############################################
void ValueLambda_check(Value v) {
	if (v->id != ValueId_Lambda) {
		printf("Type error, expected : \"Lambda\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueLambda_free(ValueLambda this) {
	Ptr first = Values_COPTR(this->args);
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueLambda_clone(ValueLambda v) {
	return ValueLambda_copy(v, ValueLambda_alloc(v->args.size));
}

Value ValueLambda_copy(ValueLambda src, ValueLambda tgt) {
	tgt->id = ValueId_Lambda;
	tgt->lambda = src->lambda;
	tgt->arity = src->arity;
	Values_COPY(src->args, tgt->args);
	return (Value) tgt;
}

Value ValueLambda_fill(ValueLambda src, ValueLambda tgt) {
	ValueLambda_realloc(tgt, src->args.size);
	return ValueLambda_copy(src, tgt);
}

//######################
// CLASS Object
//######################
Value ValueObject_make(ObjectId objectId, Ptr object, Nat arity, Values args) {
	return ValueObject_init(ValueObject_alloc(args.size), objectId, object, arity, args);
}

size_t ValueObject_sizeof(Values_FIELDS_size args_size) {
	return sizeof(S_ValueObject) + sizeof(Values_ALLOCTYPE[args_size]) + 0;
}

ValueObject ValueObject_alloc(Values_FIELDS_size args_size) {
	Byte* _ptr = Allocator_alloc(ValueObject_sizeof(args_size));
	ValueObject this = (ValueObject) _ptr;
	this->id = ValueId_Object;
	ValueObject_coalloc(this, _ptr + sizeof(S_ValueObject), args_size);
	return this;
}

Value ValueObject_init(ValueObject this, ObjectId objectId, Ptr object, Nat arity, Values args) {
	this->id = ValueId_Object;
	this->objectId = objectId;
	this->object = object;
	this->arity = arity;
	Values_COPY(args, this->args);
	return (Value) this;
}

ValueObject ValueObject_realloc(ValueObject this, Values_FIELDS_size args_size) {
	Byte* _ptr = Allocator_alloc(sizeof(Values_ALLOCTYPE[args_size]) + 0);
	ValueObject_coalloc(this, _ptr, args_size);
	return this;
}

ValueObject ValueObject_coalloc(ValueObject this, Byte* _ptr, Values_FIELDS_size args_size) {
	Values_COALLOC(this->args, _ptr, args_size);
	
	return this;
}

//############################################
//
//############################################
void ValueObject_check(Value v) {
	if (v->id != ValueId_Object) {
		printf("Type error, expected : \"Object\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void ValueObject_free(ValueObject this) {
	Ptr first = Values_COPTR(this->args);
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value ValueObject_clone(ValueObject v) {
	return ValueObject_copy(v, ValueObject_alloc(v->args.size));
}

Value ValueObject_copy(ValueObject src, ValueObject tgt) {
	tgt->id = ValueId_Object;
	tgt->objectId = src->objectId;
	tgt->object = src->object;
	tgt->arity = src->arity;
	Values_COPY(src->args, tgt->args);
	return (Value) tgt;
}

Value ValueObject_fill(ValueObject src, ValueObject tgt) {
	ValueObject_realloc(tgt, src->args.size);
	return ValueObject_copy(src, tgt);
}

