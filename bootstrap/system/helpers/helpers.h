
Int			parseInt					(String str, Nat len);
Value		dispatch_error				(char* id, char* info, Nat count, Value* values);
Value		readStream0				(Stream0 s);
Value		readStream				(Stream0 s, Nat size);
