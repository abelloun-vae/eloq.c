#include "../../main.h"

Int parseInt(String str, Nat len) {
	if (str[0] == '-') return -parseInt(str + 1, len - 1);
	Nat r = 0;
	for(Nat i = 0; i < len; i++)
		if ('0' <= str[i] && str[i] <= '9')
			r = (r << 3) + (r << 1) + (str[i] - '0');
		else break;
	return r;
}

Value dispatch_error(char* id, char* info, Nat count, Value* values) {
	printf("\n%s not dispatched for reason \"%s\" on %u object(s) :\n", id, info, count);
	for(Nat i = 0; i < count; i++) {
		Value_printInfo(values[i]);
		printf("\n");
	}
	exit(1);
	return 0;
}

Value readStream0(Stream0 s) {
	S_Writer w;
	WriterString_open(&w);
	Writer_writeStream0(&w, s);
	WriterString_close(&w);
	return ValueString_make(w.length, w.state.string.str);
}

Value readStream(Stream s, Nat size) {
	S_Writer w;
	WriterString_open(&w);
	Writer_writeStream(&w, s, size);
	WriterString_close(&w);
	return ValueString_make(w.length, w.state.string.str);
}

