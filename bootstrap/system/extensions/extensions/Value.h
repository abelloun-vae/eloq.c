Value			Value_alloc				();
void*			Value_escape				(void* v);
Value			Value_toBoolean				(Value this);
Value			Value_reflexivity			(Value l, Value r);
//##########################################
Nat			Value_print				(Value this, Value_WriterMode mode);
Nat			Value_printString			(Value this);
Nat			Value_printSerial			(Value this);
Nat			Value_printInfo				(Value this);