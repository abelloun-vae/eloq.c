Value Value_alloc() {
	return Allocator_alloc(sizeof(S_Value));
}

void* Value_escape(void* v) {
	if (Allocator_keep(v))
		return v;
	return Value_clone(v);
}

Value Value_toBoolean(Value v) {
	return ValueBoolean_make(Value_toBoo(v));
}

Value Value_reflexivity(Value l, Value r) {
	return ValueBoolean_make(Value_refl(l, r));
}

//##########################################

Nat Value_print(Value this, Value_WriterMode mode) {
	S_Writer w;
	Value_write(this, WriterPrint_open(&w), mode);
	return w.length;
}

Nat Value_printString(Value this) {
	return Value_print(this, Value_WriterMode_string);
}

Nat Value_printSerial(Value this) {
	return Value_print(this, Value_WriterMode_serial);
}

Nat Value_printInfo(Value this) {
	return Value_print(this, Value_WriterMode_info);
}
