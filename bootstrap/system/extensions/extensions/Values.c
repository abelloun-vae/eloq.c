
Boo Values_refl(Values lvs, Values rvs) {
	if (lvs.size != rvs.size)
		return 0;
	for(Nat i = 0; i < lvs.size; i++)
		if(!Value_refl(lvs.values[i], rvs.values[i]))
			return 0;
	return 1;
}