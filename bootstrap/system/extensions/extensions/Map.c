
//~ Boo Values_refl(Values lvs, Values rvs) {
	//~ if (lvs.size != rvs.size)
		//~ return 0;
	//~ for(Nat i = 0; i < lvs.size; i++)
		//~ if(!Value_refl(lvs.pairs[i], rvs.pairs[i]))
			//~ return 0;
	//~ return 1;
//~ }

//######################
//
//######################
Nat Map_concat(Map tgt, Map l, Map r) {
	Nat c = 0;
	for(Nat i = 0; i < l.size; i++)
		if (l.pairs[i][0])
			c += Map_add(tgt, l.pairs[i][0], l.pairs[i][1]);
	for(Nat i = 0; i < r.size; i++)
		if (r.pairs[i][0]) c += Map_add(tgt, r.pairs[i][0], r.pairs[i][1]);
	return c;
}

Value Map_access(Map map, Value key) {
	Nat hash = Value_toHash32(key) % map.size;
	for(Nat i = hash; i < map.size; i++) {
		if (!map.pairs[i][0])
			goto error;
		if (Value_refl(map.pairs[i][0], key))
			return map.pairs[i][1];
	}
	for(Nat i = 0; i < hash; i++) {
		if (!map.pairs[i][0])
			goto error;
		if (Value_refl(map.pairs[i][0], key))
			return map.pairs[i][1];
	}
	error:
	printf("Map key missing : ");
	Value_printSerial(key);
	printf("\n");
	exit(1);
}

Nat Map_add(Map map, Value key, Value val) {
	Nat c;
	Nat hash = Value_toHash32(key) % map.size;
	for(Nat i = hash; i < map.size; i++) {
		if (!map.pairs[i][0] || Value_refl(map.pairs[i][0], key)) {
			c = map.pairs[i][0] ? 0 : 1;
			map.pairs[i][0] = key;
			map.pairs[i][1] = val;
			return c;
		}
	}
	for(Nat i = 0; i < hash; i++) {
		if (!map.pairs[i][0] || Value_refl(key, map.pairs[i][0])) {
			c = map.pairs[i][0] ? 0 : 1;
			map.pairs[i][0] = key;
			map.pairs[i][1] = val;
			return c;
		}
	}
	printf("Panic in Map_add !!!");
	exit(1);
}
