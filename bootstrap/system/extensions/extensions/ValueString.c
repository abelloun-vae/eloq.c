
//~ Value ValueString_access(ValueString l, ValueInteger r) {
	//~ if (r->i < 0)
		//~ return dispatch_error("ValueString_access", "index must be greater or equal to zero", 2, (Value[]) {(Value) l, (Value) r});
	//~ if (((Nat)r->i) >= l->len)
		//~ return dispatch_error("ValueString_access", "index must be smaller than length", 2, (Value[]) {(Value) l, (Value) r});
	//~ return ValueChar_make(l->str[r->i]);
//~ }

Value ValueString_template(ValueString format, Nat size, Value data[size]) {

	Nat j = 0;
	S_Writer w;
	WriterString_open(&w);

	for(Nat i = 0; i < format->len; i++)
		if (format->str[i] == '%' && i + 1 < format->len) switch(format->str[i+1]) {

			case '%':
				Writer_writeChar(&w, format->str[i]);
				i++;
				break;

			case 's':
				Value_write(data[j], &w, Value_WriterMode_string);
				i++;
				j++;
				break;

			default:
				Writer_writeChar(&w, format->str[i]);

		} else Writer_writeChar(&w, format->str[i]);

	ValueString vstr = ValueString_alloc(0);
	vstr->len = w.length;
	vstr->str = w.state.string.str;
	return (Value) vstr;

}
