//~ //######################
//~ //
//~ //######################
//~ Value ValueMapExtension_concat(ValueMap l, ValueMap r) {
	//~ ValueMap arr = ValueMap_alloc(l->count + r->count);
	//~ arr->record = l->record || r->record;
	//~ arr->count = 0;
	//~ for(Nat i = 0; i < (l->count + r->count); i++)
		//~ arr->map.values[i * 2] = 0;
	//~ for(Nat i = 0; i < l->map.size; i++)
		//~ if (l->map.values[2 * i]) ValueMapExtension_add(arr, l->map.values[2 * i], l->map.values[2 * i+1]);
	//~ for(Nat i = 0; i < r->map.size; i++)
		//~ if (r->map.values[2 * i]) ValueMapExtension_add(arr, r->map.values[2 * i], r->map.values[2 * i+1]);
	//~ return (Value) arr;
//~ }

//~ Value ValueMapExtension_access(ValueMap map, Value key) {
	//~ Nat hash = Value_toHash32(key) % map->map.size;
	//~ for(Nat i = 0; i < map->map.size; i++) {
		//~ if (!map->map.values[2 * hash])
			//~ break;
		//~ if (Value_refl(key, map->map.values[2 * hash]))
			//~ return map->map.values[2 * hash + 1];
		//~ hash = (hash + 1) % map->map.size;
	//~ }
	//~ printf("Map key missing : ");
	//~ Value_printSerial(key);
	//~ printf("\n");
	//~ exit(1);
//~ }

//~ void ValueMapExtension_add(ValueMap map, Value key, Value val) {
	//~ Nat hash = Value_toHash32(key) % map->map.size;
	//~ for(Nat i = 0; i < map->map.size; i++) {
		//~ if (!map->map.values[2 * hash] || Value_refl(key, map->map.values[2 * hash])) {
			//~ map->count += map->map.values[2 * hash] ? 0 : 1;
			//~ map->map.values[2 * hash] = key;
			//~ map->map.values[2 * hash + 1] = val;
			//~ break;
		//~ }
		//~ hash = (hash + 1) % map->map.size;
	//~ }
//~ }

//~ Boo Values_refl(Values lvs, Values rvs) {
	//~ if (lvs.size != rvs.size)
		//~ return 0;
	//~ for(Nat i = 0; i < lvs.size; i++)
		//~ if(!Value_refl(lvs.values[i], rvs.values[i]))
			//~ return 0;
	//~ return 1;
//~ }


//~ Value ValueMap_diffkeys(ValueMap l, ValueMap r) {
	//~ ValueMap arr = ValueMap_alloc(l->count);
	//~ for(Nat i = 0; i < l->map.size; i++)
		//~ if (l->map.values[2 * i]) ValueMap_Add(arr, l->map.values[2 * i], l->map.values[2 * i+1]);
	//~ for(Nat i = 0; i < r->map.size; i++)
		//~ if (r->map.values[2 * i]) ValueMap_Rem(arr, r->map.values[2 * i]);
	//~ return (Value) arr;
//~ }


//~ void ValueMap_Rem(ValueMap map, Value key) {
	//~ Nat hash = Value_toHash32(key) % map->map.size;
	//~ for(Nat i = 0; i < map->map.size; i++) {
		//~ if (!map->map.values[2 * hash] || Value_refl(key, map->map.values[2 * hash])) {
			//~ map->count -= map->map.values[2 * hash] ? 1 : 0;
			//~ map->map.values[2 * hash] = 0;
			//~ map->map.values[2 * hash + 1] = 0;
			//~ break;
		//~ }
		//~ hash = (hash + 1) % map->map.size;
	//~ }
//~ }
