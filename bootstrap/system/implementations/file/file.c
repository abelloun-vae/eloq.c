Value implementation_file_getString(Values args) {
	ValueString fpath = (ValueString) args.values[0];
	FILE* fsrc = File_open(fpath->str, fpath->len, "rb");

	fseek(fsrc, 0, SEEK_END);
	off_t tot = ftell(fsrc);
	fseek(fsrc, 0, SEEK_SET);

	S_Writer w;
	WriterString_open(&w);
	WriterString_expand(&w, tot * 4);
	Writer_writeFile(&w, fsrc, 4096);
	WriterString_close(&w);

	fclose(fsrc);
	return ValueString_make(w.length, w.state.string.str);
}

Value implementation_file_getStream(Values args) {
	ValueString fpath = (ValueString) args.values[0];
	FILE* fsrc = File_open(fpath->str, fpath->len, "rb");

	fseek(fsrc, 0, SEEK_END);
	off_t tot = ftell(fsrc);
	fseek(fsrc, 0, SEEK_SET);


	S_Writer w;
	WriterStream_open(&w);
	WriterStream_expand(&w, tot);
	Writer_writeFile(&w, fsrc, 4096);
	WriterStream_close(&w);

	fclose(fsrc);
	return ValueStream_make(w.length, w.state.stream.stm);
}

Value implementation_file_putStream(Values args) {
	ValueString fpath = (ValueString) args.values[0];
	ValueStream stm = (ValueStream) args.values[1];
	FILE* ftgt = File_open(fpath->str, fpath->len, "wb");

	S_Writer w;
	WriterFile_open(&w, ftgt);
	Writer_writeStream(&w, stm->stm, stm->len);
	WriterFile_close(&w);

	fclose(ftgt);
	return ValueInteger_make(w.length);
}

Value implementation_file_copy(Values args) {
	ValueString src = (ValueString) args.values[0];
	ValueString tgt = (ValueString) args.values[1];
	FILE* fsrc = File_open(src->str, src->len, "rb");
	FILE* ftgt = File_open(tgt->str, tgt->len, "wb");

	S_Writer w;
	WriterFile_open(&w, ftgt);
	Writer_writeFile(&w, fsrc, 4096);
	WriterFile_close(&w);

	fclose(fsrc);
	fclose(ftgt);
	return ValueInteger_make(w.length);
}
