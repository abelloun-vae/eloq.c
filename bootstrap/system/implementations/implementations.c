#include "../../main.h"
#include "file/file.c"
#include "array/array.c"
#include "string//string.c"
#include "stream/stream.c"

Value implementation() {
	return ValueMap_make(0, (Map) {11, (Value[][2]) {

		readStream0((Stream0) "array_length"), ValueLambda_make(&implementation_array_length, 1, (Values) {0, 0}),
		readStream0((Stream0) "array_map"), ValueLambda_make(&implementation_array_map, 2, (Values) {0, 0}),
		readStream0((Stream0) "array_foldl"), ValueLambda_make(&implementation_array_foldl, 3, (Values) {0, 0}),
		readStream0((Stream0) "array_foldr"), ValueLambda_make(&implementation_array_foldr, 3, (Values) {0, 0}),
		readStream0((Stream0) "array_filter"), ValueLambda_make(&implementation_array_filter, 2, (Values) {0, 0}),

		readStream0((Stream0) "string_length"), ValueLambda_make(&implementation_string_length, 1, (Values) {0, 0}),

		readStream0((Stream0) "stream_length"), ValueLambda_make(&implementation_stream_length, 1, (Values) {0, 0}),

		readStream0((Stream0) "file_copy"), ValueLambda_make(&implementation_file_copy, 2, (Values) {0, 0}),
		readStream0((Stream0) "file_getString"), ValueLambda_make(&implementation_file_getString, 1, (Values) {0, 0}),
		readStream0((Stream0) "file_getStream"), ValueLambda_make(&implementation_file_getStream, 1, (Values) {0, 0}),
		readStream0((Stream0) "file_putStream"), ValueLambda_make(&implementation_file_putStream, 2, (Values) {0, 0}),

	}});
}
