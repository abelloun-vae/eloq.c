
Value implementation_array_length(Values args) {
	return ValueInteger_make(((ValueArray) args.values[0])->data.size);
}

Value implementation_array_map(Values args) {
	//#################
	Value f = args.values[0];
	ValueArray arr = (ValueArray) args.values[1];
	//#################
	ValueArray narr = ValueArray_alloc(arr->data.size);
	//#################
	narr->tuple = 0;
	narr->data.size = arr->data.size;
	for(Nat i = 0; i < arr->data.size; i++)
		narr->data.values[i] = Ast_apply(f, arr->data.values[i]);
	return (Value) narr;
}

Value implementation_array_foldr(Values args) {
	//#################
	Value f = args.values[0];
	Value i = args.values[1];
	ValueArray arr = (ValueArray) args.values[2];
	//#################
	for(Nat j = 1; j <= arr->data.size; j++)
		i = Ast_apply(Ast_apply(f, arr->data.values[arr->data.size - j]), i);
	return i;
}

Value implementation_array_foldl(Values args) {
	//#################
	Value f = args.values[0];
	Value i = args.values[1];
	ValueArray arr = (ValueArray) args.values[2];
	//#################
	for(Nat j = 0; j < arr->data.size; j++)
		i = Ast_apply(Ast_apply(f, i), arr->data.values[j]);
	return i;
}

Value implementation_array_filter(Values args) {
	//#################
	Value f = args.values[0];
	ValueArray arr = (ValueArray) args.values[1];
	//#################
	ValueArray narr = ValueArray_alloc(arr->data.size);
	narr->data.size = 0;
	for(Nat i = 0; i < arr->data.size; i++)
		if (Ast_apply(f, arr->data.values[i])) {
			narr->data.values[narr->data.size] = arr->data.values[i];
			narr->data.size++;
		}
	return (Value) narr;
}
