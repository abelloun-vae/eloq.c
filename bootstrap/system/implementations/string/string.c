
Value implementation_string_length(Values args) {
	return ValueInteger_make(((ValueString) args.values[0])->len);
}

//~ Value implementation_string_parseInt(Values args) {
	//~ ValueString str = (ValueString) args.values[0];
	//~ return ValueInteger_make(parseInt(str->str, str->len));
//~ }
