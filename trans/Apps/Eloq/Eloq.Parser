\EXPParser GRAParser MKLParser -> {:
	#/*****************/
	#// External Interface
	#/*****************/
	Module		-> EXPParser:Module
	!spaces		-> /\s++/
	#/*****************/
	#// Wiring
	#/*****************/
	Expression	-> EXPParser:Embed
	Dialect		-> MKLMode MKLParser:Tree	=> [ Mark this.0 this.1 ]
			-> GRAMode GRAParser:Grammar	=> [ Gram this.0 this.1 ]

	MKLMode		-> "@Markup" | "@Template" | ["@Markup"]
	GRAMode		-> "@Grammar" | "@Parser" | ["@Parser"]

	#/*****************/
	#// Parsers Instances
	#/*****************/
	EXPParser	-> [* EXPParser ](COMParser, Dialect)
	GRAParser	-> [* GRAParser ](COMParser, Expression)
	MKLParser	-> [* MKLParser ](COMParser, EXPParser)

	#/*****************/
	#// Standard Tokens
	#/*****************/
	COMParser	-> {:
		#/*****************/
		#// comments
		#/*****************/
		comments	-> "#"? olcomments | mlcomments
		olcomments	-> "//" /.++/?
		mlcomments	-> "/*" { "*/"! /(\*)?([^*]++)?/ }* "*/"
		eof		-> ( /( |\t)++/ | comments+ | { "\n%%"! "\n" } )* { "\n"? "%%" { /\s+/ /(.|\n)++/ }? }?
		#/*****************/
		#// Base
		#/*****************/
		identifier	-> /[a-zA-Z_]([a-zA-Z_0-9]++)?/.0

		integer		-> /\d++/.0
				=> [ S.intval this ]

		regex		-> /\/(?!\*)((?:(?:\\(?:.|\n)|[^\/])++)?)\/([a-zA-Z]*)/
				=> [ [regex: this.0, pattern: this.1, modifier: this.2] ]
		#/******************/
		#// Simple Quotes
		#/******************/
		splQuotes	-> "'" _splQuotes* "'"
				=> [ L.implode "" this.1 ]
		_splQuotes	-> { "\\" "\\" }.1
				-> { "\\" "'" }.1
				-> /[^']/.0
		#/******************/
		#// Double Quotes
		#/******************/
		dblQuotes	-> "\"" _dblQuotes* "\""
				=> [ L.implode "" this.1 ]
		_dblQuotes	-> "\\" "n" => [ "\n" ]
				-> "\\" "t" => [ "\t" ]
				-> { "\\" "\\" }.1
				-> { "\\" "\"" }.1
				-> /[^\"]/.0
		#/******************/
		#// Template Quotes
		#/******************/
		tplQuotes(X)	-> "\"" _tplQuotes(X)* "\""
				=> [ {
					L.implode '' $ L.mapf this.1 \e -> S.isString e ? e == "%" ? "%%" : e : '%s',
					L.filter (\e -> !S.isString e) this.1
				} ]

		_tplQuotes(X)	-> "\\" "n" => [ "\n" ]
				-> "\\" "t" => [ "\t" ]
				-> { "{" 0=X "}" }.1
				-> { "\\" "{" }.1
				-> { "\\" "}" }.1
				-> { "\\" "\\" }.1
				-> { "\\" "\"" }.1
				-> /[^\"]/.0
		#/******************/
		#// Template
		#/******************/
		template(X)	-> _template(X)*
				=> [ {
					L.implode '' $ L.mapf this \e -> S.isString e ? e == "%" ? "%%" : e : '%s',
					L.filter (\e -> !S.isString e) this
				} ]

		_template(X)	-> { "{" 0=X "}" }.1
				-> { "\\" "{" }.1
				-> { "\\" "}" }.1
				-> /.|\n/.0
	}
}
