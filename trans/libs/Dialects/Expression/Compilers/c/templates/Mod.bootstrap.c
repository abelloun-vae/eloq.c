#include "../main.h"
//###########################################################
// Body
//###########################################################
void ValueName_writeName(ValueName v, Writer w) {
	switch (v->name) {
		{A.implode "\n\t\t" $ A.zip (\ful k -> "case NameId_Data_{ful}: Writer_writeStream0(w, (Stream0) \"{k}\"); break;") fa.dataIds (A.keys fa.dataIds)}
		{A.implode "\n\t\t" $ A.zip (\ful k -> "case NameId_Type_{ful}: Writer_writeStream0(w, (Stream0) \"{k}\"); break;") fa.typeIds (A.keys fa.typeIds)}
		{A.implode "\n\t\t" $ A.zip (\ful k -> "case NameId_Name_{ful}: Writer_writeStream0(w, (Stream0) \"{k}\"); break;") fa.nameIds (A.keys fa.nameIds)}
		default: Writer_writeStream0(w, (Stream0) "UNKNOWN");
	}
}

//###########
// lifted lambdas
//###########
{A.implode "\n\n" fa.func}

//######################
// Lambda
//######################
Value lambda(Value _variable_implementations) {
	return {indents 1 a};
}
