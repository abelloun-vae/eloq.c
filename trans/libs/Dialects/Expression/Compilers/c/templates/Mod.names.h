#ifndef NAMES_H
#define NAMES_H

	typedef enum		NameId {
		_NameId_MIN_,
		{A.implode "\n\t\t" $ A.mapf fa.dataIds \d -> "NameId_Data_{d},"}
		{A.implode "\n\t\t" $ A.mapf fa.typeIds \d -> "NameId_Type_{d},"}
		{A.implode "\n\t\t" $ A.mapf fa.nameIds \d -> "NameId_Name_{d},"}
		_NameId_MAX_,
	}							NameId;

#endif
