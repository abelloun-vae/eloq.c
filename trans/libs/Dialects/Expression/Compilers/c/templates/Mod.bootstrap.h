#ifndef BOOTSTRAP_H
#define BOOTSTRAP_H

	//######################
	// Body
	//######################
	void		ValueName_writeName			(ValueName v, Writer w);

	//######################
	// lifted lambdas
	//######################
	{A.implode "\n\t" $ A.keys fa.func}

	//######################
	// Lambda
	//######################
	Value lambda(Value _variable_implementations);

#endif
