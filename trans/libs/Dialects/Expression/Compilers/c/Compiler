\xdata -> {

	init		= {}
	compile		= \data ast -> Classes.hylo algebra codata coalg compile data ast
	icompile	= compile init


	indent		= ($) . indents 1
	indents		= \n str -> S.replace "\n" "\n{S.repeat "\t" n}" str
	mkLbdId		= \id path -> "{id}{S.replace ['@', '.'] ['_aro_', '_dot_'] path}"

	coalg		= \data ast -> {
		ast	-> ast :
		Obj o	-> Obj $ OBJECT.compile xdata data o
	}

	codata		= \data ast -> Classes.zipo {
		ctx	= Names.location data.ctx ast
		path	= Names.path data.path ast
	}

	compileAlloc	= \e -> {
		e.ast, e.frec	-> throw "Not implementented compileAlloc {toString e}" :
		_,	Nfo _ a	-> compileAlloc a
		Obj _		-> "Value_alloc()"
		Var v		-> "Value_alloc()"
		_, Sco _ (fe,)	-> "Value_alloc()"
		App _ _		-> "Value_alloc()"
		Acc l r		-> "Value_alloc()"
		BOp o l r	-> "Value_alloc()"
		TOp c t f	-> "Value_alloc()"
		UOp _ _		-> "Value_alloc()"
		Cpr o l r	-> "Value_alloc()"
		Coa _ l r	-> "Value_alloc()"
		Match _ _	-> "Value_alloc()"




		Nil		-> "ValueNil_alloc()"
		Cns _ _		-> "ValueCons_alloc()"
		Boo _		-> "ValueBoolean_alloc()"
		Int _		-> "ValueInteger_alloc()"
		Cmp _ _		-> "ValueClosure_alloc(2)"
		Dat i n as	-> "ValueName_alloc({L.len as})"
		Typ i n as	-> "ValueName_alloc({L.len as})"
		Nam i n as	-> "ValueName_alloc({L.len as})"
		Str s		-> "ValueString_alloc({S.len s})"
		Arr as		-> "ValueArray_alloc({L.len as})"
		Tup as		-> "ValueArray_alloc({L.len as})"
		Map ps		-> "ValueMap_alloc({L.len ps})"
		Rcd ps		-> "ValueMap_alloc({L.len ps})"
		_,	Lam v b	-> "ValueClosure_alloc({A.len $ b.free --- [(v): 1]})"



	}

	compileInit	= \v rec -> {
		Classes.fmap (.code) rec.frec, rec.frec -> throw "Not implementented compileInit {toString rec}" :


		Obj a		-> "ValueObject_init((ValueObject) {v}, ObjectId_Parser, {a.code}, 3, (Values) {0, 0});"

		Nil		-> "ValueNil_init((ValueNil) {v});"
		Cns l r		-> "ValueCons_init((ValueCons) {v},\n\t{indents 1 l},\n\t{indents 1 r}\n);"

		Dat i n ()	-> "ValueName_init((ValueName) {v}, NameId_Data_{n}_{i}, {i}, (Values) {0, 0});"
		Dat i n (a,)	-> "ValueName_init((ValueName) {v}, NameId_Data_{n}_{i + 1}, {i}, (Values) \{1, (Value[]) \{{a}}});"
		Dat i n as	-> "ValueName_init((ValueName) {v}, NameId_Data_{n}_{i + L.len as}, {i}, (Values) \{{L.len as}, (Value[]) \{\n\t{L.implode ', ' $ L.fmap indent as}\n}});"

		Typ i n ()	-> "ValueName_init((ValueName) {v}, NameId_Type_{n}_{i}, {i}, (Values) {0, 0});"
		Typ i n (a,)	-> "ValueName_init((ValueName) {v}, NameId_Type_{n}_{i + 1}, {i}, (Values) \{1, (Value[]) \{{a}}});"
		Typ i n as	-> "ValueName_init((ValueName) {v}, NameId_Type_{n}_{i + L.len as}, {i}, (Values) \{{L.len as}, (Value[]) \{\n\t{L.implode ', ' $ L.fmap indent as}\n}});"

		Nam i n ()	-> "ValueName_init((ValueName) {v}, NameId_Name_{n}_{i}, {i}, (Values) {0, 0});"
		Nam i n (a,)	-> "ValueName_init((ValueName) {v}, NameId_Name_{n}_{i + 1}, {i}, (Values) \{1, (Value[]) \{{a}}});"
		Nam i n as	-> "ValueName_init((ValueName) {v}, NameId_Name_{n}_{i + L.len as}, {i}, (Values) \{{L.len as}, (Value[]) \{\n\t{L.implode ', ' $ L.fmap indent as}\n}});"

		_,	Nfo _ a	-> compileInit v a
		Var w		-> "{v} = _variable_{w};"
		App l r		-> "Value_fill(Ast_apply(\n\t{indent l},\n\t{indent r}\n), {v});"
		Acc l r		-> "Value_fill(Ast_access(\n\t{indent l},\n\t{indent r}\n), {v});"


		_, Sco _ (fe,)	-> {lbdid = mkLbdId "_sco_" rec.path}
				-> "Value_fill(all_{lbdid}({A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys rec.free}), {v});"


		Str s		-> "Value_fill(readStream0((Stream0) \"{S.serialize s}\"), {v});"

		Boo b		-> "ValueBoolean_init((ValueBoolean) {v}, {b ? 'TRUE' : 'FALSE'});"
		Int i		-> "ValueInteger_init((ValueInteger) {v}, {i});"
		Cmp l r		-> "ValueClosure_init((ValueClosure) {v}, &Value_compose, (Values) \{2, (Value[]) \{{l}, {r}}});"


		_, Lam w b 	-> {free = b.free --- [(w): 1]; lbdid = mkLbdId "_lambda_" rec.path}
				-> "ValueClosure_init((ValueClosure) {v}, &{lbdid}, (Values) \{{A.len free}, (Value[]) \{{A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys free}}});"

		Arr ()		-> "ValueArray_init((ValueArray) {v}, 0, (Values) {0, 0});"
		Arr (a,)	-> "ValueArray_init((ValueArray) {v}, 0, (Values) {1, (Value[]) \{{a}}});"
		Arr as		-> "ValueArray_init((ValueArray) {v}, 0, (Values) {{L.len as}, (Value[]) \{\n\t{L.implode ",\n\t" $ L.fmap indent as}\n}});"

		Tup ()		-> "ValueArray_init((ValueArray) {v}, 1, (Values) {0, 0});"
		Tup (a,)	-> "ValueArray_init((ValueArray) {v}, 1, (Values) {1, (Value[]) \{{a}}});"
		Tup as		-> "ValueArray_init((ValueArray) {v}, 1, (Values) {{L.len as}, (Value[]) \{\n\t{L.implode ",\n\t" $ L.fmap indent as}\n}});"

		Map ()		-> "ValueMap_init((ValueMap) {v}, 0, (Map) {0, 0});"
		Map ({k, w},)	-> "ValueMap_init((ValueMap) {v}, 0, (Map) {1, (Value[][2]) \{{k}, {w}}});"
		Map ps		-> "ValueMap_init((ValueMap) {v}, 0, (Map) {{L.len ps}, (Value[][2]) \{\n\t{L.implode ",\n\t" $ L.mapf ps \p -> "{indent p.0}, {indent p.1}"}\n}});"

		Rcd ()		-> "ValueMap_init((ValueMap) {v}, 1, (Map) {0, 0});"
		Rcd ({k, w},)	-> "ValueMap_init((ValueMap) {v}, 1, (Map) {1, (Value[][2]) \{{k}, {w}}});"
		Rcd ps		-> "ValueMap_init((ValueMap) {v}, 1, (Map) {{L.len ps}, (Value[][2]) \{\n\t{L.implode ",\n\t" $ L.mapf ps \p -> "{indent p.0}, {indent p.1}"}\n}});"


		TOp c t f	-> "Value_fill(((ValueBoolean) ({c}))->b ? {t} : {f}, {v});"
		UOp "!" a	-> "Value_fill(ValueBoolean_make(!((ValueBoolean) ({a})->b)), {v});"
		UOp "-" a	-> "Value_fill(ValueInteger_make(-((ValueInteger) ({a})->i)), {v});"

		BOp "+++" l r	-> "Value_fill(Value_concat({l}, {r}), {v});"
		BOp "---" l r	-> "Value_fill(Valuediffkeys({l}, {r}), {v});"
		BOp "++" l r	-> "Value_fill(Valueconcat({l}, {r}), {v});"

		BOp "**" l r	-> "Value_fill(Value_pow({l}, {r}), {v});"
		BOp "+" l r	-> "Value_fill(Value_add({l}, {r}), {v});"
		BOp "-" l r	-> "Value_fill(Value_dif({l}, {r}), {v});"
		BOp "*" l r	-> "Value_fill(Value_mul({l}, {r}), {v});"
		BOp "/" l r	-> "Value_fill(Value_div({l}, {r}), {v});"
		BOp "/+" l r	-> "Value_fill(Value_divc({l}, {r}), {v});"
		BOp "/=" l r	-> "Value_fill(Value_divr({l}, {r}), {v});"
		BOp "/-" l r	-> "Value_fill(Value_divf({l}, {r}), {v});"
		BOp "%" l r	-> "Value_fill(Value_mod({l}, {r}), {v});"


		Cpr "==" l r	-> "Value_fill(Value_reflexivity({l}, {r}), {v});"
		Cpr "!=" l r	-> "Value_fill(ValueBoolean_make(!Value_reflexivity({l}, {r})), {v});"
		Cpr "><" l r	-> "Value_fill(Value_xwing({l}, {r}), {v});"
		Cpr o l r	-> "Value_fill(({l} {o} {r}), {v});"

		Coa "??" l r,
		Coa _ fl fr	-> {
					lbdid = mkLbdId "_coaAND_" rec.path;
					free = !fr.free ? "" : ", " ++ A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys fr.free
				} -> "Value_fill({lbdid}({l} {free}), {v});"

		Coa "?:" l r,
		Coa _ fl fr	-> {
					lbdid = mkLbdId "_coaOR_" rec.path;
					free = !fr.free ? "" : ", " ++ A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys fr.free
				} -> "Value_fill({lbdid}({l} {free}), {v});"


		Match e bs,
		Match fe fbs	-> {lbdid = mkLbdId "_match_" rec.path}
				-> "Value_fill({lbdid}({A.implode ', ' $ [e] +++ A.fmap (\v -> "_variable_{v}") $ A.keys rec.free}), {v});"

	}


	compilePattern = \ast value -> patternAlg (Classes.zip compilePattern ast $ patternValueAlg ast value) value

	patternValueAlg	= \ast value -> {
		ast			-> throw "PatternValueAlg::MISSING : {toString ast}" :
		Var v			-> Var v
		Int n			-> Int n
		Str s			-> Str s
		Nil			-> Nil
		Cns l r			-> Cns "((ValueCons) {value})->head" "((ValueCons) {value})->rest"
		Tup ps			-> Tup $ L.fmap (\n -> "((ValueArray) {value})->data.values[{n}]") $ I.toList $ L.len ps
		Dat i n as		-> Dat i n $ L.fmap (\n -> "((ValueName) {value})->data.values[{n}]") $ I.toList $ L.len as
	}

	patternAlg	= \ast value -> {
		ast			-> throw "PatternAlg::MISSING : {toString ast}" :
		Var v	/ v == '_'	-> ()
		Var v			-> ("Value _variable_{v} = {value};",)
		Int i			-> ("if ( ((ValueInteger) ({value}))->i != {i} ) return 0;",)

		Str s			-> ("if ( !Value_refl(readStream0((Stream0) \"{S.serialize s}\"), {value}) ) return 0;",)

		Tup as			-> L.reduce L.concat () as
		Nil			-> ("if ( {value}->id != ValueId_Nil ) return 0;",)

		Cns l r 		-> "if ( {value}->id != ValueId_Cons ) return 0;" :> L.concat l r
		Dat i n as ->
			"if ( {value}->id != ValueId_Data ) return 0;" :>
			"if ( ((ValueName) {value})->name != NameId_{n}_{L.len as + i} ) return 0;" :>
			"if ( ((ValueName) {value})->arity != 0 ) return 0;" :>
			"if ( ((ValueName) {value})->values.size != {L.len as} ) return 0;" :>
			L.reduce L.concat () as
	}


	algebra		= \data frec -> {


		dataIds	= Classes.reduce (\a -> (+++) a . (.dataIds)) [] frec +++ {
			frec		-> [] :
			Sco stms _	-> A.reduce (+++) [] $
						A.fmap (\x -> {x: Dat i n _ -> ["{n}_{i}": "{n}_{i}"]}) $
						A.filter (\x -> {x -> 0 : Dat i n _ -> 1}) $
						Names.Scopes.scopeIn [] $
						L.fmap (Classes.Stmt.fmap (.ast)) stms
		}

		typeIds	= Classes.reduce (\a -> (+++) a . (.typeIds)) [] frec +++ {
			frec		-> [] :
			Sco stms _	-> A.reduce (+++) [] $
						A.fmap (\x -> {x: Typ i n _ -> ["{n}_{i}": "{n}_{i}"]}) $
						A.filter (\x -> {x -> 0 : Typ i n _ -> 1}) $
						Names.Scopes.scopeIn [] $
						L.fmap (Classes.Stmt.fmap (.ast)) stms
		}
		nameIds	= Classes.reduce (\a -> (+++) a . (.nameIds)) [] frec +++ {
			frec		-> [] :
			Sco stms _	-> A.reduce (+++) [] $
						A.fmap (\x -> {x: Nam i n _ -> ["{n}_{i}": "{n}_{i}"]}) $
						A.filter (\x -> {x -> 0 : Nam i n _ -> 1}) $
						Names.Scopes.scopeIn [] $
						L.fmap (Classes.Stmt.fmap (.ast)) stms
		}

		path	= data.path
		frec @ _= frec
		ast	= Classes.mapf frec (.ast)
		free	= ( Names.free $ Classes.mapf frec (.free) ) +++ {
			frec	-> [] :
			Obj a	-> a.expFree
		}

		func	= {
			frec	-> Classes.reduce (\a e -> a +++ e.func) [] frec :
			Mod a	-> []

			Obj a	-> a.func

			Coa "??" fl fr	-> {
				lbdid = mkLbdId "_coaAND_" data.path;
				free = !fr.free ? "" : ", " ++ A.implode ", " $ A.fmap (\e -> "Value _variable_{e}") $ A.keys fr.free
			} -> fl.func +++ fr.func +++ [
				"Value {lbdid}(Value left {free});": "Value {lbdid}(Value left {free}) {\n" ++
				"	return Value_toBoo(left) ? {fr.code} : left;\n" ++
				"}"
			]
			Coa "?:" fl fr	-> {
				lbdid = mkLbdId "_coaOR_" data.path
				free = !fr.free ? "" : ", " ++ A.implode ", " $ A.fmap (\e -> "Value _variable_{e}") $ A.keys fr.free
			} -> fl.func +++ fr.func +++ [
				"Value {lbdid}(Value left {free});": "Value {lbdid}(Value left {free}) {\n" ++
				"	return Value_toBoo(left) ? left : {fr.code};\n" ++
				"}"
			]

			Lam v fb-> {free = fb.free --- [(v): 1]; lbdid = mkLbdId "_lambda_" data.path} -> fb.func +++ [
				"Value {lbdid}(Values scope, Value _variable_{v});": "Value {lbdid}(Values scope, Value _variable_{v}) {\n" ++
				"	{A.implode "\n\t" $ A.zip (\x y -> "Value _variable_{x} = scope.values[{y}];") (A.keys free) (A.keys $ A.keys free)}\n" ++
				"	Allocator_push(0);\n" ++
				"	Value result = {indents 1 fb.code};\n" ++
				"	result = Allocator_pop(&Value_escape, result);\n" ++
				"	return result;\n" ++
				"}"
			]



			Match e brs 	-> {
						N = I.toList $ L.len brs
						lbdid = mkLbdId "_match_" data.path
						params = A.implode ", " $ A.fmap (\v -> "Value _variable_{v}") $ A.keys free
						sep = free ? ", " : ""
						rcall = \b -> "match = {b};\nif (match) return match;"
						getFree = \br -> br.expr.free +++ (br.gard == () ? [] : br.gard.$.free) --- Names.addParams Rho.zer br.patt
					mkArgs = \br -> {f = getFree br} -> !f ? "" : ", " ++ A.implode ", " $ A.fmap (\v -> "_variable_{v}") $ A.keys f
					mkParams = \br -> {f = getFree br} -> !f ? "" : ", " ++ A.implode ", " $ A.fmap (\v -> "Value _variable_{v}") $ A.keys f
					} -> Classes.reduce (\a e -> a +++ e.func) [] frec +++ [
						"Value {lbdid}(Value expr{sep}{params});": "Value {lbdid}(Value expr{sep}{params}) {\n" ++
						"	Value match = 0;\n" ++
						"	{L.implode "\n\t" $ L.zip (\br n -> rcall "_branch_{lbdid}_{n}(expr {mkArgs br})") brs N}\n" ++
						"	printf(\"No pattern\");\n" ++
						"	exit(1);\n" ++
						"}"
					] +++ L.reduce (+++) [] $ L.zip (\br n -> br.expr.func +++ (br.gard == () ? [] : br.gard.$.func) +++ [
						"Value _branch_{lbdid}_{n}(Value expr {mkParams br});": "Value _branch_{lbdid}_{n}(Value expr {mkParams br}) {\n" ++
						"	{L.implode "\n\t" $ compilePattern br.patt 'expr'}\n" ++
						(br.gard == () ? "" : "	if (!Value_toBoo({indent br.gard.$.code})) return 0;\n") ++
						"	return {indent br.expr.code};\n" ++
						"}"
					]) brs N


			Sco stms (fe,)-> {

				lbdid = mkLbdId "_sco_" data.path

				scopeOut = Names.Scopes.scopeOut [] $ L.fmap (Classes.Stmt.fmap (.ast)) stms
				freeIn = Names.Scopes.freeIn $ L.fmap (Classes.Stmt.fmap (.free)) stms

				fromTop = fe.free --- scopeOut
				fromOut = fe.free --- fromTop

				allParams = A.implode ", " $ A.fmap (\e -> "Value _variable_{e}") $ A.keys free
				eTopParams = A.implode ", " $ A.fmap (\e -> "Value _variable_{e}") $ A.keys fromTop
				eOutParams = A.implode ", " $ A.fmap (\e -> "Value _variable_{e}") $ A.keys fromOut
				sep = eTopParams ?? eOutParams ?? ", "


				outParams = !freeIn ? "" : ", " ++ A.implode ", " $ A.fmap (\e -> "Value _variable_{e}") $ A.keys freeIn
				outArgs = !freeIn ? "" : ", " ++ A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys freeIn


				allcs = L.filter ($) $ L.mapf stms \fstmt -> {
					fstmt		-> "" :
					Aff _ '_' _	-> ""
					Aff _ v e	-> "Value _variable_{v} = (Value) {compileAlloc e};"
				}

				inits = L.filter ($) $ L.mapf stms \fstmt -> {
					fstmt		-> "" :
					Aff _ '_' _	-> ""
					Aff _ v e	-> indent $ compileInit "_variable_{v}" e
				}

				compileOut	= L.filter ($) $ L.mapf stms \fstmt -> {
					fstmt		-> "" :
					Aff '_' _ _	-> ""
					Aff v '_' e	-> e.code
					Aff _ v _	-> "_variable_{v}"
				}
				N = I.toList $ L.len compileOut

				compileVar	= L.reduce (+++) [] $ L.zip (\i v -> [(v): i]) N $ L.filter ($) $ L.mapf stms \fstmt -> {
					fstmt		-> "" :
					Aff '_' _ _	-> ""
					Aff v '_' e	-> v
					Aff _ v _	-> v
				}

				eOutArgs = A.implode ", " $ A.fmap (\v -> "scope[{compileVar[v]}]") $ A.keys fromOut
				eTopArgs = A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys fromTop
				sepA = eTopArgs ?? eOutArgs ?? ", "

			} -> Classes.reduce (\a e -> a +++ e.func) [] frec +++ [

				"Value all_{lbdid}({allParams});": "Value all_{lbdid}({allParams}) {\n" ++
				"	Value scope[{L.len compileOut}];\n" ++
				"	out_{lbdid}(scope {outArgs});\n" ++
				"	return e_{lbdid}({eTopArgs}{sepA}{eOutArgs});\n" ++
				"}"

				"void out_{lbdid}(Value* scope {outParams});": "void out_{lbdid}(Value* scope {outParams}) {\n" ++
				"	{L.implode "\n\t" $ allcs}\n" ++
				"	{L.implode "\n\t" $ inits}\n" ++
				"	{L.implode "\n\t" $ L.zip (\e i -> "scope[{i}] = {indent e};") compileOut N}\n" ++
				"}"

				"Value e_{lbdid}({eTopParams}{sep}{eOutParams});": "Value e_{lbdid}({eTopParams}{sep}{eOutParams}) {\n" ++
				"	return {indents 1 fe.code};\n" ++
				"}"

			]
		}
		code	= codeAlgebra free data frec $ Classes.mapf frec (.code)
	}

	codeAlgebra	= \free data frec fcode -> {

	} -> {
		fcode,		frec :
		Mod a, Mod fa	-> {(#tpl("templates/Mod.names.h")), (#tpl("templates/Mod.bootstrap.h")), (#tpl("templates/Mod.bootstrap.c"))}

		UVar _		-> 0

		Match e bs,
		Match fe fbs	-> {lbdid = mkLbdId "_match_" data.path}
				-> "{lbdid}({A.implode ', ' $ [e] +++ A.fmap (\v -> "_variable_{v}") $ A.keys free})"

		Sco _ (e,),
		Sco _ (fe,)	-> {lbdid = mkLbdId "_sco_" data.path}
				-> "(all_{lbdid}({A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys free}))"

		#/************/
		#//*** Base
		#/************/
		Nfo _ a		-> a
		Boo b		-> "ValueBoolean_make({b ? 'TRUE' : 'FALSE'})"
		Int n		-> "ValueInteger_make({n})"
		Str s		-> "readStream0((Stream0) \"{S.serialize s}\")"


		Tpl s as	-> "ValueString_template((ValueString) readStream0((Stream0) \"{S.serialize s}\"), {L.len as}, (Value[]) \{{L.implode ', ' as}})"



		Var v		-> "_variable_{v}"
		App l r		-> "Ast_apply(\n\t{indent l},\n\t{indent r}\n)"
		Cmp l r		-> "ValueClosure_make(&Value_compose, 2, (Values) \{(Value[]) \{{l}, {r}}})"

		_, Lam v fb	-> {free = fb.free --- [(v): 1]; lbdid = mkLbdId "_lambda_" data.path}
				-> "ValueClosure_make(&{lbdid}, (Values) \{{A.len free}, (Value[]) \{{A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys free}}})"


		_, Obj a	-> "ValueObject_make(ObjectId_Grammar, {a.code}, 3, (Values) {0, 0})"
		Emb a		-> "{a}"

		Acc l r		-> "Ast_access({l}, {r})"

		Arr ()		-> "ValueArray_make(0, (Values) {0, 0})"
		Arr (a,)	-> "ValueArray_make(0, (Values) {1, (Value[]) \{{a}}})"
		Arr as		-> "ValueArray_make(0, (Values) {{L.len as}, (Value[]) \{\n\t{L.implode ",\n\t" $ L.fmap indent as}\n}})"

		Tup ()		-> "ValueArray_make(1, (Values) {0, 0})"
		Tup (a,)	-> "ValueArray_make(1, (Values) {1, (Value[]) \{{a}}})"
		Tup as		-> "ValueArray_make(1, (Values) {{L.len as}, (Value[]) \{\n\t{L.implode ",\n\t" $ L.fmap indent as}\n}})"

		Map ()		-> "ValueMap_make(0, (Map) {0, 0})"
		Map ({k, v},)	-> "ValueMap_make(0, (Map) {1, (Value[][2]) \{{k}, {v}}})"
		Map ps		-> "ValueMap_make(0, (Map) {{L.len ps}, (Value[][2]) \{\n\t{L.implode ",\n\t" $ L.mapf ps \p -> "{indent p.0}, {indent p.1}"}\n}})"

		Rcd ()		-> "ValueMap_make(1, (Map) {0, 0})"
		Rcd ({k, v},)	-> "ValueMap_make(1, (Map) {1, (Value[][2]) \{{k}, {v}}})"
		Rcd ps		-> "ValueMap_make(1, (Map) {{L.len ps}, (Value[][2]) \{\n\t{L.implode ",\n\t" $ L.mapf ps \p -> "{indent p.0}, {indent p.1}"}\n}})"



		Nil		-> "ValueNil_make()"
		Cns l r		-> "ValueCons_make(\n\t{indents 1 l},\n\t{indents 1 r}\n)"
		Car a		-> "Ast_car({a})"
		Cdr a		-> "Ast_cdr({a})"

		Dat i n ()	-> "ValueName_make(NameId_Data_{n}_{i}, {i}, (Values) {0, 0})"
		Dat i n (a,)	-> "ValueName_make(NameId_Data_{n}_{i + 1}, {i}, (Values) \{1, (Value[]) \{{a}}})"
		Dat i n as	-> "ValueName_make(NameId_Data_{n}_{i + L.len as}, {i}, (Values) \{{L.len as}, (Value[]) \{\n\t{L.implode ', ' $ L.fmap indent as}\n}})"

		Typ i n ()	-> "ValueName_make(NameId_Type_{n}_{i}, {i}, (Values) {0, 0})"
		Typ i n (a,)	-> "ValueName_make(NameId_Type_{n}_{i + 1}, {i}, (Values) \{1, (Value[]) \{{a}}})"
		Typ i n as	-> "ValueName_make(NameId_Type_{n}_{i + L.len as}, {i}, (Values) \{{L.len as}, (Value[]) \{\n\t{L.implode ', ' $ L.fmap indent as}\n}})"

		Nam i n ()	-> "ValueName_make(NameId_Name_{n}_{i}, {i}, (Values) {0, 0})"
		Nam i n (a,)	-> "ValueName_make(NameId_Name_{n}_{i + 1}, {i}, (Values) \{1, (Value[]) \{{a}}})"
		Nam i n as	-> "ValueName_make(NameId_Name_{n}_{i + L.len as}, {i}, (Values) \{{L.len as}, (Value[]) \{\n\t{L.implode ', ' $ L.fmap indent as}\n}})"

		TOp c t f	-> "(Value_toBoo({c}) ? {t} : {f})"
		UOp "!" a	-> "ValueBoolean_make(!(((ValueBoolean) ({a}))->b))"
		UOp "-" a	-> "ValueInteger_make(-(((ValueInteger) ({a}))->i))"

		BOp "+++" l r	-> "Ast_concat({l}, {r})"
		BOp "---" l r	-> "Ast_diffkeys({l}, {r})"
		BOp "++" l r	-> "Ast_concat({l}, {r})"

		BOp "**" l r	-> "Ast_pow({l}, {r})"
		BOp "+" l r	-> "Ast_add({l}, {r})"
		BOp "-" l r	-> "Ast_dif({l}, {r})"
		BOp "*" l r	-> "Ast_mul({l}, {r})"
		BOp "/" l r	-> "Ast_div({l}, {r})"
		BOp "/+" l r	-> "Ast_divc({l}, {r})"
		BOp "/=" l r	-> "Ast_divr({l}, {r})"
		BOp "/-" l r	-> "Ast_divf({l}, {r})"
		BOp "%" l r	-> "Ast_mod({l}, {r})"


		Cpr "==" l r	-> "Value_reflexivity({l}, {r})"
		Cpr "!=" l r	-> "ValueBoolean_make(!Value_reflexivity({l}, {r}))"
		Cpr "><" l r	-> "Value_xwing({l}, {r})"
		Cpr o l r	-> "ValueBoolean_make(((ValueInteger) ({l}))->i {o} ((ValueInteger) ({r}))->i)"

		Coa "??" l r,
		Coa _ fl fr	-> {
					lbdid = mkLbdId "_coaAND_" data.path;
					free = !fr.free ? "" : ", " ++ A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys fr.free
				} -> "{lbdid}({l} {free})"

		Coa "?:" l r,
		Coa _ fl fr	-> {
					lbdid = mkLbdId "_coaOR_" data.path;
					free = !fr.free ? "" : ", " ++ A.implode ", " $ A.fmap (\e -> "_variable_{e}") $ A.keys fr.free
				} -> "{lbdid}({l} {free})"

	}

}
