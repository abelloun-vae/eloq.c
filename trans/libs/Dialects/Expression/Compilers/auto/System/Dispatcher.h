{d.ret} Ast_{d.name}({L.implode ", " $ L.mapf d.params \p -> "{p.type} {p.name}"});
	{{params = \c -> (L.reduce (\a p -> A.in "dispatch" p ? {a.0.$$, L.concat a.1 (a.0.$ == 'Any' ? "{p.type} {p.name}" : "{p.type}{a.0.$} {p.name}",)} : {a.0, L.concat a.1 ("{p.type} {p.name}",)}) {c, ()} d.params).1}
	-> L.implode "\n\t" $ L.mapf d.cases \c -> "{d.ret} Ast_{d.name}_{L.implode '_' c}({L.implode ", " $ params c});"}
