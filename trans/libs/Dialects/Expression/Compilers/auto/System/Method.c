//###########################################################
// {m.name}
//###########################################################
{m.ret} Value_{m.name}({L.implode ', ' $ L.mapf m.params \p -> "{p.type} {p.name}"}) {
	{A.in "body" m ? "DISPATCHER_{m.name}_BODY({L.implode ', ' $ L.mapf m.params (.name)});" : ""}
	switch({m.params.$.name}->id) {
		{L.implode "\n\t\t" $ maptyps \t -> "case ValueId_{t.name}: " ++
			(m.ret != 'void' ? "return ({m.ret}) " : '' ) ++ (
			A.in "expr" m ? "DISPATCHER_{m.name}_EXPR({t.name}, {L.implode ', ' $ L.mapf m.params (.name)});" :
			"Value{t.name}_{m.name}({L.implode ", " $ L.mapf m.params \p -> "{A.in "recast" p ? "(Value{t.name}) " : ''}{p.name}"}); " ) ++
			(m.ret == 'void' ? 'break;' : '')}
		default:
			printf("\n{m.name} not dispatched for object !!!");
			exit(1);
	}
}
