{d.ret} Ast_{d.name}({L.implode ", " $ L.mapf d.params \p -> "{p.type} {p.name}"}) {
	switch({L.reduce (\a p -> "({a} * ValueId_MAX_ + {p.name}->id)") "0" $ L.filter (A.in "dispatch") d.params}) {

		{{
			tps = L.mapf types \t -> t.name
			extends = \matchers -> L.fold (\case cs -> L.concat (L.mapf matchers \t -> L.concat case (t,)) cs) ()
			mkCase = L.reduce (\am m -> extends (m == 'Any' ? tps : (m,)) am) ((),)
			cases = L.fold (L.concat . mkCase) ()

			filterCase = \hashes -> L.filter (\x -> !A.in (L.implode '_' x) hashes)
			strCase = \c -> "case {L.reduce (\a c -> "({a} * ValueId_MAX_ + ValueId_{c})") "0" c}:"

			ifDisp = A.in "dispatch"
			redDisp = \a p -> {a.0.$$, L.concat a.1 (a.0.$ == 'Any' ? "{p.name}" : "({p.type}{a.0.$}) {p.name}",)}
			redNoDisp = \a p -> {a.0, L.concat a.1 ("{p.name}",)}
			params = \c -> (L.reduce (\a p -> A.in "dispatch" p ? redDisp a p : redNoDisp a p) {c, ()} d.params).1
		} -> L.implode "\n\t\t" $ L.mapf d.cases \c -> "{L.implode "\n\t\t" $ L.mapf (mkCase c) strCase} return Ast_{d.name}_{L.implode '_' c}({L.implode ", " $ params c});"}
		default: 			dispatch_error("Ast_{d.name}", "no matching case", {L.len $ L.filter (A.in "dispatch") d.params}, (Value[]) \{{L.implode ', ' $ L.fmap (.name) $ L.filter (A.in "dispatch") d.params}}); return 0;
	}
}

