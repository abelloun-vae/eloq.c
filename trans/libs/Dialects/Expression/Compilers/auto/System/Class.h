//######################
// Class {type.name}
//######################
typedef struct		Value{type.name} {
			ValueId				id;
			{L.implode "\n\t\t\t" $ L.mapf type.fields \f -> "{f.type}\t\t		{f.name};"}
}							S_Value{type.name};
{L.implode "\n" $ L.mapf type.fields \f -> "typedef\t\t	{f.type}				Value{type.name}_FIELDS_{f.name};"}
//########### METHODS ###########
Value			Value{type.name}_make  		({mkParams type.fields});
size_t			Value{type.name}_sizeof		({mkAParams type.fields});
Value{type.name}{"\t\t"}Value{type.name}_alloc 		({mkAParams type.fields});
Value			Value{type.name}_init  		(Value{type.name} this{type.fields != () ? ", " : ""}{mkParams type.fields});
Value{type.name}{"\t"}	Value{type.name}_realloc{"\t"}	(Value{type.name} this{mkAParams type.fields ?? ", "}{mkAParams type.fields});
Value{type.name}{"\t"}	Value{type.name}_coalloc{"\t"}	(Value{type.name} this, Byte* _ptr{mkAParams type.fields ?? ", "}{mkAParams type.fields});
//###########  ###########
void			Value{type.name}_check 		(Value v);
void			Value{type.name}_free  		(Value{type.name} this);
Value			Value{type.name}_clone 		(Value{type.name} this);
Value			Value{type.name}_copy  		(Value{type.name} src, Value{type.name} tgt);
Value			Value{type.name}_fill  		(Value{type.name} src, Value{type.name} tgt);

