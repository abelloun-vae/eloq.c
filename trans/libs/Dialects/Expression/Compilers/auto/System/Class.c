//######################
// CLASS {type.name}
//######################
Value Value{type.name}_make({mkParams type.fields}) {
	return Value{type.name}_init(Value{type.name}_alloc({mkAArgs2 "" type.fields}){type.fields != () ? ", " : ""}{mkArgs type.fields});
}

size_t Value{type.name}_sizeof({mkAParams type.fields}) {
	return sizeof(S_Value{type.name}) + {L.fold (\f a -> "sizeof({f.type}_ALLOCTYPE[{f.name}_{f.props.alloc}]) + {a}") "0" $ ifAlloc type.fields};
}

Value{type.name} Value{type.name}_alloc({mkAParams type.fields}) {
	Byte* _ptr = Allocator_alloc(Value{type.name}_sizeof({mkAArgs type.fields}));
	Value{type.name} this = (Value{type.name}) _ptr;
	this->id = ValueId_{type.name};
	Value{type.name}_coalloc(this, _ptr + sizeof(S_Value{type.name}){mkAArgs type.fields ?? ", "}{mkAArgs type.fields});
	return this;
}

Value Value{type.name}_init{A.in "noInit" type ? "_noInit" : ""}(Value{type.name} this{type.fields != () ? ", " : ""}{mkParams type.fields}) {
	this->id = ValueId_{type.name};
	{L.implode "\n\t" $ L.mapf (noInner type.fields) \f -> A.in "alloc" f.props ? "{f.type}_COPY({f.name}, this->{f.name});" : "this->{f.name} = {f.name};"}
	return (Value) this;
}

Value{type.name} Value{type.name}_realloc(Value{type.name} this{mkAParams type.fields ?? ", "}{mkAParams type.fields}) {
	Byte* _ptr = Allocator_alloc({L.fold (\f a -> "sizeof({f.type}_ALLOCTYPE[{f.name}_{f.props.alloc}]) + {a}") "0" $ ifAlloc type.fields});
	Value{type.name}_coalloc(this, _ptr{mkAParams type.fields ?? ", "}{mkAArgs type.fields});
	return this;
}

Value{type.name} Value{type.name}_coalloc(Value{type.name} this, Byte* _ptr{mkAParams type.fields ?? ", "}{mkAParams type.fields}) {
	{(.1) $ L.reduce (\acc f -> {r = "{acc.0} + sizeof({f.type}_ALLOCTYPE[{f.name}_{f.props.alloc}])"} -> {
		r, "{acc.1}{f.type}_COALLOC(this->{f.name}, {acc.0}, {f.name}_{f.props.alloc});\n\t"
	}) {"_ptr", ""} $ ifAlloc type.fields}
	return this;
}

//############################################
//
//############################################
void Value{type.name}_check(Value v) {
	if (v->id != ValueId_{type.name}) {
		printf("Type error, expected : \"{type.name}\", found :\n");
		Value_printSerial(v);
		printf("\n");
		exit(1);
	}
}

void Value{type.name}_free(Value{type.name} this) {
	Ptr first = {{f = ifAlloc type.fields} -> f == () ? "this + 1" : "{f.$.type}_COPTR(this->{f.$.name})"};
	//~ if (first != ((Ptr) (this + 1)))
		//~ Allocator_free(first);
	//~ Allocator_free(this);
}

Value Value{type.name}_clone(Value{type.name} v) {
	return Value{type.name}_copy(v, Value{type.name}_alloc({mkAArgs2 "v" type.fields}));
}

Value Value{type.name}_copy(Value{type.name} src, Value{type.name} tgt) {
	tgt->id = ValueId_{type.name};
	{L.implode "\n\t" $ L.mapf type.fields \f -> A.in "alloc" f.props ? "{f.type}_COPY(src->{f.name}, tgt->{f.name});" : "tgt->{f.name} = src->{f.name};"}
	return (Value) tgt;
}

Value Value{type.name}_fill(Value{type.name} src, Value{type.name} tgt) {
	Value{type.name}_realloc(tgt{mkAArgs2 "src" type.fields ?? ", "}{mkAArgs2 "src" type.fields});
	return Value{type.name}_copy(src, tgt);
}
