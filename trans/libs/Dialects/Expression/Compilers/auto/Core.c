#include "../main.h"
//###########################################################
// Body
//###########################################################

//######################
// Value
//######################
void Value_writeName(Value v, Writer w) {
	switch (v->id) {
		{L.implode "\n\t\t" $ maptyps \t -> "case ValueId_{t.name}: Writer_writeStream0(w, (Stream0) \"{t.name}\"); break;"}
		default: Writer_writeStream0(w, (Stream0) "<UNKNOWN Value Type : HUGE ERROR !>");
	}
}

void Value_printSizes() {
	printf("Value\t\t: %lu\n", sizeof(Value));
	printf("S_Value\t\t: %lu\n", sizeof(S_Value));
	{L.implode "\n\t" $ maptyps \t -> "printf(\"{t.name}		: %lu\\n\", sizeof(S_Value{t.name}));"}
	printf("##############################\n");
}

//###########################################################
// DISPATCHERS
//###########################################################
#define			DISPATCHER_refl_BODY(left, right)			if (left == right) return 1; if (left->id != right->id) return 0
#define			DISPATCHER_refl_EXPR(type, left, right)		(Value##type##_refl((Value##type) left, (Value##type) right) && \
													Values_refl(Value##type##_children((Value##type) left), Value##type##_children((Value##type) right)))
#define			DISPATCHER_write_BODY(this, w, mode)		if (mode == Value_WriterMode_info) {\
														Writer_writeChar(w, '(');\
														Value_writeName(this, w);\
														Writer_writeChar(w, ')');\
														Writer_writeChar(w, ' ');\
													}

{L.implode "\n" $ L.mapf dispatchers \d -> (#tpl("System/Dispatcher.c"))}

{L.implode "\n" $ mapmets \m -> (#tpl("System/Method.c"))}

//######################
// Abstract Class
//######################
{L.implode "\n" $ maptyps \type -> (#tpl("System/Class.c"))}
