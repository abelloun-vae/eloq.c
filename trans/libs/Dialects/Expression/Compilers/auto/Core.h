#ifndef CORE_H
#define CORE_H

	//###########################################################
	//
	//###########################################################
	typedef enum		Value_WriterMode {
				Value_WriterMode_serial,
				Value_WriterMode_string,
				Value_WriterMode_info,
	}							Value_WriterMode;

	typedef enum		ObjectId {
				ObjectId_Parser,
				ObjectId_Grammar,
				ObjectId_Template,
	}							ObjectId;

	//###########
	// Ids
	//###########
	typedef enum		ValueId {
				ValueId_MIN_,
				{L.implode "\n\t\t\t\t" $ maptyps \type -> "ValueId_{type.name},"}
				ValueId_MAX_,
	}							ValueId;

	//###########
	// Abstract Value
	//###########
	typedef union		Value*				Value;
	typedef struct		Values				Values;
	typedef struct		Map				Map;
	typedef			Value				(*Closure)(Values scope, Value arg);
	typedef			Value				(*Lambda)(Values args);

	#define			LOOP(n, i)			for(Nat __tot = n, i = 0; i < __tot; i++)
	//###########################################################
	#define			Values_COPTR(x)			((Ptr) x.values)
	#define			Values_COALLOC(x, p, s)		x.size = s; x.values = (Value*) (p)
	#define			Values_COPY(x, y)		y.size = x.size; LOOP(x.size, __i) y.values[__i] = x.values[__i]
	typedef			Value*				Values_ALLOCTYPE;
	typedef			Nat				Values_FIELDS_size;
	typedef			Value*				Values_FIELDS_values;
	struct			Values {
				Nat				size;
				Value*				values;
	};

	//###########################################################
	#define			Map_COPTR(x)			((Ptr) x.pairs)
	#define			Map_COALLOC(x, p, s)		x.size = s; x.pairs = (Value(*)[2]) (p)
	#define			Map_COPY(x, y)			y.size = x.size; LOOP(x.size, __i) {\
									y.pairs[__i][0] = x.pairs[__i][0];\
									if (y.pairs[__i][0]) y.pairs[__i][1] = x.pairs[__i][1];\
								}
	typedef			Value				Map_ALLOCTYPE[2];
	typedef			Nat				Map_FIELDS_size;
	typedef			Value				(*Map_FIELDS_pairs)[2];
	struct			Map {
				Nat				size;
				Value				(*pairs)[2];
	};

	//###########
	// Types
	//###########
	{L.implode "\n\t" $ maptyps \type -> "typedef struct		Value{type.name}*			Value{type.name};"}

	//######################
	// Value
	//######################
	void			Value_writeName			(Value v, Writer w);
	void			Value_printSizes			();

	//###########################################################
	// Classes Definitions
	//###########################################################
	{L.implode "\n\t" $ maptyps \type -> indent (#tpl("System/Class.h"))}

	//###########################################################
	// Union
	//###########################################################
	typedef union		Value {
				ValueId				id;
				{L.implode "\n\t\t\t\t" $ maptyps \type -> "S_Value{type.name}			{type.name};"}
	}							S_Value;

	//###########################################################
	//
	//###########################################################
	{L.implode "\n\t" $ L.mapf dispatchers \d -> (#tpl("System/Dispatcher.h"))}

	{L.implode "\n\t" $ mapmets \m -> (
		"{m.ret}\t\t	Value_{m.name}\t\t	({L.implode ', ' $ L.mapf m.params \p -> "{p.type} {p.name}"});\n" ++
		$ !A.in "full" m ? "" : L.implode "\n" $ maptyps \t -> (
			"\t{m.ret}\t\t	Value{t.name}_{m.name}          	({L.implode ", " $ L.mapf m.params \p -> "{A.in "recast" p ? "Value{t.name}" : p.type} {p.name}"});"
	)) ++ "\n"}

#endif
