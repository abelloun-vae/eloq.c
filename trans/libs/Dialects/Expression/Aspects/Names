{
	#/*************************************************/
	#// Names initialisation -- init :: Expr => Expr
	#/*************************************************/
	init	= \ast -> {
		ast		-> ast :
		Sco ds me	-> Sco (L.fmap Stmt.init ds) me
	}

	#//************************************************/
	#// Top-Down Path Generation CoAlgebra
	#//************************************************/
	path :: String => ExprF $a => ExprF String
	path	= \path ast	-> {
		ast		-> Classes.fromList ast $ L.mapf (I.toList $ L.len $ Base.toList ast) \n -> "{path}.{n}" :
	}

	#/*************************************************/
	#// Bottom-Up Free Variables Propagation Algebra
	#/*************************************************/
	free :: ExprF (Env $a) => Env $a
	free	= \frast -> {
		frast		-> Classes.reduce Rho.add Rho.zer frast :
		Mod a		-> Rho.zer
		Var v		-> Rho.elm {v, Var v}
		Lam v b		-> Rho.remE b {v, Var v}
		Match e bs	-> L.fold (Rho.add . \b -> Rho.rem (M.reduce Rho.add b.expr b.gard) $ addParams Rho.zer b.patt) e bs
		Sco ds (e,)	-> Rho.add (Rho.rem e (Scopes.scopeOut Rho.zer ds)) $ Scopes.freeIn ds
	}

	#//************************************************/
	#// Top-Down Bound Variables Propagation CoAlgebra
	#//************************************************/
	scope	= \scope ast	-> {
		ast		-> Classes.place scope ast :
		Lam v _		-> Lam v $ Rho.addE scope {v, Var v}
		Match e bs	-> Match scope $ L.mapf bs \b -> Classes.Brnch.place (addParams scope b.patt) b
		Sco ds (e,)	-> Sco (L.fmap (Stmt.scope $ Scopes.scopeIn scope ds) ds) (Scopes.scopeOut scope ds,)
		Mod _		-> Mod $ Rho.lst $ {"true", Boo 1} :> {"false", Boo 0} :> L.fmap (\x -> {x, Var x}) $
					("reimp", "implementation", "implementations", "toString", "include", "eval", "die", "echo", "preg_match", "throw")
	}

	#//************************************************/
	#// Top-Down Location Generation CoAlgebra
	#//************************************************/
	location = \loc ast	-> {
		ast		-> Classes.place loc ast :
		Sco ds me	-> Sco (L.fmap (Stmt.location loc) ds) $ M.place "{loc}.@" me
		Lam v _		-> Lam v "{loc}.\\{v}"
	}

	#//************************************************/
	#//
	#//************************************************/
	addParams	= \scope ast -> {
		ast 		-> Classes.reduce addParams scope ast :
		Var v		-> Rho.addE scope {v, Var v}
	}

	Stmt	= {

		init		= \ast -> {
			ast		-> ast :
			Dec n ne sts	-> Dec n (Names.init Typ ne) $ L.mapf sts (Names.init Dat)
			Syn n ne e	-> Syn n (Names.init Nam ne) e
		}

		free		= \fstmt -> {
			fstmt :
			Dec _ ne ds	-> Rho.rem (L.reduce (Classes.reduce Rho.add) Rho.zer ds) $ addParams Rho.zer ne
			Syn _ ne e	-> Rho.rem e $ addParams Rho.zer ne
			Aff _ _ e	-> e
		}

		scope		= \sco stmt -> {
			stmt		-> Classes.Stmt.place sco stmt :
			Dec _ ne _	-> Classes.Stmt.place (addParams sco ne) stmt
			Syn _ ne _	-> Classes.Stmt.place (addParams sco ne) stmt
		}

		location	= \loc stm -> {
			stm		-> Classes.Stmt.place loc stm :
			Dec _ ne _	-> Classes.Stmt.place "{loc}.{Names.name ne}" stm
			Syn _ ne _	-> Classes.Stmt.place "{loc}.{Names.name ne}" stm
			Aff "_" w _	-> Classes.Stmt.place "{loc}.{w}" stm
			Aff w _ _	-> Classes.Stmt.place "{loc}.{w}" stm
		}

	}

	#//************************************************/
	#//
	#//************************************************/
	Scopes	= {

		buildRcd	:: (*StatementF p n a) => ExprF p n a
		buildRcd	= Rcd . L.fmap (\e -> {Str e.0, e.1}) . Rho.elms . scopeOut Rho.zer

		freeIn		= \sts -> Rho.rem (L.fold (Rho.add . Stmt.free) Rho.zer sts) $ scopeIn Rho.zer sts

		addName		= \scope ast -> Rho.addE scope {Names.name ast, Names.base ast}

		scopeIn		= L.reduce \scope stmt -> {
					stmt		-> scope :
					Dec _ ne sts	-> L.reduce addName (addName scope ne) sts
					Syn _ ne _	-> addName scope ne
					Aff _ '_' _	-> scope
					Aff _ v _	-> Rho.addE scope {v, Var v}
				}

		scopeOut	= L.reduce \scope stmt -> {
					stmt		-> scope :
					Dec '_' _ _	-> scope
					Dec '' ne sts	-> L.reduce addName (addName scope ne) sts
					Dec v ne _	-> Rho.addE scope {v, Names.base ne}
					Syn '_' _ _	-> scope
					Syn '' ne _	-> addName scope ne
					Syn v ne _	-> Rho.addE scope {v, Names.base ne}
					Aff '_' _ _	-> scope
					Aff v _ _	-> Rho.addE scope {v, Var v}
				}

	}

	#//************************************************/
	#//
	#//************************************************/
	Names	= {

		arity	= \ast -> {
			ast		-> throw "Trying to get arity of : {toString ast}" :
			Dat i _ as	-> i + L.len as
			Typ i _ as	-> i + L.len as
			Nam i _ as	-> i + L.len as
		}

		name	= \ast -> {
			ast		-> throw "Trying to get name of : {toString ast}" :
			Dat _ n _	-> n
			Typ _ n _	-> n
			Nam _ n _	-> n
		}

		args	= \ast -> {
			ast		-> throw "Trying to get args of : {toString ast}" :
			Dat _ _ as	-> as
			Typ _ _ as	-> as
			Nam _ _ as	-> as
		}

		base	= \ast -> {
			ast		-> throw "Trying to get base of : {toString ast}" :
			Dat i n as	-> Dat (i + L.len as) n ()
			Typ i n as	-> Typ (i + L.len as) n ()
			Nam i n as	-> Nam (i + L.len as) n ()
		}

		init	= \typ -> _init typ 0 ()
		_init	= \typ arity args nexpr -> {
			nexpr			-> throw "Error during resolution, should not find : {toString nexpr}" :
			App l r			-> _init typ (arity + 1) (r :> args) l
			Var v			-> typ 0 v args
			Nam i n as / arity == 0	-> nexpr
			Typ i n as / arity == 0	-> nexpr
			Dat i n as / arity == 0	-> nexpr
		}

	}

}
