// <?php // START REG
$node = false;
if ( preg_match("{r}", $state['input'], $m) ) {
	$len = strlen($m[0]);
	$node = [$m];
	$state['input'] = substr($state['input'], $len);
	$state['pos'] += $len;
	$state['line'] += substr_count($m[0], "\n");
	$x = strrpos($m[0], "\n");
	$state['col'] = $x === false ? $state['col'] + $len : ($len - $x);
	$state['max'] = max($state['max'], $state['pos']);
} {!ignore ? "" : "elseif ( !$state['ignore'] ) {
	$savestate = $state;
	if ($parser::pass($context, $state, $parser)) {
		if ( preg_match(\"{r}\", $state['input'], $m) ) {
			$len = strlen($m[0]);
			$node = [$m];
			$state['input'] = substr($state['input'], $len);
			$state['pos'] += $len;
			$state['line'] += substr_count($m[0], \"\\n\");
			$x = strrpos($m[0], \"\\n\");
			$state['col'] = $x === false ? $state['col'] + $len : ($len - $x);
			$state['max'] = max($state['max'], $state['pos']);
		} else {
			$state = $savestate;
		}
	}
}"}
//END REG
