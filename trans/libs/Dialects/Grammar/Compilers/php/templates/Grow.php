// START ALT_GRO <?php
	$start = $state;
	// START ALT_GRO SEED
	{indents 1 seed}
	// END ALT_GRO SEED
	// START ALT_GRO LOOP
	if ($node) do {
		$context['memo'][${keyVar}] = [$state, $node];
		$pos = $state['pos'];
		$state = $start;
		// START ALT_GRO GROW
		{indents 2 grow}
		// END ALT_GRO GROW
		if ( $state['pos'] <= $pos || !$node ) {
			$mem = $context['memo'][${keyVar}];
			if ( $mem[1] ) {
				$state['input'] = $mem[0]['input'];
				$state['pos'] = $mem[0]['pos'];
				$state['precedence'] = $mem[0]['precedence'];
			}
			$node = $mem[1];
			break;
		}
	} while(true);
	// END ALT_GRO LOOP
// END ALT_GRO
