// <?php // START STR
$node = false;
if ( ( substr($state['input'], 0, {n}) === "{s}" ) ) {

	$node = ["{s}"];
	$state['input'] = substr($state['input'], {n});
	$state['pos'] += {n};
	$state['line'] += {implementation 2 "substr_count" s "\n"};
	$x = strrpos("{s}", "\n");
	$state['col'] = $x === false ? $state['col'] + {S.len s} : ({S.len s} - $x);
	$state['max'] = max($state['max'], $state['pos']);
} {!ignore ? "" : "elseif ( !$state['ignore'] ) {
	$savestate = $state;
	if ($parser::pass($context, $state, $parser)) {
		if ( ( substr($state['input'], 0, {n}) === \"{s}\" ) ) {

			$node = [\"{s}\"];
			$state['input'] = substr($state['input'], {n});
			$state['pos'] += {n};
			$state['line'] += {implementation 2 "substr_count" s "\n"};
			$x = strrpos(\"{s}\", \"\\n\");
			$state['col'] = $x === false ? $state['col'] + {S.len s} : ({S.len s} - $x);
			$state['max'] = max($state['max'], $state['pos']);
		} else {
			$state = $savestate;
		}
	}
}"}
//END STR
