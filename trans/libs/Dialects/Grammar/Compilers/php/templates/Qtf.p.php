//START QTF+ <?php
$nodes[] = [];
do {
	{indents 1 body}
	if ($node) $nodes[count($nodes) - 1][] = $node[0];
} while ($node);
$tmpnode = array_pop($nodes);
$node = !$tmpnode ? false : [array_reduce(
	array_reverse($tmpnode),
	function($a, $e) {return self::$expressionContext[0]::cons($e, $a);},
	self::$expressionContext[0]::$nil
)];
//END QTF+
