// START ALT_GRO SEED
//printf(">>> SEED %u:%u:%i\n", state->position, state->prec, state->asso);
{indents 1 seed}
//printf("<<< SEED %s %u:%u:%i\n", node ? "OK" : "KO", state->position, state->prec, state->asso);
// END ALT_GRO SEED
// START ALT_GRO LOOP
if (node) {
	//printf(">>> GROW %u:%u:%i\n", state->position, state->prec, state->asso);
	do {
		PTable_store(context->memo + {key}, &state_{uid}, state, node);
		Nat pos_{uid} = state->position;
		*state = state_{uid};
		// START ALT_GRO GROW
		//printf(">>> GROW RUN %u:%u:%i\n", state->position, state->prec, state->asso);
		{indents 2 grow}
		//printf("<<< GROW RUN %s %u:%u:%i\n", node ? "OK" : "KO", state->position, state->prec, state->asso);
		// END ALT_GRO GROW
		if ( !node || state->position <= pos_{uid} ) {
			node = PTable_restore(context->memo + {key}, &state_{uid}, state);
			break;
		}
	} while(true);
	//printf("<<< GROW %s %u:%u:%i\n", node ? "OK" : "KO", state->position, state->prec, state->asso);
}
