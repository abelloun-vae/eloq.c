if ( PTable_exists(context->memo + {key}, state) ) {
	//printf(">>> MEMO %u:%u:%i\n", state->position, state->prec, state->asso);
	node = PTable_restore(context->memo + {key}, state, state);
	//printf("<<< MEMO %s %u:%u:%i\n", node ? "OK" : "KO", state->position, state->prec, state->asso);
} else {
	//printf(">>> NOMEMO %u:%u:%i\n", state->position, state->prec, state->asso);
	S_PState state_{uid} = *state;
	{indents 1 body}
	PTable_store(context->memo + {key}, &state_{uid}, state, node);
	//printf("<<< NOMEMO %s %u:%u:%i\n", node ? "OK" : "KO", state->position, state->prec, state->asso);
}
