#/**************************************************/
#// Parsing Expression Grammar Module
#/**************************************************/
\EXPRESSION -> {
	Sig	= Env {key: \e->e, value: \e->e}
	#/**************************************************/
	#// Types
	#/**************************************************/
	RuleF a = {*
		ignore: Boolean
		name: String
		args: (*a)
		params: (*String)
		pexpr: a
	}
	Rgx = {*
		regex: String
		pattern: String
		modifier: String
	}
	#/**************************************************/
	#// Parsing Expression Functor // Data Constructors
	#/**************************************************/
	PExprF e a :=
	|	Nfo $x a
	|	Emb a
	#// Calculus
	|	App a a
	|	Var String (*a)
	|	Gra (*String) (*RuleF a)
	#// Base Parsers
	|	Uid
	|	Reg Rgx
	|	Str String
	#// Combinators
	|	Ign a
	|	Inv a a
	|	Rep a a
	|	Alt (*a)
	|	Seq (*a)
	|	Sel String a
	|	Qtf String a
	|	Act Boolean e a
	|	Pre String Integer a
	#/**************************************************/
	#// Helpers for Data Constructors
	#/**************************************************/
	mkRule		= \ign name args params pexpr -> {ignore: ign, name: name, args: args, params: params, pexpr: pexpr}
	#/**************************************************/
	#// Components
	#/**************************************************/
	Parser		= (#exp("Grammar.Parser"))
	Base		= (#exp("Grammar.Base"))
	Classes		= Base +++ Generics Base
	Names		= (#exp("Aspects/Names"))
	Analysis	= (#exp("Aspects/Analysis"))
	Expansion	= (#exp("Aspects/Expansion"))
	#/**************************************************/
	#// Interfaces
	#/**************************************************/
	compiler	= (#exp("Compilers/c/Compiler"))
	serializer	= (#exp("Aspects/Serialize"))
	read		= (#exp("Aspects/Read"))
}
