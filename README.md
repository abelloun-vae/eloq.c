# Eloq.c

Work in progress !

Eloq programming language : test for a c implementation


- `/bootstrap` : contains the C runtime + languages source.
- `/bootstrap/autogen/` : languages source code
- `/bootstrap/core/ast/` : polymorphic operators
- `/bootstrap/core/classes/` : base data types
- `/bootstrap/core/libs/` : runtime system, allocator / garbage collector, base libs
- `/bootstrap/system/extensions/` : specific implementation of polymorphic operators
- `/bootstrap/system/implementations/` : some elementary functions ( mainly for performances reasons, ie array_map, ou string_length )



