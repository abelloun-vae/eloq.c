OBJ=gcc -O2 -c -Werror -Wall -Wextra -Wno-format-extra-args 			-Wno-unused-variable -Wno-unused-parameter
EXE=gcc -O2 -lm -Werror -Wall -Wextra -Wno-format-extra-args			-Wno-unused-variable -Wno-unused-parameter

LIBS: allocator.o strings.o files.o parser.o
allocator.o:
	$(OBJ) bootstrap/libs/Allocator/Allocator.c -o tmp/allocator.o
strings.o:
	$(OBJ) bootstrap/libs/Strings/Strings.c -o tmp/strings.o
files.o:
	$(OBJ) bootstrap/libs/Files/Files.c -o tmp/files.o
parser.o:
	$(OBJ) bootstrap/libs/Parsers/Parsers.c -o tmp/parsers.o


CORE: core.o classes.o ast.o
core.o:
	$(OBJ) bootstrap/autogen/core.c -o tmp/core.o -Wno-unused-parameter
classes.o:
	$(OBJ) bootstrap/core/classes.c -o tmp/classes.o
ast.o:
	$(OBJ) bootstrap/core/ast.c -o tmp/ast.o


SYSTEM: extensions.o helpers.o implementations.o
extensions.o:
	$(OBJ) bootstrap/system/extensions/extensions.c -o tmp/extensions.o
helpers.o:
	$(OBJ) bootstrap/system/helpers/helpers.c -o tmp/helpers.o
implementations.o:
	$(OBJ) bootstrap/system/implementations/implementations.c -o tmp/implementations.o


MAIN: main.o app.o
main.o:
	$(OBJ) bootstrap/main.c -o tmp/main.o
app.o:
	$(OBJ) bootstrap/autogen/app.c -o tmp/app.o -Wno-unused-variable -Wno-unused-parameter
	
EXE:
	$(EXE) tmp/*


MOST: clean LIBS CORE SYSTEM MAIN

ALL: clean LIBS CORE SYSTEM MAIN EXE

FULL: clean
	$(EXE) \
		bootstrap/core/*.c \
		bootstrap/autogen/*.c \
		bootstrap/libs/Allocator/Allocator.c \
		bootstrap/libs/Strings/Strings.c \
		bootstrap/libs/Files/Files.c \
		bootstrap/libs/Parsers/Parsers.c \
		bootstrap/system/helpers/helpers.c \
		bootstrap/system/extensions/extensions.c \
		bootstrap/system/implementations/implementations.c \
		bootstrap/main.c  \
		-Wno-unused-parameter

clear: clean
clean:
	clear;
	rm -f tmp/*;
	rm -f a.out
	rm -f vgcore.*;
	rm -f massif.*;
